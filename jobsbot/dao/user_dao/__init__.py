import random
import ssl
import string
from collections import Counter, defaultdict

from datetime import datetime, timedelta
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.errors import DuplicateKeyError
from typing import Generator, Iterable, List, Optional, Tuple, Union

from jobsbot.core.enum_maps import JOBS_TREE_INVERTED, COUNTRY_COUNTRY_LOCATIONS_MAP
from jobsbot.core.logging import get_logger
from jobsbot.core.models import Company, Job, User, UserCv, UserJobApplication, UserJobMatch, UserFeedback, \
    Admin, Recruiter, Session, Credentials, JobStatistics, RecruiterInvitation, UserSubscription
from jobsbot.core.enums import JobPostingStatus, JobPostingValidationStatus, UserNotificationOption, \
    UserProfileValidationStatus, UserJobMatchType, UserJobApplicationStatus, UserJobApplicationMethod, JobCategory, \
    JobPosition, UserCountry, UserJobMatchSource
from jobsbot.settings.mongodb import MongoDbConnectionParams


class UserDao:
    USERS_COLLECTION = "users"
    USERS_CVS_COLLECTION = "user_cvs"
    APPLICATIONS_COLLECTION = "user_applications"
    COMPANIES_COLLECTION = "companies"
    JOBS_COLLECTION = "jobs"
    JOB_STATISTICS_COLLECTION = "job_statistics"
    CREDENTIALS_COLLECTION = "credentials"
    ADMINS_COLLECTION = "admins"
    SESSIONS_COLLECTION = "admins_sessions"
    RECRUITERS_COLLECTION = "recruiters"
    RECRUITER_INVITATIONS_COLLECTION = "recruiter_invitations"
    USER_JOB_MATCHES = "user_job_matches"
    USER_JOB_APPLICATIONS = "user_job_applications"
    USER_SUBSCRIPTIONS = "user_subscription"
    FEEDBACK = "feedback"

    def __init__(self, connection_params: MongoDbConnectionParams, admins_collection=None, recruiters_collection=None,
                 recruiter_invitations_collection=None, sessions_collection=None, credentials_collection=None,
                 users_collection=None, user_cvs_collection=None, companies_collection=None, jobs_collection=None,
                 job_statistics_collection=None, user_job_matches_collection=None,
                 user_job_applications_collection=None, user_subscriptions_collection=None, feedback_collection=None):
        super().__init__()
        kwargs = {}
        if connection_params.username is not None and connection_params.password is not None:
            kwargs["username"] = connection_params.username
            kwargs["password"] = connection_params.password
        if connection_params.replica_set is not None:
            kwargs["replicaSet"] = connection_params.replica_set
        if connection_params.ssl_ and connection_params.ssl_certfile is not None:
            kwargs["ssl_ca_certs"] = connection_params.ssl_certfile
            kwargs["ssl_cert_reqs"] = ssl.CERT_REQUIRED
        if connection_params.auth_source is not None:
            kwargs["authSource"] = connection_params.auth_source
        self.client = MongoClient(connection_params.host, connection_params.port, **kwargs)
        self.db = self.client[connection_params.database]
        self.users = self.db[users_collection or self.USERS_COLLECTION]
        self.user_cvs = self.db[user_cvs_collection or self.USERS_CVS_COLLECTION]
        self.companies = self.db[companies_collection or self.COMPANIES_COLLECTION]
        self.jobs = self.db[jobs_collection or self.JOBS_COLLECTION]
        self.job_statistics = self.db[job_statistics_collection or self.JOB_STATISTICS_COLLECTION]
        self.credentials = self.db[credentials_collection or self.CREDENTIALS_COLLECTION]
        self.recruiters = self.db[recruiters_collection or self.RECRUITERS_COLLECTION]
        self.recruiter_invitations = self.db[recruiter_invitations_collection  or self.RECRUITER_INVITATIONS_COLLECTION]
        self.admins = self.db[admins_collection or self.ADMINS_COLLECTION]
        self.sessions = self.db[sessions_collection or self.SESSIONS_COLLECTION]
        self.user_job_matches = self.db[user_job_matches_collection or self.USER_JOB_MATCHES]
        self.user_job_applications = self.db[user_job_applications_collection or self.USER_JOB_APPLICATIONS]
        self.user_subscriptions = self.db[user_subscriptions_collection or self.USER_SUBSCRIPTIONS]
        self.feedback = self.db[feedback_collection or self.FEEDBACK]
        # self.create_indexes(reindex_=True)

    def create_indexes(self, reindex_: bool = False):
        logger = get_logger()
        def _create_index(collection: Collection, index_name: str, index_columns: List[Tuple[str, int]],
                          reindex: bool, unique: bool):
            existing_indexes = list(collection.index_information().keys())
            if index_name in existing_indexes and not reindex:
                return
            if index_name in existing_indexes:
                logger.info(f"Dropping an index {index_name}...")
                collection.drop_index(index_name)
                logger.info(f"Index {index_name} dropped!")
            logger.info(f"Creating a new index {index_name}...")
            collection.create_index(index_columns, name=index_name, unique=unique)
            logger.info(f"New index {index_name} created!")

        # USER INDEXES
        _create_index(self.users, "users_tg_id_index", [("tg_id", 1)], reindex=reindex_, unique=True)
        _create_index(self.users, "users_referral_link_id_index", [("referral_link_id", 1)], reindex=reindex_,
                      unique=True)
        _create_index(self.users, "users_referred_by_tg_id_index", [("referred_by_tg_id", 1)], reindex=reindex_,
                      unique=False)

        # USER CSV INDEXES
        _create_index(self.user_cvs, "user_cvs_tg_id_index", [("tg_id", 1)], reindex=reindex_, unique=True)

        # COMPANIES INDEXES
        try:
            # TODO: DELETEME
            try:
                self.companies.drop_index("companies_company_text_index")
            except Exception as e:
                logger.error(f"Can not drop index companies_company_text_index: {e}")
        except Exception as e:
            logger.error(f"Can not create full text index for a company collection: {e}")
        _create_index(self.companies, "companies_company_name_index", [("company_name", 1)], reindex=reindex_,
                      unique=True)
        _create_index(self.companies, "companies_company_id_index", [("company_id", 1)], reindex=reindex_, unique=True)
        _create_index(self.companies, "companies_referral_link_id_index", [("referral_link_id", 1)], reindex=reindex_,
                      unique=True)

        # JOB INDEXES
        _create_index(self.jobs, "jobs_job_id_index", [("job_id", 1)], reindex=reindex_, unique=True)
        _create_index(self.jobs, "jobs_company_id_index", [("company_id", 1)], reindex=reindex_, unique=False)

        # JOB STATISTICS INDEXES
        _create_index(self.job_statistics, "job_statistics_job_id_index", [("job_id", 1)], reindex=reindex_,
                      unique=True)

        # ADMIN INDEXES
        _create_index(self.admins, "admins_canonized_email_index", [("canonized_email", 1)], reindex=reindex_,
                      unique=True)

        # RECRUITERS INDEXES
        _create_index(self.recruiters, "recruiters_canonized_email_index", [("canonized_email", 1)], reindex=reindex_,
                      unique=False)
        _create_index(self.recruiters, "recruiters_company_id_canonized_email_index",
                      [("company_id", 1), ("canonized_email", 1)], reindex=reindex_, unique=True)

        # RECRUITER INVITATIONS INDEXES
        _create_index(self.recruiter_invitations, "recruiter_invitations_canonized_email_index",
                      [("canonized_email", 1)], reindex=reindex_, unique=False)
        _create_index(self.recruiter_invitations, "recruiter_invitations_token_index",
                      [("token", 1)], reindex=reindex_, unique=True)
        _create_index(self.recruiter_invitations, "recruiter_invitations_company_id_canonized_email_index",
                      [("company_id", 1), ("canonized_email", 1)], reindex=reindex_, unique=True)

        # CREDENTIALS
        _create_index(self.credentials, "credentials_canonized_email_index", [("canonized_email", 1)], reindex=reindex_,
                      unique=True)

        # SESSIONS INDEXES
        _create_index(self.sessions, "sessions_canonized_email_index", [("canonized_email", 1)],
                      reindex=reindex_, unique=False)
        _create_index(self.sessions, "sessions_token_index", [("token", 1)],
                      reindex=reindex_, unique=True)

        # USER-JOBS MATCHES INDEXES
        _create_index(self.user_job_matches, "user_job_matches_user_match_hash_index", [("match_hash", 1)],
                      reindex=reindex_, unique=True)
        _create_index(self.user_job_matches, "user_job_matches_user_tg_id_index", [("user_tg_id", 1)], reindex=reindex_,
                      unique=False)
        _create_index(self.user_job_matches, "user_job_matches_job_id_index", [("job_id", 1)], reindex=reindex_,
                      unique=False)
        _create_index(self.user_job_matches, "user_job_matches_user_tg_id_job_id_index",
                      [("user_tg_id", 1), ("job_id", 1)], reindex=reindex_, unique=True)

        # USER_JOBS APPLICATIONS INDEXES
        _create_index(self.user_job_applications, "user_job_applications_user_tg_id_index", [("user_tg_id", 1)],
                      reindex=reindex_, unique=False)
        _create_index(self.user_job_applications, "user_job_applications_job_id_index", [("job_id", 1)],
                      reindex=reindex_, unique=False)
        _create_index(self.user_job_applications, "user_job_applications_user_tg_id_job_id_index",
                      [("user_tg_id", 1), ("job_id", 1)], reindex=reindex_, unique=True)

        # USER SUBSCRIPTIONS
        _create_index(self.user_subscriptions, "user_subscriptions_user_tg_id_company_id_index",
                      [("user_tg_id", 1), ("company_id", 1)], reindex=reindex_, unique=True)
        _create_index(self.user_subscriptions, "user_subscriptions_company_id_index",
                      [("company_id", 1)], reindex=reindex_, unique=False)

    def create_credentials(self, credentials: Credentials, replace_if_exists: bool = False):
        if replace_if_exists:
            self.credentials.replace_one(
                filter={
                    "canonized_email": credentials.canonized_email
                },
                replacement=credentials.as_bson_dict(),
                upsert=True
            )
        else:
            try:
                self.credentials.insert_one(credentials.as_bson_dict())
            except DuplicateKeyError:
                pass

    def update_admin_recruiter_email(self, old_email: str, new_email: str):
        old_email = Credentials.canonize_email(old_email)
        new_canonized_email = Credentials.canonize_email(new_email)

        for collection in (self.credentials, self.admins, self.recruiters):
            collection.update(
                filter={
                    "canonized_email": old_email
                },
                update={
                    "$set": {
                        "canonized_email": new_canonized_email,
                        "email": new_email
                    }
                },
                upsert=False
            )

    def read_credentials(self, email: str) -> Optional[Credentials]:
        obj = self.credentials.find_one(filter={"canonized_email": Credentials.canonize_email(email)})
        if obj is None:
            return None
        return Credentials.from_dict(obj)

    def delete_credentials(self, email: str):
        self.credentials.delete_one({"canonized_email": Credentials.canonize_email(email)})

    def create_admin(self, admin: Admin, replace_if_exists: bool = False):
        if replace_if_exists:
            self.admins.replace_one(
                filter={
                    "canonized_email": admin.canonized_email
                },
                replacement=admin.as_bson_dict(),
                upsert=True
            )
        else:
            try:
                self.admins.insert_one(admin.as_bson_dict())
            except DuplicateKeyError:
                pass

    def read_admin(self, email: str) -> Optional[Admin]:
        obj = self.admins.find_one(filter={"canonized_email": Credentials.canonize_email(email)})
        if obj is None:
            return None
        return Admin.from_dict(obj)

    def delete_admin(self, email: str, delete_credentials_if_possible: bool = True):
        self.admins.delete_one(filter={"canonized_email": Credentials.canonize_email(email)})
        if delete_credentials_if_possible and not self.read_recruiters(email) and self.read_admin(email) is None:
            self.delete_credentials(email)

    def create_recruiter(self, recruiter: Recruiter, replace_if_exists: bool = False):
        if replace_if_exists:
            self.recruiters.replace_one(
                filter={
                    "company_id": recruiter.company_id,
                    "canonized_email": recruiter.canonized_email
                },
                replacement=recruiter.as_bson_dict(),
                upsert=True
            )
        else:
            try:
                self.recruiters.insert_one(recruiter.as_bson_dict())
            except DuplicateKeyError:
                pass

    def read_recruiters(self, email: str) -> List[Recruiter]:
        result = []
        for obj in self.recruiters.find(filter={"canonized_email": Credentials.canonize_email(email)}):
            result.append(Recruiter.from_dict(obj))
        return result

    def read_one_recruiter(self, email: str, company_id: str) -> Optional[Recruiter]:
        obj = self.recruiters.find_one(filter={"canonized_email": Credentials.canonize_email(email),
                                               "company_id": company_id})
        if obj is None:
            return None
        return Recruiter.from_dict(obj)

    def read_company_recruiters(self, company_id: str) -> List[Recruiter]:
        result = []
        for obj in self.recruiters.find(filter={"company_id": company_id}):
            result.append(Recruiter.from_dict(obj))
        return result

    def delete_company_recruiter(self, email: str, company_id: str, delete_credentials_if_possible: bool = True):
        self.recruiters.delete_one(filter={"canonized_email": Credentials.canonize_email(email),
                                           "company_id": company_id})
        if delete_credentials_if_possible and self.read_admin(email) is None and not self.read_recruiters(email):
            self.delete_credentials(email)

    def delete_recruiters(self, email: str, delete_credentials_if_possible: bool = True):
        self.recruiters.delete_many(filter={"canonized_email": Credentials.canonize_email(email)})
        if delete_credentials_if_possible and self.read_admin(email) is None and not self.read_recruiters(email):
            self.delete_credentials(email)

    def delete_one_recruiter(self, email: str, company_id: str, delete_credentials_if_possible: bool = True):
        self.recruiters.delete_one(filter={"canonized_email": Credentials.canonize_email(email),
                                           "company_id": company_id})
        if delete_credentials_if_possible and self.read_admin(email) is None and not self.read_recruiters(email):
            self.delete_credentials(email)

    def create_recruiter_invitation(self, recruiter_invitation: RecruiterInvitation, replace_if_exists: bool):
        def _random_token(token_len: int = 50) -> str:
            alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
            return ''.join(random.choice(alphabet) for _ in range(token_len))

        # This is ugly and should be changed in future
        while recruiter_invitation.invitation_token is None:
            recruiter_invitation.invitation_token = _random_token()
            if self.read_recruiter_invitation_by_token(recruiter_invitation.invitation_token) is not None:
                recruiter_invitation.invitation_token = None

        if replace_if_exists:
            self.recruiter_invitations.replace_one(
                filter={
                    "company_id": recruiter_invitation.company_id,
                    "canonized_email": recruiter_invitation.canonized_email
                },
                replacement=recruiter_invitation.as_bson_dict(),
                upsert=True
            )
        else:
            try:
                self.recruiter_invitations.insert_one(recruiter_invitation.as_bson_dict())
            except DuplicateKeyError:
                pass

    def read_recruiter_invitation_by_token(self, token: str) -> Optional[RecruiterInvitation]:
        obj = self.recruiter_invitations.find_one({"invitation_token": token})
        if obj is None:
            return None
        return RecruiterInvitation.from_dict(obj)

    def update_recruiter_invitation_sent_status(self, token: str, sent: bool):
        self.recruiter_invitations.update_one({"invitation_token": token}, {"invitation_sent": {"$set": sent}},
                                              upsert=False)

    def read_recruiter_invitations(self, email: str = None, company_id: str = None, invited_by: str = None,
                                   offset: int = 0, limit: int = None, expired: bool = None, sent: bool = None) \
            -> Generator[RecruiterInvitation, None, None]:
        filter_ = dict()
        if email is not None:
            filter_["canonized_email"] = Credentials.canonize_email(email)
        if company_id is not None:
            filter_["company_id"] = company_id
        if invited_by is not None:
            filter_["invited_by"] = invited_by
        if expired is not None:
            filter_["invited_at"] = {"$lt" if expired else "$gt": datetime.utcnow() - RecruiterInvitation.EXPIRED_AFTER}
        if sent is not None:
            filter_["invitation_sent"] = sent
        kwargs = dict()
        if offset and offset > 0:
            kwargs["skip"] = offset
        if limit and limit > 0:
            kwargs["limit"] = limit
        for obj in self.recruiter_invitations.find(filter_, **kwargs):
            yield RecruiterInvitation.from_dict(obj)

    def delete_recruiter_invitation_by_token(self, invitation_token: str):
        self.recruiter_invitations.delete_one({"invitation_token": invitation_token})

    def delete_one_recruiter_invitation(self, email: str, company_id: str):
        self.recruiter_invitations.delete_one({"canonized_email": Credentials.canonize_email(email),
                                               "company_id": company_id})

    def delete_expired_recruiter_invitations(self, older_than: timedelta = RecruiterInvitation.EXPIRED_AFTER):
        self.recruiter_invitations.delete_many({"created_at": {"$lt": datetime.utcnow() - older_than}})

    def read_all_admins(self) -> Generator[Admin, None, None]:
        for obj in self.admins.find():
            yield Admin.from_dict(obj)

    def read_all_recruiters(self) -> Generator[Recruiter, None, None]:
        for obj in self.recruiters.find():
            yield Recruiter.from_dict(obj)

    # ADMIN SESSION

    def create_session(self, email: str, expires_after=timedelta(days=1)) -> Session:
        def _random_token(token_len: int = 50):
            alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
            return ''.join(random.choice(alphabet) for _ in range(token_len))

        session = Session(
            email=email,
            token=_random_token(),
            expires_at=datetime.utcnow() + expires_after
        )

        self.sessions.insert_one(session.as_bson_dict())
        return session

    def read_session_by_token(self, token: str, renew_session: bool = True,
                              renew_threshold: timedelta = timedelta(minutes=10),
                              renew_expires_after: timedelta = timedelta(days=1)) \
            -> Optional[Session]:
        obj = self.sessions.find_one(filter={"token": token})
        if obj is None:
            return None

        session = Session.from_dict(obj)

        if renew_session and (datetime.utcnow() + renew_expires_after) - session.expires_at > renew_threshold:
            session.expires_at = datetime.utcnow() + renew_expires_after
            self.sessions.find_one_and_replace(
                filter={"token": token},
                replacement=session.as_bson_dict(),
                upsert=False
            )

        return session

    def delete_session_by_token(self, token: str):
        self.sessions.delete_one(filter={"token": token})

    def delete_all_sessions(self, email: str):
        self.sessions.delete_many(filter={"canonized_email": Credentials.canonize_email(email)})

    # USERS

    def update_user_tg_chat_id(self, tg_id: int, tg_chat_id: int) -> None:
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "tg_chat_id": tg_chat_id
                }
            },
            upsert=False
        )

    def update_user_tg_message_ids(self, tg_id: int, tg_settings_message_id: Optional[int] = None,
                                   tg_profile_message_id: Optional[int] = None,
                                   tg_subscriptions_message_id: Optional[int] = None,
                                   tg_feedback_message_id: Optional[int] = None,
                                   tg_notification_message_id: Optional[int] = None):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "tg_settings_message_id": tg_settings_message_id,
                    "tg_profile_message_id": tg_profile_message_id,
                    "tg_subscriptions_message_id": tg_subscriptions_message_id,
                    "tg_feedback_message_id": tg_feedback_message_id,
                    "tg_notification_message_id": tg_notification_message_id
                }
            },
            upsert=False
        )

    def update_user_tg_notification_message_id(self, tg_id: int, tg_notification_message_id: int):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "tg_notification_message_id": tg_notification_message_id
                }
            },
            upsert=False
        )

    def update_user_jobs_notifications_time(self, tg_id: int, last_daily_jobs_sent_at: datetime = None,
                                            last_weekly_jobs_sent_at: datetime = None):
        if last_daily_jobs_sent_at is None and last_weekly_jobs_sent_at is None:
            return
        update_statement = {"$set": {}}
        if last_daily_jobs_sent_at is not None:
            update_statement["$set"]["last_daily_jobs_sent_at"] = last_daily_jobs_sent_at
        if last_weekly_jobs_sent_at is not None:
            update_statement["$set"]["last_weekly_jobs_sent_at"] = last_weekly_jobs_sent_at
        self.users.find_one_and_update({"tg_id": tg_id}, update_statement, upsert=False)

    def upsert_user(self, user: User) -> None:
        def _random_referral_link(ref_link_len: int = 10) -> str:
            alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
            return ''.join(random.choice(alphabet) for _ in range(ref_link_len))

        # WARNING: This is ugly and has to be changed in future
        while user.referral_link_id is None:
            user.referral_link_id = _random_referral_link()
            if self.read_user_by_referral_link_id(user.referral_link_id) is not None:
                user.referral_link_id = None

        user.updated_at = datetime.utcnow()

        self.users.find_one_and_replace(
            filter={
                "tg_id": user.tg_id
            },
            replacement=user.as_bson_dict(),
            upsert=True
        )

    def save_user(self, user: User) -> None:
        self.upsert_user(user)

    def read_user(self, tg_id: int, create_if_doesnt_exist: bool = False) -> Optional[User]:
        obj = self.users.find_one({"tg_id": tg_id})
        if obj is None:
            if create_if_doesnt_exist:
                user = User.new_empty_user(tg_id=tg_id)
                self.upsert_user(user)
            else:
                return None
        else:
            user = User.from_dict(obj)

        return user

    def read_user_by_referral_link_id(self, referral_link_id: str) -> Optional[User]:
        obj = self.users.find_one({"referral_link_id": referral_link_id})
        if obj is None:
            return None
        return User.from_dict(obj)

    def update_user_last_interaction_time(self, tg_id: int, last_interaction_time: datetime = None):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "last_interaction_time": last_interaction_time or datetime.utcnow(),
                }
            },
            upsert=False
        )

    def update_user_last_feedback_sent_time(self, tg_id: int, last_user_feedback_sent_at: datetime = None):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "last_user_feedback_sent_at": last_user_feedback_sent_at or datetime.utcnow(),
                }
            },
            upsert=False
        )

    def update_user_last_profile_not_ready_notification_time(self, tg_id: int,
                                                             last_profile_not_ready_notification_time: datetime = None):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "last_profile_not_ready_notification_time":
                        last_profile_not_ready_notification_time or datetime.utcnow(),
                }
            },
            upsert=False
        )

    def delete_user(self, tg_id: int) -> None:
        self.delete_user_job_unsent_matches(user_tg_id=tg_id)
        self.user_cvs.delete_one({"tg_id": tg_id})
        self.users.delete_one({"tg_id": tg_id})
        self.delete_user_subscriptions(tg_id)

    def read_all_users(self) -> Generator[User, None, None]:
        for obj in self.users.find():
            yield User.from_dict(obj)

    def read_users_with_not_ready_profile(self, last_notification_min_delta: timedelta = None,
                                          last_interaction_min_delta: timedelta = None,
                                          max_profile_not_ready_notifications_sent: int = 3) \
            -> Generator[User, None, None]:
        last_notification_min_delta = last_notification_min_delta or timedelta(days=1)
        last_interaction_min_delta = last_interaction_min_delta or timedelta(days=1)

        filter_ = {
            "$and": [
                # Check that we can bother a user
                {"$or": [
                    {"last_interaction_time": {"$exists": False}},
                    {"last_interaction_time": {"$eq": None}},
                    {"last_interaction_time": {"$lt": datetime.utcnow() - last_interaction_min_delta}}
                ]},
                {"$or": [
                    {"last_profile_not_ready_notification_time": {"$exists": False}},
                    {"last_profile_not_ready_notification_time": {"$eq": None}},
                    {"last_profile_not_ready_notification_time":
                        {"$lt": datetime.utcnow() - last_notification_min_delta}}
                ]},
                {"user_notification_options": UserNotificationOption.BOT_FRIENDLY_REMINDERS.value},
                {"$or": [
                    {"profile_not_ready_notifications_sent": {"$exists": False}},
                    {"profile_not_ready_notifications_sent": {"$eq": None}},
                    {"profile_not_ready_notifications_sent": {"$lte": max_profile_not_ready_notifications_sent}}
                ]},
                # Checking different conditions
                {"$or": [
                    # Location is not sent
                    {"$and": [
                        {"$or": [
                            {"user_country": {"$exists": False}},
                            {"user_country": {"$eq": None}},
                        ]},
                        {"$or": [
                            {"user_country_location": {"$exists": False}},
                            {"user_country_location": {"$eq": None}},
                        ]},
                    ]},
                    # Job Positions are not set
                    {"desired_positions": {"$exists": False}},
                    {"desired_positions": {"$eq": None}},
                    {"desired_positions": {"$size": 0}},
                    # Missing profile data
                    {"missing_profile_parts": {"$exists": True, "$not": {"$size": 0}}}
                ]},
            ]
        }
        for obj in self.users.find(filter_):
            yield User.from_dict(obj)

    def update_user_profile_not_ready_notifications_sent(self, tg_id: int, profile_not_ready_notifications_sent: int):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "profile_not_ready_notifications_sent": profile_not_ready_notifications_sent
                }
            },
            upsert=False
        )

    def read_users_with_validation_status_update(self, update_max_delta: timedelta = timedelta(days=1),
                                                 user_statuses: Iterable[UserProfileValidationStatus] = None) \
            -> Generator[User, None, None]:
        filter_ = {
            "validation_status_update_sent": {"$eq": False},
            "validation_status_updated_at": {"$gt": datetime.utcnow() - update_max_delta}
        }
        if user_statuses is not None:
            filter_["validation_status"] = {"$in": [user_status.value for user_status in user_statuses]}
        for obj in self.users.find(filter_):
            yield User.from_dict(obj)

    def update_user_validation_status_update_info(self, tg_id: int, validation_status_update_sent: bool = True,
                                                  update_time: datetime = None):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "validation_status_update_sent": validation_status_update_sent,
                    "validation_status_updated_at": update_time or datetime.utcnow()
                }
            },
            upsert=False
        )

    def update_user_following_companies(self, tg_id: int, followed_companies_ids: List[str],
                                        followed_disabled_companies_ids: List[str]):
        self.users.update_one(
            filter={
                "tg_id": tg_id
            },
            update={
                "$set": {
                    "followed_companies_ids": followed_companies_ids,
                    "followed_disabled_companies_ids": followed_disabled_companies_ids
                }
            },
            upsert=False
        )

    # USER_CVS

    def read_user_cv(self, tg_id: int) -> Optional[UserCv]:
        obj = self.user_cvs.find_one({"tg_id": tg_id})
        if obj is None:
            return None
        return UserCv.from_dict(obj)

    def update_user_cv(self, tg_id: int, user_cv: UserCv):
        self.user_cvs.find_one_and_replace(
            filter={"tg_id": tg_id},
            replacement=user_cv.as_bson_dict(),
            upsert=True
        )

    def is_cv_uploaded(self, tg_id: int) -> bool:
        return self.user_cvs.count_documents(filter={"tg_id": tg_id}, limit=1) > 0

    def cv_latest_upload_date(self, tg_id: int) -> Optional[datetime]:
        obj = self.user_cvs.find_one(filter={"tg_id": tg_id}, projection=["upload_date"])
        if obj is None:
            return None
        else:
            return obj["upload_date"]

    # COMPANIES

    def upsert_company(self, company: Company) -> None:
        def _random_referral_link(ref_link_len: int = 10) -> str:
            alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
            return ''.join(random.choice(alphabet) for _ in range(ref_link_len))

        def _random_company_id(company_id_len: int = 10) -> str:
            alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
            return ''.join(random.choice(alphabet) for _ in range(company_id_len))

        # This is ugly and should be changed in future
        while company.referral_link_id is None:
            company.referral_link_id = _random_referral_link()
            if self.read_company_by_referral_link_id(company.referral_link_id) is not None:
                company.referral_link_id = None

        if company.company_id is None:
            while company.company_id is None:
                try:
                    company.company_id = _random_company_id()
                    self.companies.insert_one(company.as_bson_dict())
                except DuplicateKeyError:
                    company.company_id = None
        else:
            self.companies.find_one_and_replace(
                filter={"company_id": company.company_id},
                replacement=company.as_bson_dict(),
                upsert=True
            )

    def read_company(self, company_id: str) -> Optional[Company]:
        obj = self.companies.find_one({"company_id": company_id})
        if obj is None:
            return None
        return Company.from_dict(obj)

    def read_companies(self, company_ids: List[str]) -> List[Company]:
        result = []
        for company_raw in self.companies.find({"company_id": {"$in": company_ids}}):
            result.append(Company.from_dict(company_raw))
        return result

    def company_search(self, q: Optional[str] = None, company_ids: Optional[List[str]] = None, offset: int = 0,
                       limit: int = None) -> Generator[Company, None, None]:
        # TODO: Improve search in the future
        filter_ = {}
        if company_ids:
            filter_["company_id"] = {"$in": company_ids}
        allowed_characters = set(string.ascii_lowercase + "ǿšžœÿßőűḃċḋḟġṁṗṡṫşẁẃŵẅỳŷ" + "ðñòóôõöøùúûüýþÿêęa" +
                                 "абвгдеёжзиклмнопрстуфхцчшщыъьэюя")
        if q:
            q = "".join(ch for ch in q.strip().lower() if ch in allowed_characters)
        if q:
            filter_["company_name"] = {"$regex": q, "$options": "i"}
        kwargs = {}
        if limit:
            kwargs["limit"] = limit
        for company_raw in self.companies.find(filter_, skip=offset, sort=[("company_name", 1)], **kwargs):
            yield Company.from_dict(company_raw)

    def delete_company(self, company_id: str):
        for job in self.read_jobs_by_company_id(company_id):
            self.delete_user_job_matches(job_id=job.job_id)
            self.delete_user_job_applications(job_id=job.job_id)
        self.jobs.delete_many({"company_id": company_id})
        self.companies.delete_one({"company_id": company_id})
        self.admins.update_many(filter={"admin_permissions.recruiter_companies": company_id},
                                update={"$pull": {"admin_permissions.recruiter_companies": company_id},
                                        "$unset": {f"recruiter_permissions.{company_id}": ""}})
        self.recruiters.delete_many(filter={"company_id": company_id})
        self.recruiter_invitations.delete_many(filter={"company_id": company_id})
        self.delete_company_subscriptions(company_id)

    def read_all_companies(self) -> List[Company]:
        result = []
        for obj in self.companies.find():
            result.append(Company.from_dict(obj))
        return result

    def read_company_by_referral_link_id(self, referral_link_id: str) -> Optional[Company]:
        obj = self.companies.find_one({"referral_link_id": referral_link_id})
        if obj is None:
            return None
        return Company.from_dict(obj)

    def update_company_referral_link_id(self, company_id: str, referral_link_id: str):
        existing_company = self.read_company_by_referral_link_id(referral_link_id)

        # Warning: Race condition is possible here

        if existing_company is not None and existing_company.company_id != company_id:
            raise ValueError("Referral id is already in use by another company")

        if existing_company is None or existing_company.referral_link_id != referral_link_id:
            self.companies.update_one(
                filter={"company_id": company_id},
                update={"$set": {"referral_link_id": referral_link_id}},
                upsert=False
            )

    # JOBS

    def upsert_job(self, job: Job) -> None:
        def _random_job_id(job_id_len: int = 15) -> str:
            alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
            return ''.join(random.choice(alphabet) for _ in range(job_id_len))

        job.updated_at = datetime.utcnow()

        if job.job_id is None:
            job.created_at = datetime.utcnow()
            while job.job_id is None:
                try:
                    job.job_id = _random_job_id()
                    self.jobs.insert_one(job.as_bson_dict())
                except DuplicateKeyError:
                    job.job_id = None
        else:
            self.jobs.find_one_and_replace(
                filter={"job_id": job.job_id},
                replacement=job.as_bson_dict(),
                upsert=True
            )

    def read_job(self, job_id: str) -> Optional[Job]:
        obj = self.jobs.find_one({"job_id": job_id})
        if obj is None:
            return None
        return Job.from_dict(obj)

    def read_all_jobs(self, created_after: datetime = None,
                      job_posting_status: Union[JobPostingStatus, List[JobPostingStatus]] = None,
                      job_posting_validation_status: Union[JobPostingValidationStatus,
                                                           List[JobPostingValidationStatus]] = None) -> List[Job]:
        filter_ = {}
        if created_after:
            filter_["created_at"] = {"$gt": created_after}
        if job_posting_status is not None:
            if isinstance(job_posting_status, list):
                filter_["job_posting_status"] = {"$in": [jps.value for jps in job_posting_status]}
            else:
                filter_["job_posting_status"] = job_posting_status.value
        if job_posting_validation_status is not None:
            if isinstance(job_posting_validation_status, list):
                filter_["job_posting_validation_status"] = {"$in": [jps.value for jps in job_posting_validation_status]}
            else:
                filter_["job_posting_validation_status"] = job_posting_validation_status.value
        result = []
        for obj in self.jobs.find(filter_):
            result.append(Job.from_dict(obj))
        return result

    def delete_job(self, job_id: str):
        self.delete_user_job_unsent_matches(job_id=job_id)
        self.jobs.delete_one({"job_id": job_id})
        self.delete_user_job_unsent_matches(job_id=job_id)

    def archive_job(self, job_id: str):
        self.delete_user_job_unsent_matches(job_id=job_id)
        self.jobs.find_one_and_update(
            filter={"job_id": job_id},
            update={"$set": {"job_posting_status": JobPostingStatus.ARCHIVED.value}},
            upsert=False
        )
        self.delete_user_job_unsent_matches(job_id=job_id)

    def validate_job(self, job_id: str, validation_status: JobPostingValidationStatus,
                     delete_unsent_matches: bool = True):
        if delete_unsent_matches:
            self.delete_user_job_unsent_matches(job_id=job_id)
        self.jobs.find_one_and_update(
            filter={"job_id": job_id},
            update={"$set": {"job_posting_validation_status": validation_status.value}},
            upsert=False
        )
        if delete_unsent_matches:
            self.delete_user_job_unsent_matches(job_id=job_id)

    def read_jobs_by_company_id(self, company_id: str, limit: int = None, offset: int = 0) -> List[Job]:
        result = []
        kwargs = {"skip": offset}
        if limit:
            kwargs["limit"] = limit
        for obj in self.jobs.find({"company_id": company_id}, **kwargs):
            result.append(Job.from_dict(obj))
        return result

    # JOB STATISTICS

    def upsert_job_statistics(self, job_statistics: JobStatistics):
        self.job_statistics.find_one_and_replace(
            filter={"job_id": job_statistics.job_id},
            replacement=job_statistics.as_bson_dict(),
            upsert=True
        )

    def read_job_statistics(self, job_id: str) -> Optional[JobStatistics]:
        obj = self.job_statistics.find_one({"job_id": job_id})
        if obj is None:
            return None
        return JobStatistics.from_dict(obj)

    def read_multiple_job_statistics(self, job_ids: List[str]) -> List[JobStatistics]:
        if not job_ids:
            return []
        result = []
        for obj in self.job_statistics.find({"job_id": {"$in": job_ids}}):
            result.append(JobStatistics.from_dict(obj))
        return result

    # USER JOB MATCHES

    def create_user_job_match(self, user_job_match: UserJobMatch, replace_if_exists: bool = False):
        if not replace_if_exists:
            try:
                self.user_job_matches.insert_one(user_job_match.as_bson_dict())
            except DuplicateKeyError:
                pass
        else:
            self.user_job_matches.find_one_and_replace(
                filter={"user_tg_id": user_job_match.user_tg_id, "job_id": user_job_match.job_id},
                replacement=user_job_match.as_bson_dict(),
                upsert=True
            )

    def read_user_job_match_by_hash(self, user_job_match_hash: str) -> Optional[UserJobMatch]:
        raw_match = self.user_job_matches.find_one({"match_hash": user_job_match_hash})
        if raw_match is None:
            return None
        return UserJobMatch.from_dict(raw_match)

    def read_user_job_matches(self, user_tg_id: int = None, user_tg_ids: List[int] = None, job_id: str = None,
                              no_older_than: Optional[timedelta] = timedelta(days=7),
                              daily_notification_sent: Optional[bool] = None) -> Generator[UserJobMatch, None, None]:
        filter_ = {}
        if user_tg_id is not None and user_tg_ids is not None:
            raise ValueError("Either user_tg_id or user_tg_ids should be set at one moment")
        if no_older_than is not None:
            filter_["matched_on"] = {"$gt": datetime.utcnow() - no_older_than}
        if user_tg_ids is not None:
            filter_["user_tg_id"] = {"$in": user_tg_ids}
        if user_tg_id is not None:
            filter_["user_tg_id"] = user_tg_id
        if job_id is not None:
            filter_["job_id"] = job_id
        if daily_notification_sent is not None:
            filter_["daily_notification_sent"] = daily_notification_sent
        for obj in self.user_job_matches.find(filter_):
            yield UserJobMatch.from_dict(obj)

    def create_user_subscription(self, user_subscription: UserSubscription, replace_if_exists: bool = False):
        if replace_if_exists:
            self.user_subscriptions.find_one_and_replace({
                "user_tg_id": user_subscription.user_tg_id,
                "company_id": user_subscription.company_id
            }, replacement=user_subscription.as_bson_dict(), upsert=True)
        else:
            try:
                self.user_subscriptions.insert_one(user_subscription.as_bson_dict())
            except DuplicateKeyError:
                pass

    def read_one_user_subscription(self, user_tg_id: int = None, company_id: str = None) -> Optional[UserSubscription]:
        obj = self.user_subscriptions.find_one({"user_tg_id": user_tg_id, "company_id": company_id})
        if obj is None:
            return None
        return UserSubscription.from_dict(obj)

    def read_user_subscriptions(self, user_tg_id: int = None, company_id: str = None, muted: bool = None,
                                offset: int = None, limit: int = None) -> Generator[UserSubscription, None, None]:
        filter_ = {}
        if user_tg_id:
            filter_["user_tg_id"] = user_tg_id
        if company_id:
            filter_["company_id"] = company_id
        if muted is not None:
            if not muted:
                filter_["$or"] = [
                    {"muted_until": None},
                    {"muted_until": {"$lt": datetime.utcnow()}}
                ]
            else:
                filter_["$or"] = [
                    {"muted_until": {"$ne": None}},
                    {"muted_until": {"$gt": datetime.utcnow()}}
                ]
        kwargs = {}
        if offset:
            kwargs["skip"] = offset
        if limit:
            kwargs["limit"] = limit
        for obj in self.user_subscriptions.find(filter_, **kwargs):
            yield UserSubscription.from_dict(obj)

    def delete_one_user_subscription(self, user_tg_id: int, company_id: str):
        self.user_subscriptions.delete_one({"user_tg_id": user_tg_id, "company_id": company_id})

    def delete_user_subscriptions(self, user_tg_id: int):
        self.user_subscriptions.delete_many({"user_tg_id": user_tg_id})

    def delete_company_subscriptions(self, company_id: str):
        self.user_subscriptions.delete_many({"company_id": company_id})

    def remove_from_subscription(self, user_tg_id: int, company_id: str, job_category: JobCategory = None,
                                 job_position: JobPosition = None):
        assert job_position or job_category
        update = {}
        if job_category:
            update["job_categories"] = {"$pull": job_category.value}
        if job_position:
            update["job_positions"] = {"$pull": job_category.value}
        self.user_subscriptions.update_one({"user_tg_id": user_tg_id, "company_id": company_id}, update=update)

    def add_to_subscription(self, user_tg_id: int, company_id: str, job_category: JobCategory = None,
                            job_position: JobPosition = None):
        assert job_position or job_category
        update = {}
        if job_category:
            update["job_categories"] = {"$addToSet": job_category.value}
        if job_position:
            update["job_positions"] = {"$addToSet": job_category.value}
        self.user_subscriptions.update_one({"user_tg_id": user_tg_id, "company_id": company_id}, update=update)

    def read_matching_user_subscriptions(self, job: Job) -> Generator[UserSubscription, None, None]:
        or_ = [
            {"all_matching_positions": {"$eq": True}},
            {"job_positions": job.position.value}
        ]
        for job_category in JOBS_TREE_INVERTED[job.position]:
            or_.append({"job_categories": job_category.value})
        filter_ = {
            "$and": [
                {"company_id": job.company_id},
                {"$or": or_}
            ]
        }
        for obj in self.user_subscriptions.find(filter_):
            yield UserSubscription.from_dict(obj)

    def mute_user_subscription(self, user_tg_id: int, company_id: str, mute_for: timedelta):
        self.user_subscriptions.update_one(
            filter={"user_tg_id": user_tg_id, "company_id": company_id},
            update={"$set": {"muted_until": datetime.utcnow() + mute_for}},
            upsert=False
        )

    def unmute_user_subscription(self, user_tg_id: int, company_id: str):
        self.user_subscriptions.update_one(
            filter={"user_tg_id": user_tg_id, "company_id": company_id},
            update={"$set": {"muted_until": None}},
            upsert=False
        )

    def calc_job_stats(self, job_id: str) -> JobStatistics:
        stats = JobStatistics.empty_statistics(job_id=job_id)
        date_now = datetime.utcnow()
        result = self.user_job_matches.aggregate(
            pipeline=[
                {"$match": {"job_id": job_id}},
                {"$group": {"_id": {
                    "match_type": "$match_type",
                    "match_source": "$match_source",
                    "date": {"$dateToString": {"format": "%Y-%m-%d", "date": "$matched_on"}},
                    "daily_notification_sent": "$daily_notification_sent",
                    "daily_notification_sent_at": {"$dateToString": {"format": "%Y-%m-%d",
                                                                     "date": "$daily_notification_sent_at"}},
                    "weekly_notification_sent": "$weekly_notification_sent",
                    "weekly_notification_sent_at": {"$dateToString": {"format": "%Y-%m-%d",
                                                                      "date": "$weekly_notification_sent_at"}}
                }, "count": {"$sum": 1}}}
            ]
        )
        chart_matches_last_30_days_counters = defaultdict(lambda: Counter())
        chart_daily_notifications_sent_last_30_days_counters = defaultdict(lambda: Counter())
        chart_weekly_notifications_sent_last_30_days_counters = defaultdict(lambda: Counter())
        for obj in result:
            count = obj["count"]
            match_type = UserJobMatchType.parse(obj["_id"]["match_type"])
            match_source = UserJobMatchSource.parse(obj["_id"].get("match_source", UserJobMatchSource.JOB_MATCH))
            d = datetime.strptime(obj["_id"]["date"], "%Y-%m-%d")
            stats.counter_total_matches[match_type] += count
            if obj["_id"]["daily_notification_sent"]:
                stats.counter_total_daily_notification_sent[match_type] += count
            if obj["_id"]["weekly_notification_sent"]:
                stats.counter_total_weekly_notification_sent[match_type] += count
            if d > date_now - timedelta(days=31):
                chart_matches_last_30_days_counters[match_type][d] += count
                if obj["_id"]["daily_notification_sent_at"] is not None:
                    dnt = datetime.strptime(obj["_id"]["daily_notification_sent_at"], "%Y-%m-%d")
                    chart_daily_notifications_sent_last_30_days_counters[match_type][dnt] += count
                if obj["_id"]["weekly_notification_sent_at"] is not None:
                    wnt = datetime.strptime(obj["_id"]["weekly_notification_sent_at"], "%Y-%m-%d")
                    chart_weekly_notifications_sent_last_30_days_counters[match_type][wnt] += count
        for match_type in UserJobMatchType:
            stats.chart_matches_last_30_days[match_type] \
                = [(k, v) for k, v in chart_matches_last_30_days_counters[match_type].items()]
            stats.chart_daily_notifications_sent_last_30_days[match_type] \
                = [(k, v) for k, v in chart_daily_notifications_sent_last_30_days_counters[match_type].items()]
            stats.chart_weekly_notifications_sent_last_30_days[match_type] \
                = [(k, v) for k, v in chart_weekly_notifications_sent_last_30_days_counters[match_type].items()]
        result = self.user_job_applications.aggregate(
            pipeline=[
                {"$match": {"job_id": job_id}},
                {"$group": {"_id": {
                    "user_job_application_status": "$user_job_application_status",
                    "user_job_application_method": "$user_job_application_method",
                    "date": {"$dateToString": {"format": "%Y-%m-%d", "date": "$created_at"}}
                }, "count": {"$sum": 1}}}
            ]
        )
        chart_applications_by_status_last_30_days_counters = defaultdict(lambda: Counter())
        chart_applications_by_method_last_30_days_counters = defaultdict(lambda: Counter())
        for obj in result:
            count = obj["count"]
            application_status = UserJobApplicationStatus.parse(obj["_id"]["user_job_application_status"])
            application_method = UserJobApplicationMethod.parse(obj["_id"]["user_job_application_method"])
            d = datetime.strptime(obj["_id"]["date"], "%Y-%m-%d")
            stats.counter_total_applications_by_status[application_status] += count
            stats.counter_total_applications_by_method[application_method] += count
            if d > date_now - timedelta(days=31):
                chart_applications_by_status_last_30_days_counters[application_status][d] += count
                chart_applications_by_method_last_30_days_counters[application_method][d] += count
        for application_status in UserJobApplicationStatus:
            stats.chart_applications_by_status_last_30_days[application_status] \
                = [(k, v) for k, v in chart_applications_by_status_last_30_days_counters[application_status].items()]
        for application_method in UserJobApplicationMethod:
            stats.chart_applications_by_method_last_30_days[application_method] \
                = [(k, v) for k, v in chart_applications_by_method_last_30_days_counters[application_method].items()]

        return stats

    def delete_user_job_matches(self, user_tg_id: int = None, job_id: int = None):
        filter_ = dict()
        if user_tg_id is not None:
            filter_["user_tg_id"] = user_tg_id
        if job_id is not None:
            filter_["job_id"] = job_id
        self.user_job_matches.delete_many(filter_)

    def delete_user_job_unsent_matches(self, user_tg_id: int = None, job_id: str = None):
        filter_ = {
            "daily_notification_sent": False,
            "weekly_notification_sent": False
        }
        if user_tg_id is not None:
            filter_["user_tg_id"] = user_tg_id
        if job_id is not None:
            filter_["job_id"] = job_id
        self.user_job_matches.delete_many(filter_)

    def update_user_job_match_sent_status(self, user_tg_id: int, job_id: str, daily_notification_sent: bool = None,
                                          daily_notification_sent_at: Optional[datetime] = None,
                                          weekly_notification_sent: bool = None,
                                          weekly_notification_sent_at: Optional[datetime] = None):
        if daily_notification_sent is None and weekly_notification_sent is None:
            return
        if daily_notification_sent and daily_notification_sent_at is None:
            raise ValueError("daily_notification_sent_at should not be None")
        if weekly_notification_sent and weekly_notification_sent_at is None:
            raise ValueError("weekly_notification_sent_at should not be None")
        filter_ = {
            "user_tg_id": user_tg_id,
            "job_id": job_id
        }
        update_statement = {"$set": {}}
        if daily_notification_sent is not None:
            update_statement["$set"]["daily_notification_sent"] = daily_notification_sent
            update_statement["$set"]["daily_notification_sent_at"] = daily_notification_sent_at
        if weekly_notification_sent is not None:
            update_statement["$set"]["weekly_notification_sent"] = weekly_notification_sent
            update_statement["$set"]["weekly_notification_sent_at"] = weekly_notification_sent_at
        self.user_job_matches.find_one_and_update(filter_, update_statement, upsert=False)

    # USER JOB APPLICATIONS

    def create_user_job_application(self, user_job_application: UserJobApplication, replace_if_exists: bool = False):
        if not replace_if_exists:
            try:
                self.user_job_applications.insert_one(user_job_application.as_bson_dict())
            except DuplicateKeyError:
                pass
        else:
            self.user_job_applications.find_one_and_replace(
                filter={"user_tg_id": user_job_application.user_tg_id, "job_id": user_job_application.job_id},
                replacement=user_job_application.as_bson_dict(),
                upsert=True
            )

    def read_user_job_applications(self, user_tg_id: int = None, job_id: str = None, limit: int = None, offset: int = 0,
                                   no_older_than: Optional[timedelta] = timedelta(days=30)) \
            -> Generator[UserJobApplication, None, None]:
        filter_ = dict()
        if no_older_than is not None:
            filter_["created_at"] = {"$gt": datetime.utcnow() - no_older_than}
        if user_tg_id is not None:
            filter_["user_tg_id"] = user_tg_id
        if job_id is not None:
            filter_["job_id"] = job_id
        kwargs = {}
        if limit:
            kwargs["limit"] = limit
        for obj in self.user_job_applications.find(filter_, skip=offset, sort=[("created_at", -1)], **kwargs):
            yield UserJobApplication.from_dict(obj)

    def user_job_applications_count(self, user_tg_id: int = None, job_id: str = None,
                                    no_older_than: Optional[timedelta] = timedelta(days=30)) -> int:
        filter_ = dict()
        if no_older_than is not None:
            filter_["created_at"] = {"$gt": datetime.utcnow() - no_older_than}
        if user_tg_id is not None:
            filter_["user_tg_id"] = user_tg_id
        if job_id is not None:
            filter_["job_id"] = job_id
        return int(self.user_job_applications.count_documents(filter_))

    def delete_user_job_applications(self, user_tg_id: int = None, job_id: int = None):
        filter_ = dict()
        if user_tg_id is not None:
            filter_["user_tg_id"] = user_tg_id
        if job_id is not None:
            filter_["job_id"] = job_id
        self.user_job_applications.delete_many(filter_)

    # FEEDBACK

    def add_user_feedback(self, feedback: UserFeedback):
        self.feedback.insert_one(feedback.as_bson_dict())
