from threading import Lock
from typing import Optional

from .user_dao import UserDao

from jobsbot.settings import get_env
from jobsbot.settings.mongodb import get_mongodb_connection_params
from jobsbot.core.logging import get_logger

__GET_DAO_LOCK = Lock()

__USER_DAO: Optional[UserDao] = None


def get_user_dao() -> UserDao:
    with __GET_DAO_LOCK:
        global __USER_DAO
        if __USER_DAO is None:
            logger = get_logger()
            env = get_env()
            logger.info(f"Creating USER_DAO object for {env.name} environment...")
            params = get_mongodb_connection_params(env)
            logger.info(f"Using next connection params: {params}")
            __USER_DAO = UserDao(connection_params=params)
    return __USER_DAO
