import argparse

from pymongo import MongoClient
from pymongo.database import Database

from jobsbot.dao.user_dao import UserDao


def create_collections(db: Database, users_collection_name: str, user_cvs_collection_name: str):
    db.create_collection(users_collection_name)
    db.create_collection(user_cvs_collection_name)


def create_indexes(db, users_collection_name: str, user_cvs_collection_name: str):
    users_collection = db[users_collection_name]

    users_collection.create_index(("tg_id", 1), unique=True)
    users_collection.create_index(("tg_chat_id", 1), unique=True)
    users_collection.create_index(("referral_link_id", 1), unique=True)

    user_cvs_collection = db[user_cvs_collection_name]

    user_cvs_collection.create_index(("tg_id", 1), unique=True)


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "jobsbot"

    _default_users_collection_name = UserDao.USERS_COLLECTION
    _default_user_cvs_collection_name = UserDao.USERS_CVS_COLLECTION

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--users_collection_name", required=False, default=_default_users_collection_name,
                        help=f"Users collection. Default: {_default_users_collection_name}", type=str)

    parser.add_argument("--user_cvs_collection_name", required=False, default=_default_user_cvs_collection_name,
                        help=f"User CVs collection. Default: {_default_user_cvs_collection_name}", type=str)

    return parser.parse_args()


def setup_mongodb_event_dao(host, port, dbname, users_collection_name, user_cvs_collection_name):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_collections(db, users_collection_name, user_cvs_collection_name)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, users_collection_name, user_cvs_collection_name)
    print("Done.")

    client.close()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_event_dao(args.host, args.port, args.database, args.users_collection_name,
                            args.user_cvs_collection_name)
