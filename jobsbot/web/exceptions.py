from werkzeug.exceptions import HTTPException

from jobsbot.core.exceptions import JobsBotException


class JobsBotApiException(HTTPException, JobsBotException):
    pass


class BadRequestException(JobsBotApiException):
    code = 400
    description = "Bad request."


class BadRequestBodyException(JobsBotApiException):
    code = 400
    description = "Can not parse request body."


class NoTokenException(JobsBotApiException):
    code = 401
    description = "No token provided."


class WrongTokenException(JobsBotApiException):
    code = 403
    description = "Token is wrong."


class TokenHasNoPermissionException(JobsBotApiException):
    code = 403
    description = "Token has no permission to call this API method."


class WrongCredentialsException(JobsBotApiException):
    code = 403
    description = "Email and/or password is wrong."


class NoAccessToResource(JobsBotApiException):
    code = 403
    description = "User has no access to this resource"


class ObjectNotFoundException(JobsBotApiException):
    code = 404
    description = "Can not find an object."


# class OutOfRequestsLimitException(JobsBotApiException):
#     code = 429


class InternalError(JobsBotApiException):
    code = 500
    description = "Unexpected internal error happened. We have already started digging into the problem."


class MethodNotImplemented(JobsBotApiException):
    code = 501
    description = "The method is not implemented yet"
