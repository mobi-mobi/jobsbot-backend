from jobsbot.web.api.admin import AdminApi
from jobsbot.web.api.go import GoApi


def admin_api():
    api = AdminApi("admin_api")
    return api.app


def go_api():
    api = GoApi("go_api")
    return api.app
