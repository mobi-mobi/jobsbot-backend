from datetime import datetime
from flask import redirect
from typing import List, Optional, Tuple

from jobsbot.core import ApiPermission, UserJobApplicationMethod, UserJobApplicationStatus
from jobsbot.core.logging import get_logger
from jobsbot.core.models import UserJobApplication, Recruiter
from jobsbot.dao import get_user_dao
from jobsbot.web.api.common import TokenizedApi, TokenizedApiPermissions, TokenizedApiPermissionRequirements


class GoApi(TokenizedApi):
    def __init__(self, name: str):
        super().__init__(name)
        self.app.secret_key = "befbb3a56871a76f7267c0018cc36c9f0c59eb1c"

        # API METHODS

        self.register_callback("/a/<match_hash>", self.apply_for_a_job_online, should_have_a_valid_token=False,
                               methods=["GET"])

        self.register_callback("/ir/<recruiter_invitation_hash>", self.recruiter_accepted_invitation,
                               should_have_a_valid_token=False, methods=["GET"])

    @classmethod
    def get_session_expiration_date_and_email_by_token(cls, token: str) -> Optional[Tuple[datetime, str]]:
        return None

    def get_permissions_by_email(self, email: str) -> Optional[List[ApiPermission]]:
        return None

    def are_permission_requirements_met(self, permissions: TokenizedApiPermissions = None,
                                        permission_requirements: TokenizedApiPermissionRequirements = None) -> bool:
        return True

    @classmethod
    def _html_special_chars(cls, text):
        return text.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")

    @classmethod
    def apply_for_a_job_online(cls, match_hash: Optional[str]):
        logger = get_logger()
        match = get_user_dao().read_user_job_match_by_hash(match_hash)
        if match is None:
            raise Exception("The link has expired")
        job = get_user_dao().read_job(match.job_id)
        if job is None or not job.job_url:
            raise Exception("The link is broken")
        user_job_applications = list(get_user_dao().read_user_job_applications(match.user_tg_id, job.job_id,
                                                                               no_older_than=None))
        if not user_job_applications:
            logger.info(f"Created a new application for user {match.user_tg_id} and job {job.job_id} ({job.title})")
            get_user_dao().create_user_job_application(
                UserJobApplication(
                    user_tg_id=match.user_tg_id,
                    job_id=job.job_id,
                    created_at=datetime.utcnow(),
                    last_time_applied_at=datetime.utcnow(),
                    user_job_application_status=UserJobApplicationStatus.NOT_REVIEWED,
                    times_applied=1,
                    user_job_application_method=UserJobApplicationMethod.URL_REDIRECT
                )
            )
        else:
            user_job_application = user_job_applications[0]
            if user_job_application.user_job_application_method == UserJobApplicationMethod.URL_REDIRECT \
                    and user_job_application.times_applied < 10:
                logger.info(f"Renewed a application for user {match.user_tg_id} and job {job.job_id} ({job.title})")
                user_job_application.last_time_applied_at = datetime.utcnow()
                user_job_application.times_applied += 1
                get_user_dao().create_user_job_application(user_job_application, replace_if_exists=True)
        return redirect(job.job_url, code=302)


    def recruiter_accepted_invitation(self, recruiter_invitation_token):
        logger = get_logger()
        user_dao = get_user_dao()

        invitation = user_dao.read_recruiter_invitation_by_token(recruiter_invitation_token)
        if invitation is None:
            return redirect("http://admin.jobsbot.io/recruiter/invitation/error/bad_invitation", code=302)

        company = user_dao.read_company(invitation.company_id)
        if invitation.expired or company is None:
            return redirect("http://admin.jobsbot.io/recruiter/invitation/error/bad_invitation", code=302)

        if user_dao.read_one_recruiter(email=invitation.email, company_id=company.company_id) is not None:
            return redirect("http://admin.jobsbot.io/recruiter/invitation/error/bad_invitation", code=302)

        credentials = user_dao.read_credentials(invitation.email)
        if not credentials:
            logger.info(f"Recruiter {invitation.name} accepted an invitation to join {company.company_name}, but "
                        f"credentials do not exists - redirecting to the credentials form")
            return redirect(f"http://admin.jobsbot.io/recruiter/invitation/set-credentials"
                            f"?token={invitation.invitation_token}&company={company.company_name}", code=302)

        recruiter = Recruiter.from_recruiter_invitation(invitation)
        user_dao.create_recruiter(recruiter, replace_if_exists=False)
        user_dao.delete_recruiter_invitation_by_token(invitation.invitation_token)
        logger.info(f"Successfully added new recruiter {invitation.name} to company {company.company_name}, as "
                    f"this recruiter already had credentials")

        return redirect(f"http://admin.jobsbot.io/recruiter/invitation/sucess?company={company.company_name}", code=302)
