from base64 import b64encode
from collections import Counter
from datetime import datetime, timedelta

from jobsbot.core.enums import UserProfileValidationStatus, UserJobMatchType, UserJobApplicationMethod, \
    UserJobApplicationStatus
from jobsbot.core.models import Company, Job, User, UserCv, Recruiter, JobStatistics, UserJobApplication,\
    UserJobMatch, RecruiterInvitation


def candidate_as_api_json(candidate: User) -> dict:
    country = candidate.user_country
    if country is not None:
        country = country.title()

    city = candidate.user_country_location
    if city is not None:
        city = city.title()

    return {
        "name": candidate.name,
        "tg_id": candidate.tg_id,
        "email": candidate.email,
        "country": country,
        "city": city,
        "current_company": candidate.current_company_name,
        "current_position": candidate.current_position_title,
        "experience": None if candidate.experience is None else candidate.experience.name,
        "created_at": User._datetime_to_str(candidate.created_at),
        "updated_at": User._datetime_to_str(candidate.updated_at),
        "validation_status": candidate.validation_status.name,
        "ready_for_validation": candidate.validation_status == UserProfileValidationStatus.WAITING_VERIFICATION,
        # and candidate.updated_at < datetime.utcnow() - timedelta(hours=1),
        "validation_rejection_reasons": [r.name for r in candidate.validation_rejection_reasons],
        "changed_after_last_validation_profile_parts": [p.name for p in
                                                        candidate.changed_after_last_validation_profile_parts]
    }


def candidate_anonymous_as_api_json(candidate: User) -> dict:
    candidate_as_json = candidate_as_api_json(candidate)
    return {
        **candidate_as_json,
        "tg_id": 0,
        "name": None if candidate.name is None else candidate.name[0:1] + "***",
        "email": None if candidate.email is None else candidate.email[0:1] + "***",
    }


def candidate_cv_as_json(candidate_cv: UserCv) -> dict:
    return {
        "file_base64": b64encode(candidate_cv.file).decode('utf-8'),
        "upload_date": candidate_cv.upload_date.strftime(UserCv.DATE_FORMAT),
        "filename": candidate_cv.filename
    }


def company_as_json(company: Company) -> dict:
    return company.as_json_dict()


def job_as_json(job: Job) -> dict:
    return {
        **job.as_json_dict(),
        "position": job.position.name,
        "min_experience": job.min_experience.name,
        "job_posting_status": job.job_posting_status.name,
        "job_posting_validation_status": job.job_posting_validation_status.name,
        "must_have_skills": [s.name for s in job.must_have_skills],
        "better_to_have_skills": [s.name for s in job.better_to_have_skills],
        "locations": [loc.name for loc in job.locations],
        "remote_work_from_countries": [c.name for c in job.remote_work_from_countries],
        "relocation_from_countries": [c.name for c in job.relocation_from_countries]
    }


def job_statistics_as_json(job_statistics: JobStatistics) -> dict:
    json = job_statistics.as_json_dict()

    for chart_name, chart_data, enum_class in (
            ["chart_matches_last_30_days", job_statistics.chart_matches_last_30_days, UserJobMatchType],
            ["chart_daily_notifications_sent_last_30_days", job_statistics.chart_daily_notifications_sent_last_30_days,
             UserJobMatchType],
            ["chart_weekly_notifications_sent_last_30_days",
             job_statistics.chart_weekly_notifications_sent_last_30_days, UserJobMatchType],
            ["chart_applications_by_method_last_30_days", job_statistics.chart_applications_by_method_last_30_days,
             UserJobApplicationMethod],
            ["chart_applications_by_status_last_30_days", job_statistics.chart_applications_by_status_last_30_days,
             UserJobApplicationStatus]
    ):
        json[chart_name] = dict()
        for enum_value in enum_class:
            json[chart_name][enum_value.name] = []
            chart_data_list = chart_data.get(enum_value, [])
            c = Counter()
            for dt, value in chart_data_list:
                c[dt.strftime("%Y-%m-%d")] += value
            for d in range(0, 30):
                dd = datetime.utcnow() - timedelta(days=d)
                k = dd.strftime("%Y-%m-%d")
                json[chart_name][enum_value.name].append([k, c[k]])

    del json["counter_total_matches"]["NO_MATCH"]
    del json["chart_matches_last_30_days"]["NO_MATCH"]
    del json["chart_daily_notifications_sent_last_30_days"]["NO_MATCH"]
    del json["chart_weekly_notifications_sent_last_30_days"]["NO_MATCH"]
    del json["counter_total_daily_notification_sent"]["NO_MATCH"]
    del json["counter_total_weekly_notification_sent"]["NO_MATCH"]

    return json


def recruiter_as_json(recruiter: Recruiter) -> dict:
    result = recruiter.as_json_dict()
    for f in ("canonized_email", ):
        if f in result:
            del result[f]
    result["recruiter_permissions"] = {p.name: p.title() for p in recruiter.recruiter_permissions}
    return result


def recruiter_invitation_as_json(recruiter_invitation: RecruiterInvitation) -> dict:
    result = recruiter_invitation.as_json_dict()
    for f in ("canonized_email", ):
        if f in result:
            del result[f]
    result["recruiter_permissions"] = {p.name: p.title() for p in recruiter_invitation.recruiter_permissions}
    return result


def user_job_application_as_json(user_job_application: UserJobApplication) -> dict:
    result = user_job_application.as_json_dict()
    result["user_job_application_method"] = user_job_application.user_job_application_method.name
    result["user_job_application_status"] = user_job_application.user_job_application_status.name
    return result


def job_match_as_json(job_match: UserJobMatch) -> dict:
    result = job_match.as_json_dict()
    result["match_type"] = job_match.match_type.name
    return result
