import json
import time
import traceback


from abc import ABC, abstractmethod
from datetime import datetime
from flask_cors import CORS
from flask import Flask, g, request
from typing import Callable, List, Optional, Tuple, Union
from werkzeug.exceptions import HTTPException

from jobsbot.core.logging import get_logger
from jobsbot.dao import get_user_dao, UserDao
from jobsbot.web.api.parse import uri
from jobsbot.web.exceptions import BadRequestBodyException, JobsBotApiException, InternalError, NoTokenException, \
    TokenHasNoPermissionException, WrongTokenException


class TokenizedApiPermissions:
    pass


class TokenizedApiPermissionRequirements:
    pass


class TokenizedApi(ABC):
    def __init__(self, name: str):
        self.app = Flask(name)
        CORS(self.app, resources={r"/*": {"origins": "*"}}, send_wildcard=True)
        self.app.config['CORS_HEADERS'] = 'Content-Type'

        self.app.register_error_handler(Exception, self.generic_exception_handler)

        self.app.before_request(self.load_user_permissions)

        self.app.after_request(self.after_request_hook)
        self.app.before_request(self.before_request_hook)

        self.register_callback("/api/v1/status", self.status, should_have_a_valid_token=False)

    @property
    def users_dao(self) -> UserDao:
        return get_user_dao()

    def status(self):
        return self.format_result()

    @abstractmethod
    def are_permission_requirements_met(self, permissions: TokenizedApiPermissions = None,
                                        permission_requirements: TokenizedApiPermissionRequirements = None) -> bool:
        pass

    def register_callback(self, rule: str, view_func: Callable, should_have_a_valid_token: bool = True,
                          permission_requirements: Optional[TokenizedApiPermissionRequirements] = None,
                          methods: List[str] = None):
        def callback(*args, **kwargs):
            if should_have_a_valid_token:
                if not g.is_token_provided:
                    raise NoTokenException()
                if not g.is_token_valid:
                    raise WrongTokenException()
            if permission_requirements:
                if not self.are_permission_requirements_met(
                    permissions=g.permissions,
                    permission_requirements=permission_requirements
                ):
                    raise TokenHasNoPermissionException()
            return view_func(*args, **kwargs)
        self.app.add_url_rule(rule, view_func.__name__, callback, methods=methods or ["GET"])

    @classmethod
    @abstractmethod
    def get_session_expiration_date_and_email_by_token(cls, token: str) -> Optional[Tuple[datetime, str]]:
        pass

    @abstractmethod
    def get_permissions_by_email(self, email: str) -> Optional[TokenizedApiPermissions]:
        pass

    def load_user_permissions(self):
        g.token = None
        g.session_email = None
        g.is_token_provided = False
        g.is_token_valid = False
        g.permissions = None
        g.company_id = uri.read_str("company_id", required=False)
        if not g.company_id and uri.read_str("job_id", required=False):
            job_id = uri.read_str("job_id", required=False)
            job = get_user_dao().read_job(job_id)
            if job:
                g.company_id = job.company_id

        cookies_token_header = request.cookies.get('Token')
        if cookies_token_header:
            auth_token = cookies_token_header
        else:
            auth_token_header = request.headers.get("Authorization")

            if auth_token_header is None:
                return

            try:
                authorization_type, auth_token = auth_token_header.split(" ")
            except ValueError:
                return

            if authorization_type != "Bearer" or not auth_token:
                return

        g.token = auth_token
        g.is_token_provided = True

        session_data = self.get_session_expiration_date_and_email_by_token(auth_token)

        if session_data is None:
            return None

        session_expires_at, session_email = session_data
        g.session_email = session_email

        if session_expires_at is None or session_expires_at < datetime.utcnow():
            return

        g.is_token_valid = True
        permissions = self.get_permissions_by_email(session_email)

        if permissions is None:
            return

        g.permissions = permissions

    @property
    def session_email(self) -> Optional[str]:
        return g.session_email

    @property
    def user_permissions(self) -> Optional[TokenizedApiPermissions]:
        return g.permissions

    @property
    def company_id(self) -> Optional[str]:
        return g.company_id

    @classmethod
    def before_request_hook(cls):
        g.request_start_time = time.time()
        g.request_url = request.url

    @classmethod
    def after_request_hook(cls, response):
        return response

    @classmethod
    def format_result(cls, data: Optional[Union[dict, list]] = None,
                      meta: Optional[Union[dict, list]] = None,
                      exception: Optional[Exception] = None):
        code = 200
        body = {
            "status": "OK"
        }
        if meta is not None:
            body["meta"] = meta

        if data is not None:
            body["data"] = data

        if exception:
            body["status"] = "ERROR"
            log_traceback = isinstance(exception, HTTPException) and exception.code != 404
            log_traceback = log_traceback or isinstance(exception, InternalError)
            log_traceback = log_traceback or (not (isinstance(exception, JobsBotApiException)
                                                   or isinstance(exception, HTTPException)))

            try:
                code = exception.code
                description = exception.description
            except AttributeError:
                code = InternalError.code
                description = InternalError.description

            if log_traceback:
                tb_str = "".join(traceback.format_tb(exception.__traceback__))
                get_logger().error(f"{code} {request.url} {exception}\n{tb_str}")
            else:
                get_logger().error(f"{code} {request.url} {exception}")

            body["error"] = {
                "code": code,
                "description": description
            }

        return json.dumps(body), code, {'Content-Type': 'application/json; charset=utf-8'}

    def generic_exception_handler(self, e: Exception):
        return self.format_result(None, None, e)

    @classmethod
    def get_post_data(cls, max_content_length=1024 * 1024, throw_exception_if_empty: bool = True) -> Optional[bytes]:
        if request.content_length is not None and request.content_length > max_content_length:
            raise BadRequestBodyException("The content size is too large.")

        data = request.data

        if not data and throw_exception_if_empty:
            raise BadRequestBodyException("Request data is empty.")

        return data
