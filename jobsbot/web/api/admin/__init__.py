from datetime import datetime
from flask import g, request
import json
from typing import Optional, Tuple, List, Dict

from jobsbot.core.enums import AdminPermission, CountryLocations, JobCategory, JobPosition, JobPostingValidationStatus, \
    SkillsCategory, UserCountry, UserDesiredMinAnnualGrossSalary, UserProfileValidationStatus, \
    UserProfileRejectionReason, UserProfileRequiredPart, UserExperience, UserSkill, RecruiterPermission, \
    JobPostingStatus, UserJobApplicationMethod, UserNotificationOption, UserPreferences, UserJobApplicationStatus, \
    UserJobMatchType, UserCvStatus
from jobsbot.core.enum_maps import COUNTRY_COUNTRY_LOCATIONS_MAP, JOBS_TREE, SKILLS_TREE
from jobsbot.core.models import Job, JobStatistics, Credentials, RecruiterInvitation
from jobsbot.web.api.common import TokenizedApi, TokenizedApiPermissions, TokenizedApiPermissionRequirements
from jobsbot.web.api.common.serde import candidate_as_api_json, candidate_cv_as_json, company_as_json, job_as_json, \
    recruiter_as_json, job_statistics_as_json, user_job_application_as_json, candidate_anonymous_as_api_json, \
    job_match_as_json, recruiter_invitation_as_json
from jobsbot.web.api.parse.common import parse_raw_json
from jobsbot.web.exceptions import MethodNotImplemented, ObjectNotFoundException, BadRequestException, \
    WrongCredentialsException, BadRequestBodyException, InternalError, NoAccessToResource, TokenHasNoPermissionException
from jobsbot.web.api.parse.request_body import parse_update_password_request, \
    parse_set_candidate_verification_status_request, parse_admin_credentials_request, \
    parse_admin_credentials_partial_request, parse_recruiter_credentials_partial_request, \
    parse_recruiter_credentials_request
from jobsbot.web.api.parse.model import parse_company, parse_company_partial, parse_job, parse_job_partial
from jobsbot.web.api.parse import uri


class AdminApiPermissions(TokenizedApiPermissions):
    def __init__(self, admin_permissions: Optional[List[AdminPermission]],
                 recruiter_permissions: Optional[Dict[str, List[RecruiterPermission]]]):
        self.admin_permissions = admin_permissions
        self.recruiter_permissions = recruiter_permissions


class AdminApiPermissionRequirements(TokenizedApiPermissionRequirements):
    def __init__(self, admin_permissions: List[AdminPermission] = None,
                 recruiter_permissions: List[RecruiterPermission] = None):
        self.admin_permissions = admin_permissions
        self.recruiter_permissions = recruiter_permissions


class AdminApi(TokenizedApi):
    _Ap = AdminApiPermissionRequirements

    def __init__(self, name: str):
        super().__init__(name)
        self.app.secret_key = "befbb3a56871a76f7267c0018cc36c9f0c59eb1c"

        # API METHODS

        self.register_callback("/api/v1/my_profile", self.v1_get_my_profile, should_have_a_valid_token=True,
                               methods=["GET"])
        self.register_callback("/api/v1/my_profile/password", self.v1_update_my_password,
                               should_have_a_valid_token=True, methods=["PUT"])

        self.register_callback("/api/v1/token", self.v1_create_token, should_have_a_valid_token=False, methods=["POST"])
        self.register_callback("/api/v1/logout", self.v1_logout, should_have_a_valid_token=False, methods=["GET"])
        self.register_callback("/api/v1/enums", self.v1_enums, should_have_a_valid_token=True, methods=["GET"])
        self.register_callback("/api/v1/enums_maps", self.v1_enums_maps, should_have_a_valid_token=True,
                               methods=["GET"])

        # API METHODS - ADMINS

        self.register_callback("/api/v1/admin", self.v1_upsert_delete_get_admin, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.MANAGE_ADMINS]),
                               methods=["POST", "PATCH", "DELETE", "GET"])

        # API METHODS - CANDIDATES

        self.register_callback("/api/v1/candidate", self.v1_get_candidate, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_CANDIDATES]),
                               methods=["GET"])
        self.register_callback("/api/v1/candidate/cv", self.v1_get_candidate_cv, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_CANDIDATES]),
                               methods=["GET"])
        self.register_callback("/api/v1/candidate/validation_status", self.v1_set_candidate_validation_status,
                               should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[
                                   AdminPermission.VIEW_CANDIDATES, AdminPermission.VALIDATE_CANDIDATES
                               ]), methods=["PUT"])

        # API METHODS - JOBS

        self.register_callback("/api/v1/job", self.v1_get_job, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   admin_permissions=[AdminPermission.VIEW_JOBS],
                                   recruiter_permissions=[RecruiterPermission.VIEW_ALL_JOBS]
                               ), methods=["GET"])
        self.register_callback("/api/v1/job/access", self.v1_get_job_access, should_have_a_valid_token=True,
                               methods=["GET"])
        self.register_callback("/api/v1/job/applicants", self.v1_get_job_applicants, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   admin_permissions=[AdminPermission.VIEW_JOBS],
                                   recruiter_permissions=[RecruiterPermission.VIEW_ALL_JOBS]
                               ), methods=["GET"])
        self.register_callback("/api/v1/job", self.v1_create_patch_delete_job, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   admin_permissions=[AdminPermission.CREATE_JOB],
                                   recruiter_permissions=[RecruiterPermission.CREATE_JOBS]
                               ),
                               methods=["POST", "PATCH", "DELETE"])
        self.register_callback("/api/v1/job/statistics", self.v1_job_statistics, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   admin_permissions=[AdminPermission.VIEW_JOBS],
                                   recruiter_permissions=[RecruiterPermission.VIEW_ALL_JOBS]
                               ),
                               methods=["GET"])
        self.register_callback("/api/v1/job/archive", self.v1_archive_job, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   admin_permissions=[AdminPermission.CREATE_JOB],
                                   recruiter_permissions=[RecruiterPermission.CREATE_JOBS]
                               ),
                               methods=["PATCH"])
        self.register_callback("/api/v1/job/validate", self.v1_validate_job, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VALIDATE_JOBS]),
                               methods=["PATCH"])

        # API METHODS - COMPANIES

        self.register_callback("/api/v1/company", self.v1_get_company, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_COMPANIES]),
                               methods=["GET"])
        self.register_callback("/api/v1/company", self.v1_create_patch_company, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.CREATE_COMPANIES]),
                               methods=["POST", "PATCH"])
        self.register_callback("/api/v1/company", self.v1_delete_company, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_COMPANIES,
                                                                                   AdminPermission.DELETE_COMPANIES]),
                               methods=["DELETE"])
        self.register_callback("/api/v1/company/jobs", self.v1_get_company_jobs, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_JOBS]),
                               methods=["GET"])
        self.register_callback("/api/v1/company/recruiters", self.v1_get_company_recruiters,
                               should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_RECRUITERS]),
                               methods=["GET"])
        self.register_callback("/api/v1/company/search", self.v1_company_search, should_have_a_valid_token=True,
                               methods=["GET"])

        # API METHODS - RECRUITERS

        self.register_callback("/api/v1/recruiter", self.v1_get_recruiter, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   recruiter_permissions=[RecruiterPermission.MANAGE_RECRUITERS],
                                   admin_permissions=[AdminPermission.VIEW_RECRUITERS]
                               ),
                               methods=["GET"])
        self.register_callback("/api/v1/recruiter", self.v1_create_patch_delete_recruiter,
                               should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   recruiter_permissions=[RecruiterPermission.MANAGE_RECRUITERS],
                                   admin_permissions=[AdminPermission.CREATE_RECRUITER]
                               ),
                               methods=["POST", "PATCH", "DELETE"])
        self.register_callback("/api/v1/recruiter/credentials/from_invitation",
                               self.v1_create_recruiter_credentials_from_invitation,
                               should_have_a_valid_token=False, methods=["POST"])
        self.register_callback("/api/v1/recruiter/invitations", self.v1_get_recruiter_invitations,
                               should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   recruiter_permissions=[RecruiterPermission.MANAGE_RECRUITERS],
                                   admin_permissions=[AdminPermission.CREATE_RECRUITER]
                               ),
                               methods=["GET"])
        self.register_callback("/api/v1/recruiter/invitations", self.v1_delete_recruiter_invitation,
                               should_have_a_valid_token=True,
                               permission_requirements=self._Ap(
                                   recruiter_permissions=[RecruiterPermission.MANAGE_RECRUITERS],
                                   admin_permissions=[AdminPermission.CREATE_RECRUITER]
                               ),
                               methods=["DELETE"])

        # PAGES

        self.register_callback("/page/v1/admins", self.v1_page_admins, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.MANAGE_ADMINS]),
                               methods=["GET"])
        self.register_callback("/page/v1/recruiters", self.v1_page_recruiters, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_RECRUITERS],
                                                                recruiter_permissions
                                                                =[RecruiterPermission.MANAGE_RECRUITERS]),
                               methods=["GET"])
        self.register_callback("/page/v1/candidates", self.v1_page_candidates, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_CANDIDATES]),
                               methods=["GET"])
        self.register_callback("/page/v1/companies", self.v1_page_companies, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_COMPANIES]),
                               methods=["GET"])
        self.register_callback("/page/v1/company/info", self.v1_page_company_info, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_COMPANIES]),
                               methods=["GET"])
        self.register_callback("/page/v1/job", self.v1_page_job, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_JOBS]),
                               methods=["GET"])
        self.register_callback("/page/v1/jobs", self.v1_page_jobs, should_have_a_valid_token=True,
                               permission_requirements=self._Ap(admin_permissions=[AdminPermission.VIEW_JOBS]),
                               methods=["GET"])

    @property
    def user_permissions(self) -> Optional[AdminApiPermissions]:
        return super().user_permissions

    @property
    def is_admin(self) -> bool:
        return bool(self.user_permissions is not None and self.user_permissions.admin_permissions)

    def has_permission_to_view_job(self, job: Job) -> bool:
        if self.is_admin and AdminPermission.VIEW_JOBS in (self.user_permissions.admin_permissions or []):
            return True
        if not self.user_permissions.recruiter_permissions:
            return False
        if job.company_id not in self.user_permissions.recruiter_permissions:
            return False
        if Credentials.canonize_email(job.created_by) != Credentials.canonize_email(self.session_email):
            if RecruiterPermission.VIEW_ALL_JOBS \
                    not in self.user_permissions.recruiter_permissions.get(job.company_id, []):
                return False
        return True

    def has_permission_to_create_job(self, company_id: str) -> bool:
        if self.is_admin and AdminPermission.CREATE_JOB in (self.user_permissions.admin_permissions or []):
            return True
        if not self.user_permissions.recruiter_permissions \
                or company_id not in self.user_permissions.recruiter_permissions:
            return False
        if RecruiterPermission.CREATE_JOBS in self.user_permissions.recruiter_permissions[company_id]:
            return True
        else:
            return False

    def has_permission_to_update_job(self, job: Job) -> bool:
        if self.is_admin and AdminPermission.CREATE_JOB in (self.user_permissions.admin_permissions or []):
            return True
        if job.job_posting_status == JobPostingStatus.ARCHIVED:
            return False
        if job.job_posting_validation_status == JobPostingValidationStatus.VERIFIED:
            return False
        if not self.user_permissions.recruiter_permissions \
                or job.company_id not in self.user_permissions.recruiter_permissions:
            return False
        if Credentials.canonize_email(job.created_by) != Credentials.canonize_email(self.session_email):
            if RecruiterPermission.MODIFY_ALL_JOBS \
                    not in self.user_permissions.recruiter_permissions.get(job.company_id, []):
                return False
        return True

    def are_permission_requirements_met(self, permissions: AdminApiPermissions = None,
                                        permission_requirements: AdminApiPermissionRequirements = None) -> bool:
        if permission_requirements is None:
            return True
        if permissions is None:
            return False

        has_admin_permissions = True
        for admin_permission in permission_requirements.admin_permissions or []:
            if admin_permission not in (permissions.admin_permissions or []):
                has_admin_permissions = False

        has_recruiter_permissions = True
        if g.company_id is None:
            has_recruiter_permissions = False
        else:
            for recruiter_permission in permission_requirements.recruiter_permissions or []:
                if recruiter_permission not in (permissions.recruiter_permissions or dict()).get(g.company_id, []):
                    has_recruiter_permissions = False

        return has_admin_permissions or has_recruiter_permissions

    def get_permissions_by_email(self, email: str) -> Optional[AdminApiPermissions]:
        admin = self.users_dao.read_admin(email)
        recruiters = self.users_dao.read_recruiters(email)

        if admin is None and not recruiters:
            return None

        return AdminApiPermissions(
            admin_permissions=None if admin is None else admin.admin_permissions,
            recruiter_permissions=None if not recruiters else {recruiter.company_id: recruiter.recruiter_permissions
                                                               for recruiter in recruiters}
        )

    def get_session_expiration_date_and_email_by_token(self, token: str) -> Optional[Tuple[datetime, str]]:
        session = self.users_dao.read_session_by_token(
            token=token,
            renew_session=True
        )

        if session is None:
            return None

        return session.expires_at, session.email

    def v1_get_my_profile(self):
        session_email = self.session_email
        permissions: Optional[AdminApiPermissions] = self.user_permissions

        admin_permissions = dict()
        if permissions is not None and permissions.admin_permissions is not None:
            admin_permissions = {p.name: p.title() for p in permissions.admin_permissions}

        recruiter_permissions = dict()
        if permissions is not None and permissions.recruiter_permissions is not None:
            recruiter_permissions = {company_id: {p.name: p.title() for p in company_permissions}
                                     for company_id, company_permissions in permissions.recruiter_permissions.items()}

        admin = self.users_dao.read_admin(session_email)
        recruiters = self.users_dao.read_recruiters(session_email)
        company_id = uri.read_str("company_id", required=False, exception_cls=BadRequestException)

        name = ""
        email = session_email
        if admin is not None:
            name = admin.name
            email = admin.email
        else:
            for recruiter in recruiters:
                if recruiter.company_id == company_id:
                    name = recruiter.name
                    email = recruiter.email

        companies = {
            company.company_id: company_as_json(company)
            for company in self.users_dao.read_companies(list(recruiter_permissions.keys()))
        }

        result = {
            "name": name,
            "email": email,
            "admin_permissions": admin_permissions,
            "recruiter_permissions": recruiter_permissions,
            "companies": companies,
            "recruiters": {recruiter.company_id: recruiter_as_json(recruiter) for recruiter in recruiters}
        }
        return self.format_result(result)

    def v1_update_my_password(self):
        if request.method == "PUT":
            session = self.users_dao.read_session_by_token(g.token)
            if session is None:
                raise InternalError()
            credentials = self.users_dao.read_credentials(session.email)
            if credentials is None:
                raise InternalError()
            try:
                password, old_password = parse_update_password_request(self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}")
            if not credentials.is_password_correct(old_password):
                raise BadRequestBodyException("Old Password is wrong")
            credentials.update_password(password)
            self.users_dao.create_credentials(credentials, replace_if_exists=True)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_create_token(self):
        if request.method == "POST":
            email = request.form.get("email")
            password = request.form.get("password")
            try:
                json_data = json.loads(self.get_post_data())
                json_email = json_data.get("email")
                json_password = json_data.get("password")
                if json_email is not None and json_password is not None:
                    email = json_email
                    password = json_password
            except (ValueError, AttributeError, json.JSONDecodeError):
                pass

            if not email or not password:
                raise BadRequestException(description="Email and/or password is not defined")

            credentials = self.users_dao.read_credentials(email)

            if credentials is None:
                raise WrongCredentialsException()
            if not credentials.is_password_correct(password):
                raise WrongCredentialsException()

            session = self.users_dao.create_session(credentials.email)
            result = {
                "token": session.token
            }
            return self.format_result(result)
        else:
            raise MethodNotImplemented()

    def v1_logout(self):
        if request.method == "GET":
            if g.token and g.is_token_valid:
                self.users_dao.delete_session_by_token(g.token)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_enums(self):
        if request.method == "GET":
            results = {}
            for enum_name, cls in (
                    ("job_category", JobCategory),
                    ("job_position", JobPosition),
                    ("skills_category", SkillsCategory),
                    ("user_country", UserCountry),
                    ("country_locations", CountryLocations),
                    ("user_skill", UserSkill),
                    ("user_experience", UserExperience),
                    ("user_desired_min_annual_gross_salary", UserDesiredMinAnnualGrossSalary),
                    ("admin_permissions", AdminPermission),
                    ("recruiter_permissions", RecruiterPermission),
                    ("job_posting_status", JobPostingStatus),
                    ("job_posting_validation_status", JobPostingValidationStatus),
                    ("user_notification_options", UserNotificationOption),
                    ("user_preferences", UserPreferences),
                    ("user_job_application_method", UserJobApplicationMethod),
                    ("user_job_application_status", UserJobApplicationStatus),
                    ("user_job_match_type", UserJobMatchType),
                    ("user_cv_status", UserCvStatus)
            ):
                titles = cls._titles()
                results[enum_name] = {e.name: titles[e] for e in cls}
            return self.format_result(results)
        else:
            raise MethodNotImplemented()

    def v1_enums_maps(self):
        if request.method == "GET":
            results = {}
            for map_name, enums_map in (
                    ("locations_map", COUNTRY_COUNTRY_LOCATIONS_MAP),
                    ("jobs_map", JOBS_TREE),
                    ("skills_map", SKILLS_TREE)
            ):
                results[map_name] = {e.name: [ee.name for ee in enums_map[e]] for e in enums_map.keys()}
            return self.format_result(results)
        else:
            raise MethodNotImplemented()

    def v1_upsert_delete_get_admin(self):
        if request.method == "POST":
            # TODO: Check email
            try:
                admin, credentials = parse_admin_credentials_request(self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}")
            if self.users_dao.read_admin(admin.email) is not None:
                raise BadRequestException(f"Admin with email {admin.email} already exists")
            old_credentials = self.users_dao.read_credentials(admin.email)
            if not old_credentials and not credentials:
                raise BadRequestException(f"Credentials for email {admin.email} do not exist. Please, provide "
                                          f"credentials in your request")
            if credentials:
                self.users_dao.create_credentials(credentials, replace_if_exists=True)
            self.users_dao.create_admin(admin, replace_if_exists=True)
            return self.format_result()
        elif request.method == "PATCH":
            admin_email = uri.read_str("email", required=True, exception_cls=BadRequestException)
            old_admin = self.users_dao.read_admin(admin_email)
            old_credentials = self.users_dao.read_credentials(admin_email)
            if old_admin is None:
                raise ObjectNotFoundException(f"Admin with email {admin_email} does not exists")
            try:
                admin, credentials = parse_admin_credentials_partial_request(self.get_post_data(), old_admin,
                                                                             old_credentials)
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}") from None
            if admin.canonized_email != old_admin.canonized_email:
                if self.users_dao.read_admin(admin.email) is not None:
                    raise BadRequestException(f"Admin with email {admin.email} already exists")
                else:
                    self.users_dao.update_admin_recruiter_email(old_admin.email, admin.email)
            self.users_dao.create_admin(admin, replace_if_exists=True)
            if credentials:
                self.users_dao.create_credentials(credentials, replace_if_exists=True)
            return self.format_result()
        elif request.method == "DELETE":
            admin_email = uri.read_str("email", required=True, exception_cls=BadRequestException)
            self.users_dao.delete_admin(admin_email, delete_credentials_if_possible=True)
            return self.format_result()
        elif request.method == "GET":
            admin_email = uri.read_str("email", required=True, exception_cls=BadRequestException)
            admin = self.users_dao.read_admin(admin_email)
            if admin is None:
                raise ObjectNotFoundException()
            return self.format_result(admin.as_json_dict())
        else:
            raise MethodNotImplemented()

    # API METHODS - CANDIDATES

    def v1_get_candidate(self):
        if request.method == "GET":
            tg_id = uri.read_int("tg_id", required=True, exception_cls=BadRequestException)
            user = self.users_dao.read_user(tg_id)
            if user is None:
                raise ObjectNotFoundException("User does not exist")
            return self.format_result(candidate_as_api_json(user))
        else:
            raise MethodNotImplemented()

    def v1_get_candidate_cv(self):
        if request.method == "GET":
            tg_id = uri.read_int("tg_id", required=True, exception_cls=BadRequestException)
            user = self.users_dao.read_user(tg_id)
            if user is None:
                raise ObjectNotFoundException("User does not exist")
            user_cv = self.users_dao.read_user_cv(tg_id)
            result = {
                "cv_uploaded": user_cv is not None,
                "cv": None if user_cv is None else candidate_cv_as_json(user_cv)
            }
            return self.format_result(result)
        else:
            raise MethodNotImplemented()

    def v1_set_candidate_validation_status(self):
        if request.method == "PUT":
            tg_id = uri.read_int("tg_id", required=True, exception_cls=BadRequestException)
            user = self.users_dao.read_user(tg_id)
            if user is None:
                raise ObjectNotFoundException("User does not exist")
            try:
                json_data = parse_set_candidate_verification_status_request(self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}")

            if json_data["validation_status"] == UserProfileValidationStatus.VERIFIED:
                user.accept_profile_details()
            elif json_data["validation_status"] == UserProfileValidationStatus.REJECTED:
                user.reject_profile_details(
                    rejected_profile_parts=json_data["rejected_profile_parts"],
                    rejection_reasons=json_data["rejection_reasons"]
                )
            else:
                raise BadRequestException(f"Unsupported validation status: \"{json_data['validation_status']}\"")

            self.users_dao.save_user(user)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_get_company(self):
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        company = self.users_dao.read_company(company_id)
        if company is None:
            raise ObjectNotFoundException("Company with such id does not exist")
        return self.format_result(company_as_json(company))

    def v1_get_company_jobs(self):
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        company = self.users_dao.read_company(company_id)
        if company is None:
            raise ObjectNotFoundException("Company with such id does not exist")
        jobs = [job_as_json(job) for job in self.users_dao.read_jobs_by_company_id(company_id)]
        return self.format_result(jobs)

    def v1_get_company_recruiters(self):
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        company = self.users_dao.read_company(company_id)
        if company is None:
            raise ObjectNotFoundException("Company with such id does not exist")
        recruiters = [recruiter_as_json(recruiter) for recruiter in self.users_dao.read_company_recruiters(company_id)]
        return self.format_result(recruiters)

    def v1_company_search(self):
        query = uri.read_str("q", required=False, default=None, exception_cls=BadRequestException)
        offset = uri.read_int("offset", required=False, default=0, min_allowed=0, exception_cls=BadRequestException)
        limit = uri.read_int("limit", required=False, default=20, min_allowed=1, exception_cls=BadRequestException)
        company_ids = None
        if not self.is_admin:
            company_ids = []
            if self.user_permissions.recruiter_permissions:
                company_ids = list(self.user_permissions.recruiter_permissions.keys())
        companies = list(self.users_dao.company_search(q=query, company_ids=company_ids, offset=offset, limit=limit))
        return self.format_result({
            "search_result": [company_as_json(company) for company in companies]
        })

    def v1_create_patch_company(self):
        if request.method == "PATCH":
            company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
            company = self.users_dao.read_company(company_id)
            if company is None:
                raise ObjectNotFoundException("Company with such ID does not exist")
            try:
                company = parse_company_partial(company, self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}") from None
            ref_company = self.users_dao.read_company_by_referral_link_id(company.referral_link_id)
            if ref_company is not None and ref_company.company_id != company_id:
                raise BadRequestException(f"Company with such referral ID already exists")
            self.users_dao.upsert_company(company)
            return self.format_result()
        elif request.method == "POST":
            try:
                company = parse_company(self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}") from None
            if self.users_dao.read_company_by_referral_link_id(company.referral_link_id) is not None:
                raise BadRequestException(f"Company with such referral ID already exists")
            self.users_dao.upsert_company(company)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_delete_company(self):
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        self.users_dao.delete_company(company_id)
        return self.format_result()

    def v1_get_recruiter(self):
        recruiter_email = uri.read_str("recruiter_email", required=True, exception_cls=BadRequestException)
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        recruiter = self.users_dao.read_one_recruiter(email=recruiter_email, company_id=company_id)
        if recruiter is None:
            raise ObjectNotFoundException(f"Recruiter {recruiter_email} does not exist")
        return self.format_result(recruiter_as_json(recruiter))

    def v1_create_patch_delete_recruiter(self):
        if request.method == "POST":
            company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
            skip_invitation = uri.read_bool("skip_invitation", required=False, default=False,
                                            exception_cls=BadRequestException)
            if skip_invitation and not self.is_admin:
                raise TokenHasNoPermissionException("You have no permissions to add a recruiter without an invitation")
            try:
                recruiter, credentials = parse_recruiter_credentials_request(self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}") from None
            if recruiter.company_id != company_id:
                raise BadRequestException("Company id mismatch")
            if self.users_dao.read_one_recruiter(email=recruiter.email, company_id=company_id) is not None:
                raise BadRequestException("Recruiter with such email already exists")
            if skip_invitation:
                if not credentials:
                    raise BadRequestException("Can not create a recruiter without credentials")
                self.users_dao.create_recruiter(recruiter, replace_if_exists=True)
                self.users_dao.create_credentials(credentials, replace_if_exists=False)
            else:
                if self.users_dao.read_one_recruiter(email=recruiter.email, company_id=recruiter.company_id):
                    raise BadRequestException("This recruiter was already added to your organization")
                existing_invitations = list(self.users_dao.read_recruiter_invitations(email=recruiter.email,
                                                                                      company_id=recruiter.company_id))
                if existing_invitations:
                    raise BadRequestException("This recruiter has already been invited. Please, wait a couple of "
                                              "days before inviting them one more time")
                self.users_dao.create_recruiter_invitation(
                    RecruiterInvitation.from_recruiter(recruiter, invited_by=self.session_email),
                    replace_if_exists=False
                )
            return self.format_result()
        elif request.method == "PATCH":
            recruiter_email = uri.read_str("recruiter_email", required=True, exception_cls=BadRequestException)
            company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
            old_recruiter = self.users_dao.read_one_recruiter(email=recruiter_email, company_id=company_id)
            old_credentials = self.users_dao.read_credentials(email=recruiter_email)
            if old_recruiter is None:
                raise ObjectNotFoundException("Recruiter with such email does not exist")
            if old_credentials is None:
                raise ObjectNotFoundException("Can not find credentials for such requiter. This is unexpected "
                                              "behaviour - please, contact us")
            try:
                recruiter, credentials = parse_recruiter_credentials_partial_request(self.get_post_data(),
                                                                                     old_recruiter, old_credentials)
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}") from None
            if old_recruiter.company_id != recruiter.company_id:
                raise BadRequestException("Can not update company_id for recruiter")
            if old_recruiter.canonized_email != recruiter.canonized_email:
                raise BadRequestException("Can not update email for recruiter")
            if credentials:
                admin = self.users_dao.read_admin(old_recruiter.email)
                recruiters = self.users_dao.read_recruiters(old_recruiter.email)
                if admin is not None or len(recruiters) != 1:
                    if self.is_admin:
                        self.users_dao.create_credentials(credentials, replace_if_exists=True)
                    else:
                        raise BadRequestException("Can not update credentials for this specific recruiter")
            self.users_dao.create_recruiter(recruiter, replace_if_exists=True)
            return self.format_result()
        elif request.method == "DELETE":
            company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
            recruiter_email = uri.read_str("recruiter_email", required=True, exception_cls=BadRequestException)
            self.users_dao.delete_one_recruiter(email=recruiter_email, company_id=company_id)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_create_recruiter_credentials_from_invitation(self):
        if request.method == "POST":
            invitation_token = uri.read_str("token", required=True, exception_cls=BadRequestException)
            invitation = self.users_dao.read_recruiter_invitation_by_token(invitation_token)
            if not invitation or invitation.expired:
                raise BadRequestException("Invalid invitation token. Maybe you have already accepted this invitation?")
            existing_credentials = self.users_dao.read_credentials(invitation.email)
            if existing_credentials:
                raise BadRequestException("You credentials have already been set. Just try to log in.")
            try:
                password = parse_raw_json(self.get_post_data())["password"]
            except Exception:
                raise BadRequestBodyException("Can not parse request")
            if len(password) < 8:
                raise BadRequestException("Password is too short. Should be at least 8 characters")
            self.users_dao.create_credentials(Credentials(email=invitation.email, raw_password=password),
                                              replace_if_exists=False)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_get_recruiter_invitations(self):
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        offset = uri.read_int("offset", min_allowed=0, required=False, default=0, exception_cls=BadRequestException)
        limit = uri.read_int("limit", min_allowed=1, required=False, default=20, exception_cls=BadRequestException)
        result = []
        for recruiter_invitation in self.users_dao.read_recruiter_invitations(company_id=company_id, limit=limit,
                                                                              offset=offset):
            result.append(recruiter_invitation_as_json(recruiter_invitation))
        return self.format_result(result)

    def v1_delete_recruiter_invitation(self):
        if request.method == "DELETE":
            company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
            recruiter_email = uri.read_str("recruiter_email", required=True, exception_cls=BadRequestException)
            self.users_dao.delete_one_recruiter_invitation(email=recruiter_email, company_id=company_id)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_get_job(self):
        job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
        job = self.users_dao.read_job(job_id)
        if job is None:
            raise ObjectNotFoundException("Job with such id does not exist")
        if not self.has_permission_to_view_job(job):
            raise BadRequestException("You do not have permissions to view this job")
        return self.format_result(job_as_json(job))

    def v1_get_job_access(self):
        job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
        job = self.users_dao.read_job(job_id)
        if job is None:
            raise ObjectNotFoundException("Job with such id does not exist")
        return self.format_result({
            "view": self.has_permission_to_view_job(job),
            "update": self.has_permission_to_update_job(job),
            "archive": self.has_permission_to_update_job(job),
            "delete": self.has_permission_to_update_job(job)
        })

    def v1_get_job_applicants(self):
        job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
        offset = uri.read_int("offset", min_allowed=0, required=False, default=0, exception_cls=BadRequestException)
        limit = uri.read_int("limit", min_allowed=1, required=False, default=20, exception_cls=BadRequestException)
        job = self.users_dao.read_job(job_id)
        if job is None:
            raise ObjectNotFoundException("Job with such id does not exist")
        if not self.has_permission_to_view_job(job):
            raise TokenHasNoPermissionException("You have no access to the applicants for this job")
        result = []
        user_job_applications = list(self.users_dao.read_user_job_applications(job_id=job.job_id, no_older_than=None,
                                                                               limit=limit, offset=offset))
        user_tg_ids = [application.user_tg_id for application in user_job_applications]
        job_matches = {match.user_tg_id: match
                       for match in self.users_dao.read_user_job_matches(user_tg_ids=user_tg_ids, job_id=job.job_id,
                                                                         no_older_than=None)}
        result = {
            "user_job_applications_count": self.users_dao.user_job_applications_count(job_id=job.job_id,
                                                                                      no_older_than=None),
            "applicants": []
        }
        for user_job_application in user_job_applications:
            user = self.users_dao.read_user(user_job_application.user_tg_id)
            user_json = None
            if user is not None:
                if user_job_application.user_job_application_method == UserJobApplicationMethod.URL_REDIRECT:
                    user_json = candidate_anonymous_as_api_json(user)
                else:
                    user_json = candidate_as_api_json(user)
            job_match = job_matches.get(user.tg_id)
            if job_match:
                job_match = job_match_as_json(job_match)
            result["applicants"].append({
                "job_application": user_job_application_as_json(user_job_application),
                "job_match": job_match,
                "candidate": user_json
            })
        return self.format_result(result)

    def v1_create_patch_delete_job(self):
        if request.method == "PATCH":
            job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
            old_job = self.users_dao.read_job(job_id)
            if old_job is None:
                raise ObjectNotFoundException("Job with such ID does not exist")
            try:
                job = parse_job_partial(old_job, self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}") from None
            if old_job.company_id != job.company_id:
                raise BadRequestBodyException(f"Can no update company_id")
            if not self.has_permission_to_update_job(job):
                raise TokenHasNoPermissionException("You have no access to update this job")
            job.updated_at = datetime.utcnow()
            self.users_dao.upsert_job(job)
            return self.format_result()
        elif request.method == "POST":
            company_id = uri.read_str("company_id", required=False)
            if not self.has_permission_to_create_job(company_id):
                raise TokenHasNoPermissionException("You have no access to create jobs")
            try:
                job = parse_job(self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Can not parse request body: {str(e)}") from None
            if company_id is not None and company_id != job.company_id:
                raise BadRequestBodyException("Company_id mismatch")
            if self.users_dao.read_company(job.company_id) is None:
                raise BadRequestBodyException(f"Company with ID {job.company_id} does not exist")
            job.created_by = self.session_email or "n/a"
            job.created_at = datetime.utcnow()
            self.users_dao.upsert_job(job)
            return self.format_result(job_as_json(self.users_dao.read_job(job.job_id)))
        elif request.method == "DELETE":
            job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
            job = self.users_dao.read_job(job_id)
            if job and not self.has_permission_to_update_job(job):
                raise TokenHasNoPermissionException("You have no access to delete this job")
            self.users_dao.delete_job(job_id)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_job_statistics(self):
        if request.method == "GET":
            job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
            job = self.users_dao.read_job(job_id)
            if job is None:
                raise ObjectNotFoundException(f"Job {job_id} does not exist")
            if not self.has_permission_to_view_job(job):
                raise BadRequestException("You do not have permissions to check statistics for this job")
            statistics = self.users_dao.read_job_statistics(job_id)
            if statistics is None:
                statistics = JobStatistics.empty_statistics(job_id)
            return self.format_result(job_statistics_as_json(statistics))
        else:
            raise MethodNotImplemented()

    def v1_archive_job(self):
        if request.method == "PATCH":
            job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
            self.users_dao.archive_job(job_id)
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_validate_job(self):
        if request.method == "PATCH":
            job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
            validation_status_raw = uri.read_str("validation_status", required=True, exception_cls=BadRequestException)
            try:
                validation_status: JobPostingValidationStatus = JobPostingValidationStatus.parse(validation_status_raw)
            except Exception:
                raise BadRequestException("Can not parse validation status")
            self.users_dao.validate_job(
                job_id, validation_status,
                validation_status in (
                    JobPostingValidationStatus.NOT_READY,
                    JobPostingValidationStatus.REJECTED,
                    JobPostingValidationStatus.WAITING_VERIFICATION
                )
            )
            return self.format_result()
        else:
            raise MethodNotImplemented()

    def v1_page_admins(self):
        result = {"admins": [], "possible_admin_permissions": {p.name: p.title() for p in AdminPermission}}
        for admin in self.users_dao.read_all_admins():
            result["admins"].append({
                "email": admin.email,
                "name": admin.name,
                "permissions": [p.name for p in admin.admin_permissions]
            })
        return self.format_result(result)

    def v1_page_recruiters(self):
        result = {"recruiters": []}
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        for recruiter in self.users_dao.read_company_recruiters(company_id):
            result["recruiters"].append(recruiter_as_json(recruiter))
        return self.format_result(result)

    def v1_page_candidates(self):
        result = {
            "candidates": [],
            "possible_validation_statuses": {s.name: s.title() for s in UserProfileValidationStatus},
            "possible_rejection_reasons": {r.name: r.title() for r in UserProfileRejectionReason},
            "possible_rejected_profile_parts": {p.name: p.title() for p in UserProfileRequiredPart},
            "possible_experience_options": {e.name: e.title() for e in UserExperience},
            "rejection_validation_status": UserProfileValidationStatus.REJECTED.name,
            "approval_validation_status": UserProfileValidationStatus.VERIFIED.name
        }

        for candidate in self.users_dao.read_all_users():
            result["candidates"].append(candidate_as_api_json(candidate))
        return self.format_result(result)

    def v1_page_companies(self):
        result = {
            "companies": [company_as_json(company) for company in self.users_dao.read_all_companies()]
        }

        return self.format_result(result)

    def v1_page_company_info(self):
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        company = self.users_dao.read_company(company_id)
        if company is None:
            raise ObjectNotFoundException(f"Company with id {company_id} does not exist")

        result = {
            "company": company_as_json(company),
            "jobs": [job_as_json(job) for job in self.users_dao.read_jobs_by_company_id(company_id)],
            "recruiters": [recruiter_as_json(recruiter)
                           for recruiter in self.users_dao.read_company_recruiters(company_id=company_id)]
        }

        return self.format_result(result)

    def v1_page_job(self):
        job_id = uri.read_str("job_id", required=True, exception_cls=BadRequestException)
        job = self.users_dao.read_job(job_id)
        if job is None:
            raise ObjectNotFoundException("Can not find a job")
        company = self.users_dao.read_company(job.company_id)
        if company is None:
            raise ObjectNotFoundException("Can not find a company")
        if job.created_by != self.session_email:
            has_access = False
            if self.user_permissions.admin_permissions \
                    and AdminPermission.VIEW_JOBS in self.user_permissions.admin_permissions:
                has_access = True
            if self.user_permissions.recruiter_permissions \
                    and job.company_id in self.user_permissions.recruiter_permissions \
                    and RecruiterPermission.VIEW_ALL_JOBS \
                    in self.user_permissions.recruiter_permissions[job.company_id]:
                has_access = True
            if not has_access:
                raise NoAccessToResource("User has no access to view this job")
        result = {
            "job": job_as_json(job),
            "company": company_as_json(company),
            "job_statistics": job_statistics_as_json(self.users_dao.read_job_statistics(job.job_id)
                                                     or JobStatistics.empty_statistics(job.job_id)),
            "can_edit": self.has_permission_to_update_job(job)
        }
        return self.format_result(result)

    def v1_page_jobs(self):
        company_id = uri.read_str("company_id", required=True, exception_cls=BadRequestException)
        offset = uri.read_int("offset", min_allowed=0, default=0, required=False, exception_cls=BadRequestException)
        limit = uri.read_int("limit", min_allowed=1, default=20, required=False, exception_cls=BadRequestException)

        jobs = self.users_dao.read_jobs_by_company_id(company_id, offset=offset, limit=limit)
        job_statistics = self.users_dao.read_multiple_job_statistics([job.job_id for job in jobs])

        result = {
            "jobs": [job_as_json(job) for job in self.users_dao.read_jobs_by_company_id(company_id)],
            "job_statistics": [job_statistics_as_json(js) for js in job_statistics]
        }

        return self.format_result(result)
