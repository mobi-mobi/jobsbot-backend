import copy


def _str_schema(max_len: int = None, min_len: int = None, nullable: bool = True, pattern: str = None):
    _schema = {"type": "string" if not nullable else ["string", "null"]}
    if max_len is not None:
        _schema["maxLength"] = max_len
    if min_len is not None:
        _schema["minLength"] = min_len
    if pattern is not None:
        _schema["pattern"] = pattern
    return _schema


def _int_schema(min_value: int = None, max_value: int = None, nullable: bool = True):
    _schema = {"type": "integer" if not nullable else ["integer", "null"]}
    if min_value is not None:
        _schema["minimum"] = min_value
    if max_value is not None:
        _schema["maximum"] = max_value
    return _schema


def _bool_schema(nullable: bool = True):
    return {"type": ["boolean"] if not nullable else ["boolean", "null"]}


CREDENTIALS_SCHEMA = {
    "type": "object",
    "properties": {
        "email": _str_schema(100, min_len=5, nullable=False),
        "password": _str_schema(100, min_len=8, nullable=False),
    },
    "additionalProperties": False,
    "required": [
        "email", "password"
    ]
}


ADMIN_SCHEMA = {
    "type": "object",
    "properties": {
        "email": _str_schema(100, min_len=5, nullable=False),
        "name": _str_schema(100, min_len=3, nullable=False),
        "admin_permissions": {
            "type": ["array"],
            "uniqueItems": True,
            "minItems": 0,
            "maxItems": 1024,
            "items": _str_schema(100, nullable=False)
        }
    },
    "additionalProperties": False,
    "required": [
        "email", "name", "admin_permissions"
    ]
}

ADMIN_SCHEMA_PARTIAL = copy.deepcopy(ADMIN_SCHEMA)
del ADMIN_SCHEMA_PARTIAL["required"]


ADMIN_CREDENTIALS_REQUEST_SCHEMA = copy.deepcopy(ADMIN_SCHEMA)
ADMIN_CREDENTIALS_REQUEST_SCHEMA["properties"]["credentials"] = copy.deepcopy(CREDENTIALS_SCHEMA)
del ADMIN_CREDENTIALS_REQUEST_SCHEMA["properties"]["credentials"]["properties"]["email"]
ADMIN_CREDENTIALS_REQUEST_SCHEMA["properties"]["credentials"]["required"].remove("email")

ADMIN_CREDENTIALS_PARTIAL_REQUEST_SCHEMA = copy.deepcopy(ADMIN_CREDENTIALS_REQUEST_SCHEMA)
ADMIN_CREDENTIALS_PARTIAL_REQUEST_SCHEMA.pop("required")
ADMIN_CREDENTIALS_PARTIAL_REQUEST_SCHEMA["properties"]["credentials"].pop("required")


RECRUITER_SCHEMA = {
    "type": "object",
    "properties": {
        "email": _str_schema(100, min_len=5, nullable=False),
        "name": _str_schema(100, min_len=5, nullable=False),
        "company_id": _str_schema(100, min_len=5, nullable=False),
        "recruiter_permissions": {
            "type": ["array"],
            "uniqueItems": True,
            "minItems": 0,
            "maxItems": 1024,
            "items": _str_schema(100, nullable=False)
        }
    },
    "additionalProperties": False,
    "required": [
        "email", "name", "recruiter_permissions", "company_id"
    ]
}

RECRUITER_SCHEMA_PARTIAL = copy.deepcopy(RECRUITER_SCHEMA)
del RECRUITER_SCHEMA_PARTIAL["required"]

RECRUITER_CREDENTIALS_REQUEST_SCHEMA = copy.deepcopy(RECRUITER_SCHEMA)
RECRUITER_CREDENTIALS_REQUEST_SCHEMA["properties"]["credentials"] = copy.deepcopy(CREDENTIALS_SCHEMA)
del RECRUITER_CREDENTIALS_REQUEST_SCHEMA["properties"]["credentials"]["properties"]["email"]
RECRUITER_CREDENTIALS_REQUEST_SCHEMA["properties"]["credentials"]["required"].remove("email")

RECRUITER_CREDENTIALS_PARTIAL_REQUEST_SCHEMA = copy.deepcopy(RECRUITER_CREDENTIALS_REQUEST_SCHEMA)
del RECRUITER_CREDENTIALS_PARTIAL_REQUEST_SCHEMA["required"]
del RECRUITER_CREDENTIALS_PARTIAL_REQUEST_SCHEMA["properties"]["credentials"]["required"]

UPDATE_PASSWORD_SCHEMA = {
    "type": "object",
    "properties": {
        "old_password": _str_schema(100, min_len=1, nullable=False),
        "password": _str_schema(100, min_len=8, nullable=False),
    },
    "additionalProperties": False,
    "required": [
        "old_password", "password"
    ]
}


COMPANY_SCHEMA = {
    "type": "object",
    "properties": {
        "company_name": _str_schema(100, min_len=1, nullable=False),
        "referral_link_id": _str_schema(10, min_len=1, nullable=False),
        "is_agency": _bool_schema(nullable=False),
        "is_consulting": _bool_schema(nullable=False),
        "is_startup": _bool_schema(nullable=False)
    },
    "additionalProperties": False,
    "required": [
        "company_name", "referral_link_id", "is_agency", "is_consulting", "is_startup"
    ]
}

COMPANY_SCHEMA_PARTIAL = copy.deepcopy(COMPANY_SCHEMA)
del COMPANY_SCHEMA_PARTIAL["required"]

JOB_SCHEMA = {
    "type": "object",
    "properties": {
        "job_posting_status": _str_schema(100, nullable=False),
        "job_posting_validation_status": _str_schema(100, nullable=False),
        "job_url": _str_schema(1000, min_len=10, nullable=True),
        "allow_one_click_applications": _bool_schema(nullable=False),
        "allow_non_complete_profiles": _bool_schema(nullable=False),
        "company_id": _str_schema(100, min_len=1, nullable=False),
        "position": _str_schema(100, min_len=1, nullable=False),
        "title": _str_schema(100, min_len=5, nullable=False),
        "min_experience": _str_schema(10, min_len=1, nullable=False),
        "must_have_skills": {
            "type": ["array"],
            "uniqueItems": True,
            "minItems": 0,
            "maxItems": 20,
            "items": _str_schema(100, nullable=False)
        },
        "better_to_have_skills": {
            "type": ["array"],
            "uniqueItems": True,
            "minItems": 0,
            "maxItems": 20,
            "items": _str_schema(100, nullable=False)
        },
        "hiring_manager_name": _str_schema(100, min_len=0, nullable=True),
        "department_name": _str_schema(100, min_len=0, nullable=True),
        "description": _str_schema(500, min_len=0, nullable=True),
        "about_company": _str_schema(500, min_len=0, nullable=True),
        "technologies": _str_schema(500, min_len=0, nullable=True),
        "locations": {
            "type": ["array"],
            "uniqueItems": True,
            "minItems": 0,
            "maxItems": 10,
            "items": _str_schema(100, nullable=False)
        },
        "remote_work_from_countries": {
            "type": ["array"],
            "uniqueItems": True,
            "minItems": 0,
            "maxItems": 10,
            "items": _str_schema(100, nullable=False)
        },
        "annual_salary_min": _int_schema(min_value=0, max_value=10 ** 10, nullable=False),
        "annual_salary_max": _int_schema(min_value=0, max_value=10 ** 10, nullable=False),
        "show_annual_salary": _bool_schema(nullable=False),
        "is_temporary": _bool_schema(nullable=False),
        "is_internship": _bool_schema(nullable=False),
        "with_conferences_budget": _bool_schema(nullable=False),
        "fully_remote_allowed": _bool_schema(nullable=False),
        "partially_remote_allowed": _bool_schema(nullable=False),
        "work_from_office_is_not_allowed": _bool_schema(nullable=False),
        "with_relocation": _bool_schema(nullable=False),
        "relocation_from_countries": {
            "type": ["array"],
            "uniqueItems": True,
            "minItems": 0,
            "maxItems": 100,
            "items": _str_schema(100, nullable=False)
        },
        "stocks_available": _bool_schema(nullable=False)
    },
    "additionalProperties": False,
    "required": [
        "job_posting_status", "job_posting_validation_status", "job_url",
        "allow_one_click_applications", "allow_non_complete_profiles",
        "company_id", "title", "min_experience", "must_have_skills", "better_to_have_skills",
        "hiring_manager_name", "department_name", "description", "about_company",
        "technologies", "locations", "remote_work_from_countries", "annual_salary_min", "annual_salary_max",
        "show_annual_salary", "is_temporary", "is_internship", "with_conferences_budget", "fully_remote_allowed",
        "partially_remote_allowed", "work_from_office_is_not_allowed", "with_relocation", "relocation_from_countries",
        "stocks_available"
    ]
}

JOB_SCHEMA_PARTIAL = copy.deepcopy(JOB_SCHEMA)
del JOB_SCHEMA_PARTIAL["required"]


SET_CANDIDATE_VALIDATION_STATUS_SCHEMA = {
    "type": "object",
    "properties": {
        "validation_status": _str_schema(100, nullable=False),
        "rejection_reasons": {
            "type": "array",
            "uniqueItems": True,
            "minItems": 1,
            "maxItems": 1024,
            "items": _str_schema(100, nullable=False)
        },
        "rejected_profile_parts": {
            "type": "array",
            "uniqueItems": True,
            "minItems": 1,
            "maxItems": 1024,
            "items": _str_schema(100, nullable=False)
        }
    },
    "additionalProperties": False,
    "required": [
        "validation_status"
    ]
}

RECRUITER_PERMISSIONS_LIST_SCHEMA = {
    "type": "array",
    "uniqueItems": True,
    "minItems": 0,
    "maxItems": 1024,
    "items": _int_schema(min_value=0, nullable=False)
}
