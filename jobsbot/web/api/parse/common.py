import json

from typing import Any, Union

from jobsbot.core.exceptions import JobsBotDecodeException


def bytes_to_str(data: bytes) -> str:
    try:
        return data.decode("utf-8")
    except UnicodeDecodeError:
        raise JobsBotDecodeException("Can not decode bytes to a valid string") from None


def str_to_json(raw_json: str) -> Any:
    try:
        return json.loads(raw_json)
    except json.JSONDecodeError:
        raise JobsBotDecodeException("Can not parse JSON string") from None


def parse_raw_json(raw_json: Union[bytes, str]) -> Any:
    if isinstance(raw_json, bytes):
        raw_json = bytes_to_str(raw_json)
    return str_to_json(raw_json)
