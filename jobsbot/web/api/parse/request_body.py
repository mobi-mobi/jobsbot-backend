from jsonschema import validate, SchemaError, ValidationError
from typing import Union, Tuple, Optional

from jobsbot.core.enums import UserProfileValidationStatus, UserProfileRejectionReason, UserProfileRequiredPart
from jobsbot.core.exceptions import JobsBotSchemaValidationException
from jobsbot.core.models import Admin, Credentials, Recruiter
from jobsbot.web.api.parse.common import parse_raw_json
from jobsbot.web.api.parse.schemas import UPDATE_PASSWORD_SCHEMA, SET_CANDIDATE_VALIDATION_STATUS_SCHEMA,\
    RECRUITER_PERMISSIONS_LIST_SCHEMA, ADMIN_CREDENTIALS_REQUEST_SCHEMA, ADMIN_CREDENTIALS_PARTIAL_REQUEST_SCHEMA,\
    RECRUITER_CREDENTIALS_REQUEST_SCHEMA, RECRUITER_CREDENTIALS_PARTIAL_REQUEST_SCHEMA


def validate_update_password_request(raw_json: str, schema=None):
    err_prefix = "Can not parse request data: "

    try:
        validate(raw_json, schema or UPDATE_PASSWORD_SCHEMA)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_update_password_request(raw_json: Union[bytes, str]) -> Tuple[str, str]:
    json_obj = parse_raw_json(raw_json)
    validate_update_password_request(json_obj)
    return json_obj["password"], json_obj["old_password"]


def validate_set_candidate_verification_status_request(raw_json: str, schema=None):
    err_prefix = "Can not parse request data: "

    try:
        validate(raw_json, schema or SET_CANDIDATE_VALIDATION_STATUS_SCHEMA)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_set_candidate_verification_status_request(raw_json: Union[bytes, str]):
    json_obj = parse_raw_json(raw_json)
    validate_set_candidate_verification_status_request(json_obj)

    try:
        validation_status = UserProfileValidationStatus.parse(json_obj["validation_status"])
        json_obj["validation_status"] = validation_status
    except ValueError as e:
        raise JobsBotSchemaValidationException(str(e)) from None

    if validation_status == UserProfileValidationStatus.VERIFIED:
        if "rejection_reasons" in json_obj:
            raise JobsBotSchemaValidationException("rejection_reasons field should not be defined for "
                                                   "VERIFIED status")
        if "rejected_profile_parts" in json_obj:
            raise JobsBotSchemaValidationException("rejected_profile_parts field should not be defined for "
                                                   "VERIFIED status")
    elif validation_status == UserProfileValidationStatus.REJECTED:
        rejected_reasons = set()
        rejected_profile_parts = set()

        if "rejection_reasons" not in json_obj:
            raise JobsBotSchemaValidationException("rejection_reasons field is missing for REJECTED status")
        if "rejected_profile_parts" not in json_obj:
            raise JobsBotSchemaValidationException("rejected_profile_parts field is missing for REJECTED status")

        for raw_reason in json_obj["rejection_reasons"]:
            try:
                rejected_reasons.add(UserProfileRejectionReason.parse(raw_reason))
            except ValueError:
                raise JobsBotSchemaValidationException(f"Can not parse rejected reason \"{raw_reason}\"") from None

        for raw_rejected_profile_part in json_obj["rejected_profile_parts"]:
            try:
                rejected_profile_parts.add(UserProfileRequiredPart.parse(raw_rejected_profile_part))
            except ValueError:
                raise JobsBotSchemaValidationException(f"Can not parse rejected profile part "
                                                       f"\"{raw_rejected_profile_part}\"") from None

        json_obj["rejection_reasons"] = list(rejected_reasons)
        json_obj["rejected_profile_parts"] = list(rejected_profile_parts)
    else:
        JobsBotSchemaValidationException("Validation status has to be either VERIFIED or REJECTED")

    return json_obj


def validate_recruiter_permissions_list_request(raw_json: str, schema=None):
    err_prefix = "Can not parse request data: "

    try:
        validate(raw_json, schema or RECRUITER_PERMISSIONS_LIST_SCHEMA)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_recruiter_permissions_list(raw_json: Union[bytes, str]):
    json_obj = parse_raw_json(raw_json)
    validate_recruiter_permissions_list_request(json_obj)
    return json_obj


def validate_admin_credentials_request(raw_json: str, schema=None):
    err_prefix = "Can not parse request data: "

    try:
        validate(raw_json, schema or ADMIN_CREDENTIALS_REQUEST_SCHEMA)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_admin_credentials_request(raw_json: Union[bytes, str]) -> Tuple[Admin, Optional[Credentials]]:
    json_obj = parse_raw_json(raw_json)
    validate_admin_credentials_request(json_obj)
    admin = Admin.from_dict(json_obj)
    credentials = None
    if "credentials" in json_obj and json_obj["credentials"]:
        json_obj["credentials"]["email"] = admin.email
        if "password" in json_obj["credentials"]:
            json_obj["credentials"]["raw_password"] = json_obj["credentials"]["password"]
            del json_obj["credentials"]["password"]
        credentials = Credentials.from_dict(json_obj["credentials"])
    return admin, credentials


def validate_admin_credentials_partial_request(raw_json: str, schema=None):
    err_prefix = "Can not parse request data: "

    try:
        validate(raw_json, schema or ADMIN_CREDENTIALS_PARTIAL_REQUEST_SCHEMA)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_admin_credentials_partial_request(raw_json: Union[bytes, str], admin: Admin, credentials: Credentials) \
        -> Tuple[Admin, Optional[Credentials]]:
    json_obj = parse_raw_json(raw_json)
    validate_admin_credentials_partial_request(json_obj)
    admin = Admin.from_dict({**admin.as_json_dict(), **json_obj})
    new_credentials = None
    if "credentials" in json_obj and json_obj["credentials"]:
        json_obj["credentials"]["email"] = admin.email
        new_credentials = Credentials.from_dict({**credentials.as_json_dict(), **json_obj["credentials"]})
    return admin, new_credentials


def validate_recruiter_credentials_request(raw_json: str, schema=None):
    err_prefix = "Can not parse request data: "

    try:
        validate(raw_json, schema or RECRUITER_CREDENTIALS_REQUEST_SCHEMA)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_recruiter_credentials_request(raw_json: Union[bytes, str]) -> Tuple[Recruiter, Optional[Credentials]]:
    json_obj = parse_raw_json(raw_json)
    validate_recruiter_credentials_request(json_obj)
    recruiter = Recruiter.from_dict(json_obj)
    credentials = None
    if "credentials" in json_obj and json_obj["credentials"]:
        json_obj["credentials"]["email"] = recruiter.email
        if "password" in json_obj["credentials"]:
            json_obj["credentials"]["raw_password"] = json_obj["credentials"]["password"]
            del json_obj["credentials"]["password"]
        credentials = Credentials.from_dict(json_obj["credentials"])
    return recruiter, credentials


def validate_recruiter_credentials_partial_request(raw_json: str, schema=None):
    err_prefix = "Can not parse request data: "

    try:
        validate(raw_json, schema or RECRUITER_CREDENTIALS_PARTIAL_REQUEST_SCHEMA)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_recruiter_credentials_partial_request(raw_json: Union[bytes, str], recruiter: Recruiter,
                                                credentials: Credentials) -> Tuple[Recruiter, Optional[Credentials]]:
    json_obj = parse_raw_json(raw_json)
    validate_recruiter_credentials_partial_request(json_obj)
    recruiter = Recruiter.from_dict({**recruiter.as_json_dict(), **json_obj})
    new_credentials = None
    if "credentials" in json_obj and json_obj["credentials"]:
        json_obj["credentials"]["email"] = recruiter.email
        new_credentials = Credentials.from_dict({**credentials.as_json_dict(), **json_obj["credentials"]})
    return recruiter, new_credentials
