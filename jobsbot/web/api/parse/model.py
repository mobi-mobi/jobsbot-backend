from datetime import datetime

from jsonschema import validate, ValidationError, SchemaError
from typing import Any, Union, Optional
from validators import url as validate_url, ValidationFailure

from jobsbot.core.exceptions import JobsBotSchemaValidationException
from jobsbot.core.models import Company, Job, RecruiterInvitation
from jobsbot.web.api.parse.common import parse_raw_json
from jobsbot.web.api.parse.schemas import COMPANY_SCHEMA, COMPANY_SCHEMA_PARTIAL, JOB_SCHEMA, JOB_SCHEMA_PARTIAL


def validate_json(json: Any, schema: dict):
    err_prefix = "Can not parse object parameters: "

    try:
        validate(json, schema)
    except ValidationError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise JobsBotSchemaValidationException(err_prefix + e.message) from None


def parse_company(raw_json: Union[bytes, str]) -> Company:
    json_obj = parse_raw_json(raw_json)
    validate_json(json_obj, COMPANY_SCHEMA)
    return Company.from_dict(json_obj)


def parse_company_partial(old_company: Company, raw_json: Union[bytes, str]) -> Company:
    json_obj = parse_raw_json(raw_json)
    validate_json(json_obj, COMPANY_SCHEMA_PARTIAL)
    return Company.from_dict({**old_company.as_json_dict(), **json_obj})


def parse_job(raw_json: Union[bytes, str]) -> Job:
    json_obj = parse_raw_json(raw_json)
    validate_json(json_obj, JOB_SCHEMA)
    job = Job.from_dict(json_obj)
    job_url_is_valid = True
    try:
        if isinstance(validate_url(job.job_url, public=True), ValidationFailure):
            job_url_is_valid = False
    except TypeError:
        job_url_is_valid = False
    if not job.allow_one_click_applications and not job_url_is_valid:
        raise JobsBotSchemaValidationException("You have to use a valid URL as you disabled one click "
                                               "application type")
    if job.allow_non_complete_profiles and not job_url_is_valid:
        raise JobsBotSchemaValidationException("You have to use a valid URL as you accept non complete profiles")
    return Job.from_dict(json_obj)


def parse_job_partial(old_job: Job, raw_json: Union[bytes, str]) -> Job:
    json_obj = parse_raw_json(raw_json)
    validate_json(json_obj, JOB_SCHEMA_PARTIAL)
    job = Job.from_dict({**old_job.as_json_dict(), **json_obj})
    job_url_is_valid = True
    try:
        if isinstance(validate_url(job.job_url, public=True), ValidationFailure):
            job_url_is_valid = False
    except TypeError:
        job_url_is_valid = False
    if not job.allow_one_click_applications and not job_url_is_valid:
        raise JobsBotSchemaValidationException("You have to use a valid URL as you disabled one click "
                                               "application type")
    if job.allow_non_complete_profiles and not job_url_is_valid:
        raise JobsBotSchemaValidationException("You have to use a valid URL as you accept non complete profiles")
    return job
