from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler

from jobsbot.bot.telegram.callbacks.main import refer_a_friend, help_message, unknown_command_message, \
    close_quick_dialog
from jobsbot.bot.telegram.callbacks.start import start_command
from jobsbot.bot.telegram.callbacks.user_job_applications import apply_for_job, cancel_job_application
from jobsbot.bot.telegram.conversations.profile import profile_conversation
from jobsbot.bot.telegram.conversations.settings import settings_conversation
from jobsbot.bot.telegram.conversations.subscriptions import subscriptions_conversation
from jobsbot.bot.telegram.conversations.feedback import feedback_conversation
from jobsbot.settings import get_tg_token, get_env


"""
BOT COMMANDS:

profile - Manage your profile
settings - Manage your settings & filters
subscriptions - Manage the list of companies you follow
refer - Recommend us to your friends!
feedback - Share your feedback with us
help - Help
"""


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    tg_token = get_tg_token(get_env())
    updater = Updater(tg_token, use_context=True, workers=16)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(profile_conversation(), 1)
    dispatcher.add_handler(settings_conversation(), 2)
    dispatcher.add_handler(subscriptions_conversation(), 3)
    dispatcher.add_handler(feedback_conversation(), 4)

    dispatcher.add_handler(CommandHandler("start", start_command), 11)
    dispatcher.add_handler(CommandHandler("help", help_message), 12)
    dispatcher.add_handler(CommandHandler("refer", refer_a_friend), 13)

    # User job application handlers
    dispatcher.add_handler(CallbackQueryHandler(apply_for_job, pattern="^apply_for_job:"), 21)
    dispatcher.add_handler(CallbackQueryHandler(apply_for_job, pattern="^apply_for_job_on_website:"), 21)
    dispatcher.add_handler(CallbackQueryHandler(cancel_job_application, pattern="^cancel_job_application:"), 22)

    # on noncommand i.e message - echo the message on Telegram
    dispatcher.add_handler(CallbackQueryHandler(close_quick_dialog, pattern="^cancel_quick_"), 31)
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, unknown_command_message), 32)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    main()
