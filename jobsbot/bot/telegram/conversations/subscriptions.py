from telegram.ext import ConversationHandler, CommandHandler, CallbackQueryHandler

from jobsbot.bot.telegram.callbacks.subscriptions import subscriptions_cancel, subscriptions_main,\
    subscriptions_manage_subscription


def subscriptions_conversation() -> ConversationHandler:
    return ConversationHandler(
        entry_points=[CommandHandler("subscriptions", subscriptions_main)],
        states={
            "subscriptions_main": [
                CallbackQueryHandler(subscriptions_manage_subscription, pattern="^subscriptions:[a-zA-Z0-9]+$"),
                CallbackQueryHandler(subscriptions_manage_subscription, pattern="^subscriptions:1$"),
            ],
            "subscriptions_manage_subscription": [
                CallbackQueryHandler(subscriptions_manage_subscription, pattern="^subscriptions:enable:[a-zA-Z0-9]+$"),
                CallbackQueryHandler(subscriptions_manage_subscription, pattern="^subscriptions:disable:[a-zA-Z0-9]+$"),
                CallbackQueryHandler(subscriptions_manage_subscription,
                                     pattern="^subscriptions:subscribe:[a-zA-Z0-9]+$"),
                CallbackQueryHandler(subscriptions_manage_subscription,
                                     pattern="^subscriptions:unsubscribe:[a-zA-Z0-9]+$"),
                CallbackQueryHandler(subscriptions_main, pattern="^subscriptions_main$")
            ],
        },
        fallbacks=[
            CommandHandler("subscriptions", subscriptions_main),
            CallbackQueryHandler(subscriptions_cancel, pattern="^subscriptions_close$")
        ]
    )
