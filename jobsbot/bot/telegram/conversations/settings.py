from jobsbot.core.enums import JobCategory, SkillsCategory, UserPreferences, UserCountry, \
    UserDesiredMinAnnualGrossSalary, UserNotificationOption
from jobsbot.core.enum_maps import JOBS_TREE, SKILLS_TREE, COUNTRY_COUNTRY_LOCATIONS_MAP
from jobsbot.bot.telegram.callbacks.settings import settings_main, settings_cancel, settings_salary,\
    settings_job_categories, settings_job_positions, settings_skills_categories, settings_skills, \
    settings_preferences, settings_refer_a_friend, settings_notifications, settings_user_relocation_location

from telegram.ext import ConversationHandler, CommandHandler, CallbackQueryHandler


def settings_conversation() -> ConversationHandler:
    return ConversationHandler(
        allow_reentry=True,
        entry_points=[
            CommandHandler("settings", settings_main),
            CallbackQueryHandler(settings_job_categories, pattern="^quick_settings_job_categories$")
        ],
        fallbacks=[
            CommandHandler("settings", settings_main),
            CallbackQueryHandler(settings_cancel, pattern="^settings_cancel$")
        ],
        states={
            "settings_main": [
                CallbackQueryHandler(settings_notifications, pattern="^settings_notifications$"),
                CallbackQueryHandler(settings_salary, pattern="^settings_salary$"),
                CallbackQueryHandler(settings_job_categories, pattern="^settings_job_categories"),
                CallbackQueryHandler(settings_skills_categories, pattern="^settings_skill_categories$"),
                CallbackQueryHandler(settings_preferences, pattern="^settings_preferences$"),
                CallbackQueryHandler(settings_user_relocation_location, pattern="^settings_relocation$"),
                CallbackQueryHandler(settings_refer_a_friend, pattern="^settings_refer_a_friend$")
            ],
            "settings_salary": [
                CallbackQueryHandler(settings_salary, pattern=f"^settings_salary:{salary.value}$")
                for salary in UserDesiredMinAnnualGrossSalary
            ] + [
                CallbackQueryHandler(settings_main, pattern="^settings_main$")
            ],
            "settings_relocation": [
                CallbackQueryHandler(settings_user_relocation_location,
                                     pattern=f"^settings_relocation:{country.value}$")
                for country in UserCountry
            ] + [
                CallbackQueryHandler(settings_user_relocation_location, pattern="^settings_relocation:none$"),
                CallbackQueryHandler(settings_main, pattern="^settings_main$"),
            ],
            **{
                f"settings_relocation:{country.value}": [
                    CallbackQueryHandler(settings_user_relocation_location,
                                         pattern=f"^settings_relocation:{country.value}:{city.value}$")
                    for city in COUNTRY_COUNTRY_LOCATIONS_MAP.get(country, [])
                ] + [
                    CallbackQueryHandler(settings_user_relocation_location,
                                         pattern=f"^settings_relocation:{country.value}:any$"),
                    CallbackQueryHandler(settings_user_relocation_location, pattern="^settings_relocation$"),
                ] for country in UserCountry
            },
            "settings_job_categories": [
                CallbackQueryHandler(settings_job_positions, pattern=f"^settings_job_category:{category.value}$")
                for category in JobCategory
            ] + [
                CallbackQueryHandler(settings_main, pattern="^settings_main$")
            ],
            **{
                f"settings_job_category_{category.value}": [
                    CallbackQueryHandler(settings_job_positions,
                                         pattern=f"^settings_job_position:{category.value}:{position.value}$")
                    for position in JOBS_TREE[category]
                ] + [
                    CallbackQueryHandler(settings_job_categories, pattern="^settings_job_categories$"),
                ] for category in JobCategory
            },
            "settings_skill_categories": [
                CallbackQueryHandler(settings_skills, pattern=f"^settings_skills_category:{category.value}$")
                for category in SkillsCategory
            ] + [
                CallbackQueryHandler(settings_main, pattern="^settings_main$")
            ],
            ** {
                f"settings_skills_category_{category.value}": [
                    CallbackQueryHandler(settings_skills, pattern=f"^settings_skill:{category.value}:{skill.value}$")
                    for skill in SKILLS_TREE[category]
                ] + [
                    CallbackQueryHandler(settings_skills_categories, pattern="^settings_skill_categories$")
                ] for category in SkillsCategory
            },
            "settings_preferences": [
                CallbackQueryHandler(settings_preferences, pattern=f"^settings_preferences:{preference.value}$")
                for preference in UserPreferences
            ] + [
                CallbackQueryHandler(settings_main, pattern="^settings_main$")
            ],
            "settings_refer_a_friend": [
                CallbackQueryHandler(settings_main, pattern="^settings_main$")
            ],
            "settings_notifications": [
                CallbackQueryHandler(settings_notifications, pattern=f"^settings_notifications:{option.value}$")
                for option in UserNotificationOption
            ] + [
                CallbackQueryHandler(settings_main, pattern="^settings_main$")
            ]
        }
    )
