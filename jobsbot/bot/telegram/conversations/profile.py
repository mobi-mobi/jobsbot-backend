from telegram.ext import ConversationHandler, CommandHandler, MessageHandler, Filters, CallbackQueryHandler

from jobsbot.bot.telegram.callbacks.profile import profile_main, profile_name, profile_current_company, \
    profile_current_position, profile_email, profile_cv_upload, profile_location, profile_experience, profile_cancel

from jobsbot.core.enums import UserCountry, UserExperience

from jobsbot.core.enum_maps import COUNTRY_COUNTRY_LOCATIONS_MAP


def profile_conversation() -> ConversationHandler:
    return ConversationHandler(
        allow_reentry=True,
        entry_points=[
            CommandHandler("profile", profile_main),
            CallbackQueryHandler(profile_main, pattern="^quick_user_profile$"),
            CallbackQueryHandler(profile_location, pattern="^quick_profile_location$")
        ],
        fallbacks=[
            CommandHandler("profile", profile_main),
            CallbackQueryHandler(profile_cancel, pattern="^profile_cancel$")
        ],
        states={
            "profile_main": [
                CallbackQueryHandler(profile_name, pattern="^profile:name$"),
                CallbackQueryHandler(profile_current_company, pattern="^profile:current_company$"),
                CallbackQueryHandler(profile_current_position, pattern="^profile:current_position$"),
                CallbackQueryHandler(profile_email, pattern="^profile:email$"),
                CallbackQueryHandler(profile_cv_upload, pattern="^profile:upload_cv$"),
                CallbackQueryHandler(profile_location, pattern="^profile:location$"),
                CallbackQueryHandler(profile_experience, pattern="^profile:experience"),
            ],
            "profile:name": [
                CallbackQueryHandler(profile_main, pattern="^profile$"),
                MessageHandler(filters=Filters.text & (~Filters.command), callback=profile_name),
                MessageHandler(Filters.command, callback=profile_cancel)
            ],
            "profile:current_company": [
                CallbackQueryHandler(profile_main, pattern="^profile$"),
                MessageHandler(filters=Filters.text & (~Filters.command), callback=profile_current_company),
                MessageHandler(Filters.command, callback=profile_cancel)
            ],
            "profile:current_position": [
                CallbackQueryHandler(profile_main, pattern="^profile$"),
                MessageHandler(filters=Filters.text & (~Filters.command), callback=profile_current_position),
                MessageHandler(Filters.command, callback=profile_cancel)
            ],
            "profile:email": [
                CallbackQueryHandler(profile_main, pattern="^profile$"),
                MessageHandler(filters=Filters.text & (~Filters.command), callback=profile_email),
                MessageHandler(Filters.command, callback=profile_cancel)
            ],
            "profile:upload_cv": [
                CallbackQueryHandler(profile_main, pattern="^profile$"),
                MessageHandler(filters=Filters.document, callback=profile_cv_upload)
            ],
            "profile:experience": [
                CallbackQueryHandler(profile_experience, pattern=f"profile:experience:{experience.value}$")
                for experience in UserExperience
            ] + [
                CallbackQueryHandler(profile_main, pattern="^profile$"),
            ],
            "profile:location": [
                                    CallbackQueryHandler(profile_location, pattern=f"^profile:"
                                                                                   f"location:{country.value}$")
                                    for country in UserCountry
                                ] + [CallbackQueryHandler(profile_main, pattern="^profile$")],
            **{
                f"profile:location:{country.value}": [
                                                         CallbackQueryHandler(
                                                             profile_location,
                                                             pattern=f"^profile:location:{country.value}:{city.value}$"
                                                         )
                                                         for city in COUNTRY_COUNTRY_LOCATIONS_MAP.get(country, [])
                                                      ] + [
                                                         CallbackQueryHandler(
                                                             profile_location,
                                                             pattern=f"^profile:location:{country.value}:none$"
                                                         ),
                                                         CallbackQueryHandler(
                                                             profile_location,
                                                             pattern="^profile:location$"),
                                                      ] for country in UserCountry
            },
        }
    )
