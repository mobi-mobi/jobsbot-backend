from telegram.ext import ConversationHandler, CommandHandler, CallbackQueryHandler, Filters, MessageHandler

from jobsbot.bot.telegram.callbacks.feedback import feedback_cancel, feedback_main, feedback_sent


def feedback_conversation() -> ConversationHandler:
    return ConversationHandler(
        entry_points=[CommandHandler("feedback", feedback_main)],
        states={
            "feedback_main": [
                MessageHandler(filters=Filters.text & (~Filters.command), callback=feedback_sent)
            ],
        },
        fallbacks=[
            CommandHandler("feedback", feedback_main),
            CallbackQueryHandler(feedback_cancel, pattern="^feedback_cancel$"),
            MessageHandler(Filters.text, callback=lambda *_: ConversationHandler.END)
        ]
    )
