WARNING = "⚠️"

CANCEL = "❌"
CLOSE = "❌"
CHECKBOX = "✅"
RETURN = "↩️"

CV = "📄"
EXPERIENCE = "🎓"
MONEY = "💶"
POSITIONS = "📑"
SKILLS = "🔧"
PREFERENCES = "⚙️"
RELOCATION = "✈️"

RIGHT = "➡️"
BELL = "🔔"
BELL_CROSSED = "🔕"
WAVING_HAND = "👋"

APPLY_FOR_JOB = "👌"

SUBSCRIBE = "👌"
UNSUBSCRIBE = "🙅"
LINK = "🔗"
