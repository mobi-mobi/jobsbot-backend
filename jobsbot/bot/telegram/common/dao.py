from datetime import datetime
from typing import Optional

from jobsbot.core.models import User
from jobsbot.dao import get_user_dao


__all__ = [
    "get_user"
]


def get_user(tg_id: int, tg_chat_id: Optional[int] = None) -> User:
    user_dao = get_user_dao()
    user = user_dao.read_user(tg_id=tg_id, create_if_doesnt_exist=True)
    user_dao.update_user_last_interaction_time(tg_id=user.tg_id, last_interaction_time=datetime.utcnow())
    if tg_chat_id is not None and user.tg_chat_id != tg_chat_id:
        user_dao.update_user_tg_chat_id(tg_id=user.tg_id, tg_chat_id=tg_chat_id)
    return user
