import io
import textwrap

from PIL import Image, ImageDraw, ImageFont
from PIL.ImageFont import FreeTypeFont
from typing import Optional, Tuple, List

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Bot
from telegram.error import BadRequest
from telegram.utils.helpers import escape_markdown

from jobsbot.bot.telegram.common.emoji import APPLY_FOR_JOB, CANCEL, WARNING, LINK
from jobsbot.core.models import Company, Job, User


def em(text: str):
    return escape_markdown(text, version=2)


def single_job_found_message_text(job: Job, company: Company, user: User, is_applied: bool,
                                  is_request_cancelled: bool, is_too_many_requests: bool) -> str:
    text = ""

    text += "*" + em(job.title) + "*" + em(" @ ") + "*" + em(company.company_name) + "*"

    # if job.department_name:
    #     text += em(f"\nDepartment: {job.department_name}")

    if job.show_annual_salary:
        text += f"\n\n*Salary{em(':')}* ≈ {em(str(job.annual_salary_min // 1000))} {em('-')} " \
                + f"{em(str(job.annual_salary_max // 1000))}K EUR/year"
    else:
        text += f"\n\n*Salary{em(':')}* Matches your expectations {em('(')}{em(user.min_annual_salary.title())}{em(')')}"

    # if job.hiring_manager_name:
    #     text += em(f"\nHiring Manager: {job.hiring_manager_name}")

    if job.locations or job.fully_remote_allowed:
        text += f"\n\n*Location{em(':')}* "
        if job.locations:
            text += em(", ".join([loc.title() for loc in job.locations]))
        if job.fully_remote_allowed:
            if job.locations:
                text += em(", ")
            text += "Remote"

    if job.description:
        text += f"\n\n*Description{em(':')}* {em(job.description)}"

    if job.technologies:
        text += f"\n\n*Technologies{em(':')}* {em(job.technologies)}"

    if job.about_company:
        text += f"\n\n*About Company{em(':')}* {em(job.about_company)}"

    if is_applied:
        text += em(f"\n\n{APPLY_FOR_JOB}") + "*Applied*" + em("!")

    if is_request_cancelled:
        text += em(f"\n\n{APPLY_FOR_JOB}") + "*Job request cancelled*" + em("!")

    if is_too_many_requests:
        text += em(f"\n\n{WARNING}") + "*" \
                + em("You sent too many requests. You can not apply for this position in one click anymore") + "*"

    return text


def single_job_found_message_keyboard_markup(job: Job, show_apply_button: bool = False,
                                             show_cancel_button: bool = False, show_apply_on_site_button: bool = False,
                                             apply_on_website_url: str = None, apply_on_website_text: str = None) \
        -> Optional[InlineKeyboardMarkup]:
    keyboard = []
    if show_apply_button:
        keyboard.append([
            InlineKeyboardButton(f"{APPLY_FOR_JOB} Apply for this job in one click!",
                                 callback_data=f"apply_for_job:{job.job_id}")
        ])
    if show_cancel_button:
        keyboard.append([
            InlineKeyboardButton(f"{CANCEL} Cancel your request",
                                 callback_data=f"cancel_job_application:{job.job_id}")
        ])
    if show_apply_on_site_button and apply_on_website_url and apply_on_website_text:
        keyboard.append([
            InlineKeyboardButton(f"{LINK} {em(apply_on_website_text)}",
                                 url=apply_on_website_url,
                                 callback_data=f"apply_for_job_on_website:{job.job_id}")
        ])
    return InlineKeyboardMarkup(keyboard) if keyboard else None


def single_job_found_message_image(job: Job, company: Company) -> bytes:
    def get_text_dimensions(text_string: str, font_: ImageFont) -> Tuple[int, int]:
        # https://stackoverflow.com/a/46220683/9263761
        ascent, descent = font_.getmetrics()
        text_width = font_.getmask(text_string).getbbox()[2]
        text_height = font_.getmask(text_string).getbbox()[3] + descent
        return text_width, text_height

    def find_font(text_string: str, font_file_: str, max_width: int = None, max_height: int = None, min_size: int = 5,
                  max_size: int = 14, prefer_larger: bool = True) -> Optional[Tuple[FreeTypeFont, int, int]]:
        it = range(min_size, max_size + 1)
        if prefer_larger:
            it = range(max_size, min_size - 1, -1)
        for font_size in it:
            font_: ImageFont = ImageFont.truetype(font_file_, font_size)
            w, h = get_text_dimensions(text_string, font_)
            if max_width is not None and w > max_width:
                continue
            if max_height is not None and h > max_height:
                continue
            return font_, w, h

    def text_to_lines(text_string: str, font_file_: str, best_wrap_width: int = 20, max_wrap_width: int = None,
                      max_lines: int = 1, max_width: int = None, max_height: int = None, min_size: int = 5,
                      max_size: int = 14, prefer_larger: bool = True) \
            -> Optional[Tuple[FreeTypeFont, List[Tuple[str, int, int]]]]:
        placeholder = "..."
        text = []
        max_wrap_width = max_wrap_width or best_wrap_width
        if max_wrap_width < best_wrap_width:
            max_wrap_width = best_wrap_width
        for wrap_width in range(best_wrap_width, max_wrap_width + 1):
            text = textwrap.wrap(
                text_string, width=wrap_width, max_lines=max_lines, expand_tabs=False, break_long_words=True,
                placeholder=placeholder
            )
            if not text or not text[-1].endswith(placeholder):
                break
        font_size = None
        for text_line in text:
            font = find_font(text_line, font_file_=font_file_, max_width=max_width, max_height=max_height,
                             min_size=min_size, max_size=max_size, prefer_larger=prefer_larger)
            if font is None:
                return None
            if font_size is None:
                font_size = font[0].size
            else:
                font_size = min(font_size, font[0].size)
        font_ = ImageFont.truetype(font_file_, font_size)
        res = []
        for text_line in text:
            w, h = get_text_dimensions(text_line, font_)
            res.append((text_line, w, h))

        return font_, res

    image: Image = Image.open("img/bot/job-announce-bg.png")
    d = ImageDraw.Draw(image)

    width, height = image.size
    padding = 30
    adj_width = width - 2 * padding
    current_height = 20

    title_font, title_lines = text_to_lines(
        job.title, "fonts/nunito/NunitoBold.ttf",
        max_width=adj_width,
        best_wrap_width=20,
        max_wrap_width=40,
        max_lines=2,
        max_size=47,
        min_size=32
    )

    for line, w, h in title_lines:
        d.text(
            xy=(padding, current_height),
            text=line,
            font=title_font,
            fill=(0, 0, 0, 255)
        )
        current_height += h

    at_company = "@ " + company.company_name
    at_company_font, at_company_lines = text_to_lines(
        at_company, "fonts/nunito/NunitoRegular.ttf",
        max_width=adj_width,
        max_lines=1,
        max_size=40,
        min_size=35
    )
    at_company, w, h = at_company_lines[0]
    d.text(
        xy=(padding, current_height),
        text=at_company,
        font=at_company_font,
        fill=(0, 0, 0, 255)
    )
    current_height += h

    if job.department_name:
        current_height += 30

        department_font, department_lines = text_to_lines(
            "Department: " + job.department_name, "fonts/nunito/NunitoRegular.ttf",
            max_width=adj_width,
            best_wrap_width=30,
            max_lines=2,
            max_size=25,
            min_size=20
        )

        for line, w, h in department_lines:
            d.text(
                xy=(padding, current_height),
                text=line,
                font=department_font,
                fill=(0, 0, 0, 255)
            )
            current_height += h

    if job.hiring_manager_name:
        current_height += 30

        hiring_manager_font, hiring_manager_lines = text_to_lines(
            "Hiring Manager: " + job.hiring_manager_name, "fonts/nunito/NunitoRegular.ttf",
            max_width=adj_width,
            best_wrap_width=30,
            max_lines=2,
            max_size=25,
            min_size=20
        )

        for line, w, h in hiring_manager_lines:
            d.text(
                xy=(padding, current_height),
                text=line,
                font=hiring_manager_font,
                fill=(0, 0, 0, 255)
            )
            current_height += h

    if current_height + 20 < height:
        image = image.crop((0, 0, width, current_height + 20))

    bytes_io = io.BytesIO()
    image.save(bytes_io, format="PNG")
    return bytes_io.getvalue()


def delete_user_dialog_messages(bot: Bot, user: User, keep_message_ids: List[int] = None):
    for message_id in user.message_ids():
        if keep_message_ids and message_id in keep_message_ids:
            continue
        try:
            bot.delete_message(user.tg_id, message_id)
        except BadRequest:
            pass
