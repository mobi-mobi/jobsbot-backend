import re

from string import ascii_lowercase, digits

__all__ = [
    "allowed_name_characters",
    "allowed_name_characters_set",
    "remove_not_allowed_symbols_from_name",
    "canonize_user_name",
    "remove_not_allowed_symbols_from_company_name",
    "canonize_company_name",
    "remove_not_allowed_symbols_from_job_title",
    "canonize_job_title",
    "canonize_user_feedback"
]

allowed_name_characters = ascii_lowercase \
                          + "ǿšžœÿßőűḃċḋḟġṁṗṡṫşẁẃŵẅỳŷ" \
                          + "ðñòóôõöøùúûüýþÿêęa" \
                          + "'`-., "

allowed_name_characters_set = set(allowed_name_characters)

allowed_company_characters = allowed_name_characters + digits
allowed_company_characters_set = set(allowed_company_characters)

allowed_job_title_characters = allowed_name_characters
allowed_job_title_characters_set = set(allowed_job_title_characters)

allowed_feedback_characters = allowed_name_characters \
                              + "\\/!@#$%^&*()_+={}[]\"'<>;:?`~" \
                              + "абвгдеёжзиклмнопрстуфхцчшщыъьэюя"
allowed_feedback_characters_set = set(allowed_feedback_characters)


def remove_not_allowed_symbols_from_name(name: str) -> str:
    return "".join(s for s in name if s.lower() in allowed_name_characters_set)


def canonize_user_name(name: str, max_len: int = 60) -> str:
    return remove_not_allowed_symbols_from_name(re.sub(' +', ' ', name)).strip()[0:max_len].strip()


def remove_not_allowed_symbols_from_company_name(company_name: str) -> str:
    return "".join(s for s in company_name if s.lower() in allowed_company_characters_set)


def canonize_company_name(company_name: str, max_len: int = 60) -> str:
    return remove_not_allowed_symbols_from_company_name(re.sub(' +', ' ', company_name)).strip()[0:max_len].strip()


def remove_not_allowed_symbols_from_job_title(company_name: str) -> str:
    return "".join(s for s in company_name if s.lower() in allowed_job_title_characters_set)


def canonize_job_title(job_title: str, max_len: int = 60) -> str:
    return remove_not_allowed_symbols_from_job_title(re.sub(' +', ' ', job_title)).strip()[0:max_len].strip()


def canonize_user_feedback(msg: str, max_len: int = 10000, max_lines: int = 20) -> str:
    result = ""
    nl_sym = "\n"
    lines = 0
    for line in msg.split("\n"):
        if not line:
            continue
        line = "".join(s for s in line if s.lower() in allowed_feedback_characters_set)
        if result:
            result += nl_sym
        result += line
        if len(result) > max_len:
            return result[0:max_len]
        lines += 1
        if lines == max_lines:
            nl_sym = " // "
    return result
