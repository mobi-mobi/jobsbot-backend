import re
import tempfile

from datetime import datetime, timedelta

from telegram import Document, Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import BadRequest
from telegram.ext import CallbackContext, ConversationHandler, DispatcherHandlerStop
from traceback import format_exc

from jobsbot.bot.telegram.callbacks.inline_keyboards import profile_countries_keyboard, profile_experience_keyboard

from jobsbot.bot.telegram.common.dao import get_user
from jobsbot.bot.telegram.common.emoji import CLOSE, RETURN, WARNING
from jobsbot.bot.telegram.common.messages import delete_user_dialog_messages
from jobsbot.bot.telegram.common.string import canonize_user_name, canonize_company_name, canonize_job_title
from jobsbot.core.enums import UserCountry, UserExperience, CountryLocations, UserProfileRequiredPart, \
    UserProfileValidationStatus
from jobsbot.core.enum_maps import COUNTRY_COUNTRY_LOCATIONS_MAP
from jobsbot.core.logging import get_logger
from jobsbot.core.models import UserCv

from jobsbot.dao import get_user_dao


def profile_main(update: Update, _: CallbackContext) -> str:
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    if update.message:
        tg_id = update.message.from_user.id
        try:
            update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=update.message.message_id)
        except BadRequest:
            pass
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    user = get_user(tg_id, tg_chat_id)

    in_menu_user_name = user.name or "<not set>"
    in_menu_user_name_warn = f"{WARNING} " if not user.name else ""
    if len(in_menu_user_name) > 20:
        in_menu_user_name = in_menu_user_name[:17] + "..."

    if user.user_country is None:
        in_menu_user_location_warn = f"{WARNING} "
        in_menu_user_location = "<not set>"
    else:
        in_menu_user_location_warn = ""
        in_menu_user_location = user.user_country.title()
        if user.user_country_location is not None:
            in_menu_user_location = f"{user.user_country_location.title()}, {in_menu_user_location}"

    in_menu_email = user.email or "<not set>"
    in_menu_email_warn = f"{WARNING} " if not user.email else ""
    if in_menu_email.find("@") != -1:
        email_1, email_2 = in_menu_email.split("@", maxsplit=2)
        if len(email_1) > 2:
            email_1 = email_1[:len(email_1) // 3] + "..." + email_1[-len(email_1) // 3:]
        if len(email_2) > 15:
            email_2 = email_2[:len(email_2) // 3] + "..." + email_2[-len(email_2) // 3:]
        in_menu_email = email_1 + "@" + email_2

    cv_uploaded = get_user_dao().is_cv_uploaded(tg_id)
    in_menu_cv_status = "Uploaded" if cv_uploaded else "Not Uploaded"
    in_menu_cv_warn = f"{WARNING} " if not cv_uploaded else ""

    in_menu_experience_warn = f"{WARNING} " if user.experience is None else ""
    in_menu_experience = user.experience.title() if user.experience is not None else "<not set>"

    in_menu_current_company_warn = f"{WARNING} " if not user.current_company_name else ""
    in_menu_current_company = user.current_company_name if user.current_company_name else "<not set>"
    if len(in_menu_current_company) > 20:
        in_menu_current_company = in_menu_current_company[:17] + "..."

    in_menu_current_position_warn = f"{WARNING} " if not user.current_position_title else ""
    in_menu_current_position = user.current_position_title if user.current_position_title else "<not set>"
    if len(in_menu_current_position) > 20:
        in_menu_current_position = in_menu_current_position[:17] + "..."

    keyboard = [
        [
            InlineKeyboardButton(f"{in_menu_user_name_warn}Name: {in_menu_user_name}",
                                 callback_data="profile:name"),
        ],
        [
            InlineKeyboardButton(f"{in_menu_current_company_warn}Current Company: {in_menu_current_company}",
                                 callback_data="profile:current_company"),
        ],
        [
            InlineKeyboardButton(f"{in_menu_current_position_warn}Current Position: {in_menu_current_position}",
                                 callback_data="profile:current_position"),
        ],
        [
            InlineKeyboardButton(f"{in_menu_user_location_warn}Location: {in_menu_user_location}",
                                 callback_data="profile:location"),
        ],
        [
            InlineKeyboardButton(f"{in_menu_email_warn}Email: {in_menu_email}",
                                 callback_data="profile:email"),
        ],
        [
            InlineKeyboardButton(f"{in_menu_cv_warn}CV: {in_menu_cv_status}",
                                 callback_data="profile:upload_cv"),
        ],
        [
            InlineKeyboardButton(f"{in_menu_experience_warn} Experience: {in_menu_experience}",
                                 callback_data="profile:experience")
        ],
        [
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel"),
        ]
    ]

    text = "This information will become visible to companies whose vacancies you will send your resume to. \n\n"
    text += "Each time you update your profile, its status becomes \"Not-verified\" and you will not be able to " \
            "apply for open positions until our moderators review it.\n\n"
    text += f"Your current profile status is \"{user.validation_status.title()}\" which means that "
    if user.validation_status == UserProfileValidationStatus.NOT_READY:
        text += "we're still waiting from you to complete your profile."
    elif user.validation_status == UserProfileValidationStatus.WAITING_VERIFICATION:
        text += "it will be soon reviewed and verified or rejected by our moderators. Please, give us some time."
    elif user.validation_status == UserProfileValidationStatus.REJECTED:
        text += "something is wrong with the information your provided."
    elif user.validation_status == UserProfileValidationStatus.VERIFIED:
        text += "your profile was successfully validated! Congratulations! You can now apply for open positions."
    if user.missing_profile_parts:
        text += f"\n\n{WARNING} In order for your profile to be verified, please, update the next part(s):\n"
        for missing_part in user.missing_profile_parts:
            text += f"\n * {missing_part.title()}"

    if update.message:
        delete_user_dialog_messages(update.message.bot, user)
        profile_msg = update.message.reply_text(text=text, reply_markup=InlineKeyboardMarkup(keyboard))
        get_user_dao().update_user_tg_message_ids(user.tg_id, tg_profile_message_id=profile_msg.message_id)
    elif update.callback_query:
        update_message_id = False
        if update.callback_query.message.message_id != user.tg_settings_message_id:
            delete_user_dialog_messages(update.callback_query.message.bot, user,
                                        keep_message_ids=[update.callback_query.message.message_id])
            update_message_id = True
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text,
            reply_markup=InlineKeyboardMarkup(keyboard)
        )
        if update_message_id:
            get_user_dao().update_user_tg_message_ids(user.tg_id,
                                                      tg_profile_message_id=update.callback_query.message.message_id)

    return "profile_main"


def profile_cancel(update: Update, _: CallbackContext):
    text = 'You can always edit your profile details in /profile and change your settings in /settings'

    if update.message:
        update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=update.message.message_id)

        user = get_user(update.message.from_user.id)
        if user.tg_profile_message_id:
            try:
                update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=user.tg_profile_message_id)
            except BadRequest:
                pass
            get_user_dao().update_user_tg_message_ids(user.tg_id)
        return "feedback_main"
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text(text)

    return ConversationHandler.END


def profile_name(update: Update, _: CallbackContext) -> str:
    keyboard = [
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="profile"),
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    new_name_try = False
    new_name = None
    if update.message:
        update.message.delete()
        tg_id = update.message.from_user.id
        new_name_try = True
        new_name = canonize_user_name(str(update.message.text))
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    message_lines = []

    if new_name_try:
        if new_name:
            if len(new_name) >= 3:
                user.name = new_name
                user.set_profile_detail_as_updated(UserProfileRequiredPart.NAME)
                get_user_dao().save_user(user)
                get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
                message_lines.append("You name was successfully updated!")
            else:
                message_lines.append(f"{WARNING} The name you provided is too short. "
                                     f"Your name should be at least 3 letters long")
        else:
            message_lines.append(f"{WARNING} We were not able to parse your name. Did you send an empty string?")

    current_user_name = "not defined :("
    if user.name:
        current_user_name = f"\"{user.name}\""
    message_lines.append(f"Your current name is {current_user_name}")

    message_lines.append("Please, send your name as a text right now if you want to change it:")

    if update.callback_query is not None:
        update.callback_query.answer()
        update.callback_query.message.edit_text("\n\n".join(message_lines), reply_markup=reply_markup)
    else:
        try:
            update.message.bot.edit_message_text(text="\n\n".join(message_lines), chat_id=update.message.chat_id,
                                                 message_id=user.tg_profile_message_id,
                                                 reply_markup=reply_markup)
        except BadRequest as e:
            if "Message is not modified" not in str(e):
                raise
    raise DispatcherHandlerStop("profile:name")


def profile_cv_upload(update: Update, _: CallbackContext) -> str:
    def _is_pdf_correct(pdf_file: bytes, max_pages: int = 2) -> bool:
        rx_count_pages = re.compile(r"/Type\s*/Page([^s]|$)", re.MULTILINE | re.DOTALL)
        return 1 <= len(rx_count_pages.findall(pdf_file.decode(encoding="utf-8", errors="ignore"))) <= max_pages

    _threshold_interval_minutes = 60
    _threshold_limit = 5
    _max_cv_size_mb = 1
    _max_pages_in_cv = 2

    keyboard = [
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="profile"),
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    new_cv_try = False
    new_cv = None
    if update.message:
        update.message.delete()
        new_cv = update.message.effective_attachment
        tg_id = update.message.from_user.id
        new_cv_try = True
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    message_lines = []

    if new_cv_try:
        if len(list(filter(lambda dt: dt > datetime.utcnow() - timedelta(minutes=_threshold_interval_minutes),
                           user.cv_upload_datetimes[-_threshold_limit:]))) == _threshold_limit:
            message_lines.append(f"{WARNING} Sorry, but you update your CV too often. It's allowed to update CV "
                                 f"no more than {_threshold_limit} times in {_threshold_interval_minutes} minutes.")
        else:
            user.cv_upload_datetimes.append(datetime.utcnow())
            user.cv_upload_datetimes = user.cv_upload_datetimes[-_threshold_limit:]
            get_user_dao().upsert_user(user)

            if new_cv is not None:
                if isinstance(new_cv, Document) and "pdf" in new_cv.mime_type.lower():
                    if new_cv.file_size <= _max_cv_size_mb * 1024 * 1024:
                        with tempfile.NamedTemporaryFile(mode="w+b", delete=True) as named_temp_file:
                            new_cv.get_file().download(custom_path=named_temp_file.name, timeout=10)
                            file_content = named_temp_file.read()

                            if _is_pdf_correct(file_content, max_pages=_max_pages_in_cv):
                                user_cv = UserCv(
                                    tg_id=user.tg_id,
                                    file=file_content,
                                    filename="user_cv.pdf",
                                    upload_date=datetime.utcnow()
                                )
                                get_user_dao().update_user_cv(tg_id=user.tg_id, user_cv=user_cv)
                                user.set_profile_detail_as_updated(UserProfileRequiredPart.CV)
                                get_user_dao().save_user(user)
                                get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
                                message_lines.append("You CV has been successfully uploaded! Thanks :)")
                            else:
                                message_lines.append(f"{WARNING} The file you sent is incorrect. Please, ensure you "
                                                     f"send a valid PDF file which hae no more than "
                                                     f"{_max_pages_in_cv} pages.")
                    else:
                        message_lines.append(f"{WARNING} The file you sent is too large. Maximum allowed size is "
                                             f"{_max_cv_size_mb} MB.")
                else:
                    message_lines.append(f"{WARNING} Looks like the file you sent is not a valid PDF file.")
            else:
                message_lines.append(f"{WARNING} Looks like you have not sent a file.")

    message_lines.append("If you wish to update your CV, please send it as a valid 1- or 2-page PDF file right now.")
    cv_latest_upload_date = get_user_dao().cv_latest_upload_date(user.tg_id)
    message_lines.append(f"Last time you uploaded your CV it was: " +
                         ("never" if cv_latest_upload_date is None
                          else cv_latest_upload_date.strftime("%Y-%m-%d at %H:%M (UTC)")))

    if update.callback_query is not None:
        update.callback_query.answer()
        update.callback_query.message.edit_text("\n\n".join(message_lines), reply_markup=reply_markup)
    else:
        try:
            update.message.bot.edit_message_text(text="\n\n".join(message_lines), chat_id=update.message.chat_id,
                                                 message_id=user.tg_profile_message_id,
                                                 reply_markup=reply_markup)
        except BadRequest as e:
            if "Message is not modified" not in str(e):
                raise

    return "profile:upload_cv"


def profile_location(update: Update, _: CallbackContext) -> str:
    if update.message:
        tg_id = update.message.from_user.id
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)
    query = update.callback_query
    query.answer()
    user_dao = get_user_dao()

    update_message_id = False
    if query.message.message_id != user.tg_settings_message_id:
        # GOT HERE FROM A QUICK NOTIFICATION MESSAGE
        delete_user_dialog_messages(query.message.bot, user, keep_message_ids=[query.message.message_id])
        update_message_id = True

    country = None
    city = None
    unset_city = False
    try:
        if query.data.startswith("profile:location:"):
            country = UserCountry.parse(int(query.data.split(":")[2]))
        if len(query.data.split(":")) == 4:
            if query.data.split(":")[3] == "none":
                unset_city = True
            else:
                city = CountryLocations.parse(int(query.data.split(":")[3]))
    except (ValueError, KeyError, IndexError) as e:
        logger = get_logger()
        logger.error(f"Exception: {e}")
        logger.error(format_exc())
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    message_text = "First, select your country"
    if city is not None or unset_city:
        message_text = "Select your city"
        if unset_city and user.user_country_location is not None:
            user.user_country_location = None
            user.user_country = country
            user.set_profile_detail_as_updated(UserProfileRequiredPart.LOCATION)
            user_dao.save_user(user)
            get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
        if user.user_country_location != city or user.user_country != country:
            user.user_country_location = city
            user.user_country = country
            user.set_profile_detail_as_updated(UserProfileRequiredPart.LOCATION)
            user_dao.save_user(user)
            get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
        keyboard = profile_countries_keyboard(user, country)
    elif country is not None:
        message_text = "Select your city"
        if user.user_country != country and country not in COUNTRY_COUNTRY_LOCATIONS_MAP:
            user.user_country = country
            user.user_country_location = None
            user.set_profile_detail_as_updated(UserProfileRequiredPart.LOCATION)
            user_dao.save_user(user)
            get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
        if country in COUNTRY_COUNTRY_LOCATIONS_MAP:
            keyboard = profile_countries_keyboard(user, country)
        else:
            keyboard = profile_countries_keyboard(user)
    else:
        keyboard = profile_countries_keyboard(user)

    reply_markup = InlineKeyboardMarkup(keyboard)
    try:
        query.edit_message_text(message_text, reply_markup=reply_markup)
    except BadRequest as e:
        if "Message is not modified" not in e.message:
            raise
    if update_message_id:
        get_user_dao().update_user_tg_message_ids(user.tg_id, tg_profile_message_id=query.message.message_id)

    country_suffix = ""
    if country and country in COUNTRY_COUNTRY_LOCATIONS_MAP:
        country_suffix = f":{country.value}"
    return f"profile:location{country_suffix}"


def profile_experience(update: Update, _: CallbackContext) -> str:
    if update.message:
        tg_id = update.message.from_user.id
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)
    query = update.callback_query
    query.answer()
    user_dao = get_user_dao()

    try:
        if query.data.startswith("profile:experience:"):
            experience = UserExperience.parse(int(query.data.split(":")[2]))
            user.experience = experience
            user.set_profile_detail_as_updated(UserProfileRequiredPart.EXPERIENCE)
            user_dao.save_user(user)
            get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
    except (ValueError, KeyError, IndexError) as e:
        logger = get_logger()
        logger.error(f"Exception: {e}")
        logger.error(format_exc())
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    keyboard = profile_experience_keyboard(user)
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        'Please, tell us how many years of experience you have...',
        reply_markup=reply_markup
    )

    return "profile:experience"


def profile_current_company(update: Update, _: CallbackContext) -> str:
    keyboard = [
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="profile"),
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    new_company_try = False
    new_company = None
    if update.message:
        update.message.delete()
        tg_id = update.message.from_user.id
        new_company_try = True
        new_company = canonize_company_name(str(update.message.text))
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    message_lines = []

    if new_company_try:
        if new_company:
            if len(new_company) >= 2:
                user.current_company_name = new_company
                user.set_profile_detail_as_updated(UserProfileRequiredPart.CURRENT_COMPANY_NAME)
                get_user_dao().save_user(user)
                get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
                message_lines.append("You current company name was successfully updated!")
            else:
                message_lines.append(f"{WARNING} The company name you provided is too short. "
                                     f"Company name should be at least 2 letters long")
        else:
            message_lines.append(f"{WARNING} We were not able to parse your company name. "
                                 f"Did you send an empty string?")

    current_company_name = "not defined :("
    if user.current_company_name:
        current_company_name = f"\"{user.current_company_name}\""
    message_lines.append(f"Your current company is {current_company_name}")

    message_lines.append("Please, send your company name as a text right now if you want to change it:")

    if update.callback_query is not None:
        update.callback_query.answer()
        update.callback_query.message.edit_text("\n\n".join(message_lines), reply_markup=reply_markup)
    else:
        try:
            update.message.bot.edit_message_text(text="\n\n".join(message_lines), chat_id=update.message.chat_id,
                                                 message_id=user.tg_profile_message_id,
                                                 reply_markup=reply_markup)
        except BadRequest as e:
            if "Message is not modified" not in str(e):
                raise

    raise DispatcherHandlerStop("profile:current_company")


def profile_current_position(update: Update, _: CallbackContext) -> str:
    keyboard = [
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="profile"),
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    new_position_try = False
    new_position = None
    if update.message:
        update.message.delete()
        tg_id = update.message.from_user.id
        new_position_try = True
        new_position = canonize_job_title(str(update.message.text))
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    message_lines = []

    if new_position_try:
        if new_position:
            if len(new_position) >= 3:
                user.current_position_title = new_position
                user.set_profile_detail_as_updated(UserProfileRequiredPart.CURRENT_POSITION_TITLE)
                get_user_dao().save_user(user)
                get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
                message_lines.append("You current position was successfully updated!")
            else:
                message_lines.append(f"{WARNING} The position you provided is too short. "
                                     f"Position should be at least 3 letters long")
        else:
            message_lines.append(f"{WARNING} We were not able to parse your position. "
                                 f"Did you send an empty string?")

    current_position = "not defined :("
    if user.current_position_title:
        current_position = f"\"{user.current_position_title}\""
    message_lines.append(f"Your current position is {current_position}")

    message_lines.append("Please, send your position as a text right now if you want to change it:")

    if update.callback_query is not None:
        update.callback_query.answer()
        update.callback_query.message.edit_text("\n\n".join(message_lines), reply_markup=reply_markup)
    else:
        try:
            update.message.bot.edit_message_text(text="\n\n".join(message_lines), chat_id=update.message.chat_id,
                                                 message_id=user.tg_profile_message_id,
                                                 reply_markup=reply_markup)
        except BadRequest as e:
            if "Message is not modified" not in str(e):
                raise

    return "profile:current_position"


def profile_email(update: Update, _: CallbackContext) -> str:
    keyboard = [
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="profile"),
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    new_email_try = False
    new_email = None
    if update.message:
        update.message.delete()
        tg_id = update.message.from_user.id
        new_email_try = True
        new_email = update.message.text
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    message_lines = []

    if new_email_try:
        if new_email:
            if len(new_email) >= 5:
                user.email = new_email
                user.set_profile_detail_as_updated(UserProfileRequiredPart.EMAIL)
                get_user_dao().save_user(user)
                get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
                message_lines.append("You email was successfully updated!")
            else:
                message_lines.append(f"{WARNING} The email you provided is too short. "
                                     f"Your email should be at least 5 letters long")
        else:
            message_lines.append(f"{WARNING} We were not able to parse your email. Did you send an empty string?")

    current_user_email = "not defined :("
    if user.name is not None:
        current_user_email = f"\"{user.email}\""
    message_lines.append(f"Your current email is {current_user_email}")

    message_lines.append("Please, send your email as a text right now if you want to change it:")

    if update.callback_query is not None:
        update.callback_query.answer()
        update.callback_query.message.edit_text("\n\n".join(message_lines), reply_markup=reply_markup)
    else:
        try:
            update.message.bot.edit_message_text(text="\n\n".join(message_lines), chat_id=update.message.chat_id,
                                                 message_id=user.tg_profile_message_id,
                                                 reply_markup=reply_markup)
        except BadRequest as e:
            if "Message is not modified" not in str(e):
                raise
    raise DispatcherHandlerStop("profile:email")
