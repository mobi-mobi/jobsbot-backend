from datetime import datetime, timedelta
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import BadRequest
from telegram.ext import CallbackContext, ConversationHandler, DispatcherHandlerStop

from jobsbot.bot.telegram.common.dao import get_user
from jobsbot.bot.telegram.common.emoji import CANCEL
from jobsbot.bot.telegram.common.string import canonize_user_feedback
from jobsbot.core.models import UserFeedback
from jobsbot.dao import get_user_dao


def feedback_main(update: Update, _: CallbackContext):
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    if update.message:
        tg_id = update.message.from_user.id
        try:
            update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=update.message.message_id)
        except BadRequest:
            pass
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    user = get_user(tg_id, tg_chat_id)

    if user.last_user_feedback_sent_at is not None \
            and datetime.utcnow() - user.last_user_feedback_sent_at < timedelta(minutes=5):
        minutes = 5 - int((datetime.utcnow() - user.last_user_feedback_sent_at).total_seconds()) // 60
        text = f"You've already sent us feedback recently. Please, wait another {minutes} minutes(s) " \
               f"before submitting a new one"
        keyboard_markup = None
        to_return = ConversationHandler.END
    else:
        text = "Feedback is always appreciated! Feel free to share your feedback by submitting a message right now"
        keyboard_markup = InlineKeyboardMarkup([[
            InlineKeyboardButton(f"{CANCEL} Cancel", callback_data="feedback_cancel")
        ]])
        to_return = "feedback_main"

    if update.message:
        if user.tg_profile_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_profile_message_id)
            except BadRequest:
                pass
        if user.tg_settings_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_settings_message_id)
            except BadRequest:
                pass
        if user.tg_subscriptions_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_subscriptions_message_id)
            except BadRequest:
                pass
        if user.tg_feedback_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_feedback_message_id)
            except BadRequest:
                pass
        feedback_msg = update.message.reply_text(text=text, reply_markup=keyboard_markup)
        get_user_dao().update_user_tg_message_ids(user.tg_id, tg_feedback_message_id=feedback_msg.message_id)
    elif update.callback_query:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text,
            reply_markup=keyboard_markup
        )

    return to_return


def feedback_sent(update: Update, _: CallbackContext):
    if update.message:
        update.message.delete()
        tg_id = update.message.from_user.id
        user_dao = get_user_dao()
        user = user_dao.read_user(tg_id)
        if user.tg_feedback_message_id:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_feedback_message_id)
            except BadRequest:
                pass
        feedback = UserFeedback(tg_id, canonize_user_feedback(str(update.message.text)), datetime.utcnow())
        get_user_dao().update_user_last_feedback_sent_time(tg_id, last_user_feedback_sent_at=feedback.sent_at)
        get_user_dao().add_user_feedback(feedback)

        update.message.reply_text(text="Thank you for your feedback! Please, wait for 5 minutes before submitting "
                                       "another one", reply_markup=None)

    raise DispatcherHandlerStop(ConversationHandler.END)


def feedback_cancel(update: Update, _: CallbackContext):
    query = update.callback_query
    query.answer()
    query.edit_message_text(
        'You can always submit your feedback by selecting the /feedback menu item'
    )

    return ConversationHandler.END
