from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.error import BadRequest
from telegram.ext import CallbackContext, ConversationHandler
from traceback import format_exc

from jobsbot.bot.telegram.callbacks.inline_keyboards import jobs_categories_keyboard, jobs_positions_keyboard, \
    settings_keyboard, skills_categories_keyboard, skills_keyboard, user_preferences_keyboard, user_salary_keyboard, \
    user_notifications_keyboard, settings_relocation_location_keyboard

from jobsbot.bot.telegram.common.dao import get_user
from jobsbot.bot.telegram.common.emoji import CLOSE, RETURN, WARNING
from jobsbot.bot.telegram.common.messages import delete_user_dialog_messages
from jobsbot.core.enums import JobCategory, JobPosition, SkillsCategory, UserSkill, UserPreferences, UserCountry, \
    CountryLocations, UserDesiredMinAnnualGrossSalary, UserNotificationOption
from jobsbot.core.enum_maps import COUNTRY_COUNTRY_LOCATIONS_MAP
from jobsbot.core.logging import get_logger

from jobsbot.dao import get_user_dao

from jobsbot.settings import get_env, get_tg_botname

__all__ = [
    "settings_main",
    "settings_cancel",
    "settings_salary",
    "settings_job_categories",
    "settings_job_positions",
    "settings_skills_categories",
    "settings_skills",
    "settings_preferences",
    "settings_refer_a_friend",
    "settings_user_relocation_location",
    "settings_notifications"
]


def settings_main(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    logger.info(f"UPDATE: {update}")
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    if update.message:
        tg_id = update.message.from_user.id
        try:
            update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=update.message.message_id)
        except BadRequest:
            pass
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    user = get_user(tg_id, tg_chat_id)
    keyboard_markup = InlineKeyboardMarkup(settings_keyboard(user))
    logger.info(f"User: {user}")

    if update.message:
        delete_user_dialog_messages(update.message.bot, user)
        settings_msg = update.message.reply_text(text='Update Your Settings', reply_markup=keyboard_markup)
        get_user_dao().update_user_tg_message_ids(user.tg_id, tg_settings_message_id=settings_msg.message_id)
    elif update.callback_query:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            'Update Your Settings',
            reply_markup=keyboard_markup
        )

    return "settings_main"


def settings_cancel(update: Update, _: CallbackContext):
    query = update.callback_query
    query.answer()
    query.edit_message_text(
        'You can always edit your profile details in /profile and change your settings in /settings'
    )

    return ConversationHandler.END


def settings_notifications(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    if query.data.startswith("settings_notifications:"):
        try:
            option = UserNotificationOption.parse(int(query.data.split(":")[1]))
        except (ValueError, IndexError) as e:
            logger.error(f"Exception: {e}")
            logger.error(format_exc())
            query.edit_message_text(
                "We have some troubles on our side :( Try again a bit later"
            )
            return ConversationHandler.END
        if option in user.user_notification_options:
            user.user_notification_options = [o for o in user.user_notification_options if o != option]
        else:
            user.user_notification_options = list(set(user.user_notification_options + [option]))
        get_user_dao().upsert_user(user)

    reply_markup = InlineKeyboardMarkup(user_notifications_keyboard(user))

    warn_message = ""
    if not user.user_notification_options:
        warn_message = f"{WARNING} You have no notifications enabled. You will not receive any notifications on " \
                       f"opened positions. Please, enable some of them. \n\n"

    try:
        query.edit_message_text(
            f"{warn_message}Update your notifications settings...",
            reply_markup=reply_markup
        )
    except BadRequest as e:
        if "Message is not modified" not in str(e):
            # As user can click on the same option a few times, the message may remain the same
            raise

    return "settings_notifications"


def settings_salary(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    salary = None
    try:
        if query.data.startswith("settings_salary:"):
            salary = UserDesiredMinAnnualGrossSalary.parse(int(query.data.split(":")[1]))
    except (ValueError, KeyError, IndexError) as e:
        logger.error(f"Exception: {e}")
        logger.error(format_exc())
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    msg_prefix = ""
    if salary is not None:
        if user.min_annual_salary == salary:
            msg_prefix = f"You min annual gross salary expectations remained the same: {salary.title()}\n"
        else:
            user.min_annual_salary = salary
            msg_prefix = f"You min annual gross salary expectations were updated: {salary.title()}\n"
        get_user_dao().upsert_user(user)
        get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)

    reply_markup = InlineKeyboardMarkup(user_salary_keyboard(user))
    try:
        query.edit_message_text(
            msg_prefix + "Choose the minimum annual gross salary expectations...",
            reply_markup=reply_markup
        )
    except BadRequest as e:
        if "Message is not modified" not in str(e):
            # As user can click on the same option a few times, the message may remain the same
            raise
    return "settings_salary"


def settings_refer_a_friend(update: Update, _: CallbackContext) -> str:
    query = update.callback_query
    query.answer()

    if update.message:
        tg_id = update.message.from_user.id
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    keyboard = [
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_main"),
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="settings_cancel")
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    referral_link = f"`https://t.me/{get_tg_botname(get_env())}?start=ref_" + user.referral_link_id + "`\n\n"
    referral_link += "Simply click on the link to copy it to your clipboard\\. " \
                     "And then send to your friend\\! Thank you :\\)\n\n" \
                     "Right now we have no bonuses that we can give you for inviting your friends, but we will " \
                     "store information on who invited whom and will recall this information once we introduce a " \
                     "bonus program\\.\n\n" \
                     "Anyway, you help your friends to find a new job \\- isn't that a great bonus itself? :\\)"

    query.edit_message_text(
        f"Here is your personal referral link: {referral_link}",
        parse_mode=ParseMode.MARKDOWN_V2,
        reply_markup=reply_markup
    )
    return "settings_refer_a_friend"


def settings_user_relocation_location(update: Update, _: CallbackContext) -> str:
    if update.message:
        tg_id = update.message.from_user.id
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)
    query = update.callback_query
    query.answer()
    user_dao = get_user_dao()

    country = None
    city = None
    unset_all = False
    all_cities = False
    try:
        if query.data.startswith("settings_relocation:"):
            country_raw = query.data.split(":")[1]
            if country_raw == "none":
                unset_all = True
            else:
                country = UserCountry.parse(int(country_raw))
        if len(query.data.split(":")) == 3:
            city_raw = query.data.split(":")[2]
            if city_raw == "any":
                all_cities = True
            else:
                city = CountryLocations.parse(int(city_raw))
    except (ValueError, KeyError, IndexError) as e:
        logger = get_logger()
        logger.error(f"Exception: {e}")
        logger.error(format_exc())
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    if country is None and not unset_all:
        keyboard = settings_relocation_location_keyboard(user)
    elif unset_all:
        user.user_relocation_locations = dict()
        user_dao.save_user(user)
        user_dao.delete_user_job_unsent_matches(user_tg_id=user.tg_id)
        keyboard = settings_relocation_location_keyboard(user)
    elif all_cities:
        if country in user.user_relocation_locations and not user.user_relocation_locations[country]:
            del user.user_relocation_locations[country]
        else:
            user.user_relocation_locations[country] = []
        user_dao.save_user(user)
        user_dao.delete_user_job_unsent_matches(user_tg_id=user.tg_id)
        keyboard = settings_relocation_location_keyboard(user, country)
    elif city is None:
        keyboard = settings_relocation_location_keyboard(user, country)
    else:
        if country not in user.user_relocation_locations:
            user.user_relocation_locations[country] = [city]
        elif city not in user.user_relocation_locations[country]:
            user.user_relocation_locations[country].append(city)
        else:
            user.user_relocation_locations[country].remove(city)
            if not user.user_relocation_locations[country]:
                del user.user_relocation_locations[country]
        user_dao.save_user(user)
        user_dao.delete_user_job_unsent_matches(user_tg_id=user.tg_id)
        keyboard = settings_relocation_location_keyboard(user, country)

    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        'Select your country...',
        reply_markup=reply_markup
    )

    country_suffix = ""
    if country and country in COUNTRY_COUNTRY_LOCATIONS_MAP:
        country_suffix = f":{country.value}"
    return f"settings_relocation{country_suffix}"


def settings_preferences(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    preference = None
    try:
        if query.data.startswith("settings_preferences:"):
            preference = UserPreferences.parse(int(query.data.split(":")[1]))
    except (ValueError, KeyError, IndexError) as e:
        logger.error(f"Exception: {e}")
        logger.error(format_exc())
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    msg_prefix = ""
    if preference is not None:
        if preference in user.preferences:
            msg_prefix = f"Preference \"{preference.title()}\" was successfully removed " \
                         f"from the list of your desired preferences.\n"
            user.preferences = UserPreferences.remove_item(user.preferences, preference)
        else:
            msg_prefix = f"Preference \"{preference.title()}\" was successfully added " \
                         f"to the list of your desired preferences.\n"
            user.preferences = UserPreferences.add_new_item(user.preferences, preference)
        get_user_dao().upsert_user(user)
        get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)

    keyboard = user_preferences_keyboard(user)
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        msg_prefix + "Choose your preferences",
        reply_markup=reply_markup
    )
    return "settings_preferences"


def settings_job_categories(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    query = update.callback_query
    query.answer()
    logger.info(f"MESSAGE: {query.message}")
    logger.info(f"DATA {query.data}")
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)
    reply_markup = InlineKeyboardMarkup(jobs_categories_keyboard(user))

    update_message_id = False
    if query.message.message_id != user.tg_settings_message_id:
        # GOT HERE FROM A QUICK NOTIFICATION MESSAGE
        delete_user_dialog_messages(query.message.bot, user, keep_message_ids=[query.message.message_id])
        update_message_id = True

    query = update.callback_query
    query.answer()
    query.edit_message_text(
        'Please, choose a category...',
        reply_markup=reply_markup
    )
    if update_message_id:
        get_user_dao().update_user_tg_message_ids(user.tg_id, tg_settings_message_id=query.message.message_id)

    return "settings_job_categories"


def settings_job_positions(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    logger.info(f"UPDATE: {update}")
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    job_position = None
    try:
        if query.data.startswith("settings_job_category"):
            job_category = JobCategory.parse(int(query.data.split(":")[1]))
        elif query.data.startswith("settings_job_position"):
            job_category = JobCategory.parse(int(query.data.split(":")[1]))
            job_position = JobPosition.parse(int(query.data.split(":")[2]))
        else:
            raise ValueError(f"Unknown query data: {query.data}")
    except (ValueError, KeyError, IndexError) as e:
        logger.error(f"Exception: {e}")
        logger.error(format_exc())
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    msg_prefix = ""
    if job_position is not None:
        if job_position in user.desired_positions:
            msg_prefix = f"Position \"{job_category.title()}/{job_position.title()}\" was successfully removed " \
                         f"from the list of positions you consider.\n"
            user.desired_positions = JobPosition.remove_item(user.desired_positions, job_position)
        else:
            msg_prefix = f"Position \"{job_category.title()}/{job_position.title()}\" was successfully added " \
                         f"to the list of positions you consider.\n"
            user.desired_positions = JobPosition.add_new_item(user.desired_positions, job_position)
        get_user_dao().upsert_user(user)
        get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)

    reply_markup = InlineKeyboardMarkup(jobs_positions_keyboard(user, job_category))
    query.edit_message_text(
        msg_prefix + f"Please, choose one ore more desired position within category {job_category.title()}...",
        reply_markup=reply_markup
    )
    return f"settings_job_category_{job_category.value}"


def settings_skills_categories(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    query = update.callback_query
    query.answer()
    logger.info(f"MESSAGE: {query.message}")
    logger.info(f"DATA {query.data}")
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)
    reply_markup = InlineKeyboardMarkup(skills_categories_keyboard(user))
    query.edit_message_text(
        'Please, choose skills...',
        reply_markup=reply_markup
    )
    return "settings_skill_categories"


def settings_skills(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    logger.info(f"UPDATE: {update}")
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id

    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    skill = None
    try:
        if query.data.startswith("settings_skills_category"):
            skill_category = SkillsCategory.parse(int(query.data.split(":")[1]))
        elif query.data.startswith("settings_skill"):
            skill_category = SkillsCategory.parse(int(query.data.split(":")[1]))
            skill = UserSkill.parse(int(query.data.split(":")[2]))
        else:
            raise ValueError(f"Unknown query.data = {query.data}")
    except (ValueError, KeyError, IndexError) as e:
        logger.error(f"Exception: {str(e)}")
        logger.error(format_exc())
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    msg_prefix = ""
    if skill is not None:
        if skill in user.skills:
            msg_prefix = f"Skill \"{skill_category.title()}/{skill.title()}\" was successfully removed " \
                         f"from the skills.\n"
            user.skills = [sk for sk in user.skills if sk != skill]
        else:
            msg_prefix = f"Skill \"{skill_category.title()}/{skill.title()}\" was successfully added " \
                         f"to the skills.\n"
            user.skills = list(set(user.skills + [skill]))
        get_user_dao().delete_user_job_unsent_matches(user_tg_id=user.tg_id)
        get_user_dao().upsert_user(user)

    reply_markup = InlineKeyboardMarkup(skills_keyboard(user, skill_category))
    query.edit_message_text(
        msg_prefix + f"Please, choose one ore more skills within category {skill_category.title()}...",
        reply_markup=reply_markup
    )
    return f"settings_skills_category_{skill_category.value}"
