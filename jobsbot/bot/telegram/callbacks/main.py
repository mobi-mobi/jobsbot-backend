from telegram import Update, ParseMode
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from jobsbot.bot.telegram.common.messages import em
from jobsbot.dao import get_user_dao
from jobsbot.settings import get_tg_botname, get_env


def refer_a_friend(update: Update, _: CallbackContext):
    tg_id = update.message.from_user.id
    try:
        update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=update.message.message_id)
    except BadRequest:
        pass

    user = get_user_dao().read_user(tg_id)

    update_message_ids = False
    for msg_id in (
        user.tg_profile_message_id,
        user.tg_settings_message_id,
        user.tg_feedback_message_id,
        user.tg_subscriptions_message_id
    ):
        if msg_id is not None:
            try:
                update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=msg_id)
                update_message_ids = True
            except BadRequest:
                pass

    if update_message_ids:
        get_user_dao().update_user_tg_message_ids(user.tg_id)

    referral_link = f"`https://t.me/{get_tg_botname(get_env())}?start=ref_" + user.referral_link_id + "`\n\n"
    referral_link += "Simply click on the link to copy it to your clipboard\\. " \
                     "And then send to your friend\\! Thank you :\\)\n\n" \
                     "Right now we have no bonuses that we can give you for inviting your friends, but we will " \
                     "store information on who invited whom and will recall this information once we introduce a " \
                     "bonus program\\.\n\n" \
                     "Anyway, you help your friends to find a new job \\- isn't that a great bonus itself? :\\)"

    update.message.bot.send_message(
        chat_id=update.message.chat_id,
        text=referral_link,
        disable_web_page_preview=True,
        parse_mode=ParseMode.MARKDOWN_V2
    )


def help_message(update: Update, _: CallbackContext):
    tg_id = update.message.from_user.id
    try:
        update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=update.message.message_id)
    except BadRequest:
        pass

    user = get_user_dao().read_user(tg_id)

    update_message_ids = False
    for msg_id in (
        user.tg_profile_message_id,
        user.tg_settings_message_id,
        user.tg_feedback_message_id,
        user.tg_subscriptions_message_id
    ):
        if msg_id is not None:
            try:
                update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=msg_id)
                update_message_ids = True
            except BadRequest:
                pass

    if update_message_ids:
        get_user_dao().update_user_tg_message_ids(user.tg_id)

    text = "Hey! Let us help you!\n\n"
    text += "First of all, you need to understand what JobBot.io is. JobBot.io is a telegram bot that can help you " \
            "find a new job or keep you updated on what's going on in the jobs market. So try it even if you are not " \
            "actively looking for a job!\n\n"
    text += "Start by filling out your /profile - it only takes a few minutes! This will allow you to apply for " \
            "a job with just one click! In addition, most jobs are not available to people without a full profile.\n\n"
    text += "After that check /settings and configure filters there. This will help you receive job alerts that " \
            "match your experience and your desires!\n\n"
    text += "And of course, recommend us to your friends: /refer And feel free to leave /feedback - it will " \
            "be appreciated!\n\n"
    text += "Enjoy JobsBot.io :)"

    update.message.bot.send_message(
        chat_id=update.message.chat_id,
        text=em(text),
        disable_web_page_preview=True,
        parse_mode=ParseMode.MARKDOWN_V2
    )


def close_quick_dialog(update: Update, _: CallbackContext):
    update.callback_query.delete_message()


def unknown_command_message(update: Update, _: CallbackContext):
    text = "We didn't understand you :(\n\n"
    text += "Please, check the list of supported commands:\n\n"
    text += "/help - Help\n"
    text += "/profile - Manage your profile\n"
    text += "/settings - Manage your settings & filters\n"
    text += "/subscriptions - Manage the list of companies you follow\n"
    text += "/refer - Get a personal referral link and share it with your friends!\n"
    text += "/feedback - Share your feedback with us\n\n"
    text += "Thank you :)"

    update.message.bot.send_message(
        chat_id=update.message.chat_id,
        text=em(text),
        disable_web_page_preview=True,
        parse_mode=ParseMode.MARKDOWN_V2
    )

