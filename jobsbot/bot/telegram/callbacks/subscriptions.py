from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.error import BadRequest
from telegram.ext import CallbackContext, ConversationHandler

from jobsbot.bot.telegram.common.dao import get_user
from jobsbot.bot.telegram.common.emoji import BELL, BELL_CROSSED, CLOSE, RETURN, SUBSCRIBE, UNSUBSCRIBE
from jobsbot.core.logging import get_logger

from jobsbot.dao import get_user_dao


def subscriptions_main(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    logger.info(f"UPDATE: {update}")
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    if update.message:
        tg_id = update.message.from_user.id
        try:
            update.message.bot.delete_message(chat_id=update.message.chat_id, message_id=update.message.message_id)
        except BadRequest:
            pass
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    user = get_user(tg_id, tg_chat_id)

    logger.info(f"User: {user}")
    if not user.followed_companies_ids:
        text = "You don't follow any companies. Search for our button on companies " \
               "vacancies pages and click to subscribe"
        keyboard_markup = None
    else:
        text = "Companies you follow:"
        companies = get_user_dao().read_companies(user.followed_companies_ids)
        disabled_companies = set(user.followed_disabled_companies_ids)
        companies.sort(key=lambda c: c.company_name)
        keyboard_markup = InlineKeyboardMarkup([
            [
                InlineKeyboardButton(
                    text=company.company_name + (f" {BELL_CROSSED}"
                                                 if company.company_id in disabled_companies else ""),
                    callback_data=f"subscriptions:{company.company_id}"
                )
            ]
            for company in companies
        ] + [[
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="subscriptions_close")
        ]])

    if update.message:
        if user.tg_settings_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_settings_message_id)
            except BadRequest:
                pass
        if user.tg_profile_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_profile_message_id)
            except BadRequest:
                pass
        if user.tg_subscriptions_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_subscriptions_message_id)
            except BadRequest:
                pass
        if user.tg_feedback_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_feedback_message_id)
            except BadRequest:
                pass
        subscriptions_msg = update.message.reply_text(text=text, reply_markup=keyboard_markup)
        get_user_dao().update_user_tg_message_ids(user.tg_id, tg_subscriptions_message_id=subscriptions_msg.message_id)

    elif update.callback_query:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text=text,
            reply_markup=keyboard_markup
        )

    return "subscriptions_main"


def subscriptions_manage_subscription(update: Update, _: CallbackContext) -> str:
    logger = get_logger()
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    prefix = "subscriptions:"
    prefix_enable = prefix + "enable:"
    prefix_disable = prefix + "disable:"
    prefix_subscribe = prefix + "subscribe:"
    prefix_unsubscribe = prefix + "unsubscribe:"
    company = None
    should_enable = False
    should_disable = False
    should_subscribe = False
    should_unsubscribe = False
    if query.data.startswith(prefix):
        if query.data.startswith(prefix_enable):
            prefix = prefix_enable
            should_enable = True
        if query.data.startswith(prefix_disable):
            prefix = prefix_disable
            should_disable = True
        if query.data.startswith(prefix_subscribe):
            prefix = prefix_subscribe
            should_subscribe = True
        if query.data.startswith(prefix_unsubscribe):
            prefix = prefix_unsubscribe
            should_unsubscribe = True
        company_id = query.data[len(prefix):]
        company = get_user_dao().read_company(company_id)

    if not company:
        logger.error(f"Can not find company with id {query.data[len(prefix):]}. query.data = {query.data}")
        query.edit_message_text(
            "We have some troubles on our side :( Try again a bit later"
        )
        return ConversationHandler.END

    if should_enable:
        try:
            user.followed_disabled_companies_ids.remove(company.company_id)
        except ValueError:
            pass
    if should_disable:
        user.followed_disabled_companies_ids = list(set(user.followed_disabled_companies_ids + [company.company_id]))
    if should_subscribe:
        user.followed_companies_ids = list(set(user.followed_companies_ids + [company.company_id]))
        try:
            user.followed_disabled_companies_ids.remove(company.company_id)
        except ValueError:
            pass
    if should_unsubscribe:
        try:
            user.followed_companies_ids.remove(company.company_id)
        except ValueError:
            pass
        try:
            user.followed_disabled_companies_ids.remove(company.company_id)
        except ValueError:
            pass
    if should_enable or should_disable or should_subscribe or should_unsubscribe:
        get_user_dao().update_user_following_companies(
            user.tg_id,
            user.followed_companies_ids,
            user.followed_disabled_companies_ids
        )

    en_dis_text = f"{BELL_CROSSED} Disable Notifications"
    en_dis_callback_data = f"subscriptions:disable:{company.company_id}"
    if company.company_id in user.followed_disabled_companies_ids:
        en_dis_text = f"{BELL} Enable Notifications"
        en_dis_callback_data = f"subscriptions:enable:{company.company_id}"

    show_notifications_button = False
    sub_unsub_text = f"{SUBSCRIBE} Subscribe"
    sub_unsub_callback_data = f"subscriptions:subscribe:{company.company_id}"
    if company.company_id in user.followed_companies_ids:
        show_notifications_button = True
        sub_unsub_text = f"{UNSUBSCRIBE} Unsubscribe"
        sub_unsub_callback_data = f"subscriptions:unsubscribe:{company.company_id}"

    keyboard = [
        [
            InlineKeyboardButton(text=sub_unsub_text, callback_data=sub_unsub_callback_data),
        ],
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="subscriptions_main"),
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="subscriptions_close")
        ]
    ]

    if show_notifications_button:
        keyboard = [[InlineKeyboardButton(text=en_dis_text, callback_data=en_dis_callback_data)]] + keyboard

    try:
        query.edit_message_text(
            f"Manage your subscription for company {company.company_name}",
            reply_markup=InlineKeyboardMarkup(keyboard)
        )
    except BadRequest as e:
        if "Message is not modified" not in str(e):
            # As user can click on the same option a few times, the message may remain the same
            raise

    return "subscriptions_manage_subscription"


def subscriptions_cancel(update: Update, _: CallbackContext):
    query = update.callback_query
    query.answer()
    query.edit_message_text(
        'You can always manage your subscription in /subscriptions'
    )

    return ConversationHandler.END
