from datetime import datetime, timedelta
from telegram import Update
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from jobsbot.bot.telegram.common.dao import get_user
from jobsbot.dao import get_user_dao

MAX_COMPANIES_TO_FOLLOW = 10


def start_command(update: Update, _: CallbackContext) -> None:

    """Send a message when the command /help is issued."""
    if update.message:
        tg_id = update.message.from_user.id
    elif update.callback_query:
        tg_id = update.callback_query.from_user.id
    else:
        raise ValueError(f"Can not process update: {update}")

    company_referral_id = None
    referral_id = None
    user = get_user(tg_id)

    reply_text = "Hello!"

    if update.message is not None and update.message.text:
        text_split = update.message.text.split(" ")
        if len(text_split) > 1:
            command_text = text_split[1].strip()

            if command_text.startswith("follow_"):
                company_referral_id_raw = command_text[7:].strip()
                if company_referral_id_raw:
                    company_referral_id = company_referral_id_raw

            if command_text.startswith("ref_"):
                referral_id_raw = command_text[4:].strip()
                if referral_id_raw:
                    referral_id = referral_id_raw

    if update.message:
        if user.tg_settings_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_settings_message_id)
            except BadRequest:
                pass
        if user.tg_profile_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_profile_message_id)
            except BadRequest:
                pass
        if user.tg_subscriptions_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_subscriptions_message_id)
            except BadRequest:
                pass
        if user.tg_feedback_message_id is not None:
            try:
                update.message.bot.delete_message(user.tg_id, user.tg_feedback_message_id)
            except BadRequest:
                pass
        get_user_dao().update_user_tg_message_ids(user.tg_id)

    if company_referral_id:
        referral_company = get_user_dao().read_company_by_referral_link_id(company_referral_id)
        if referral_company is None:
            reply_text += "\n\nUnfortunately, the company's referral link seems to be invalid, but the good "\
                          "news is that you still can use this bot to find a job of your dreams!"
        else:
            if referral_company.company_id in user.followed_companies_ids:
                reply_text += f"\n\nYou are already following company {referral_company.company_name} :\\)"
            elif len(user.followed_companies_ids) >= MAX_COMPANIES_TO_FOLLOW:
                reply_text += f"\n\nUnfortunately, you already follow maximum allowed number of companies: "\
                              f"{MAX_COMPANIES_TO_FOLLOW}. Try to unfollow some of them in /settings menu "\
                              f"and use the same referral link again"
            else:
                user.followed_companies_ids = list(set(user.followed_companies_ids)
                                                   .union([referral_company.company_id]))
                get_user_dao().upsert_user(user)
                reply_text += f"\n\nYou successfully started following company: "\
                              f"{referral_company.company_name}! You can manage a list of companies you "\
                              f"follow in /settings"

    elif referral_id:
        referral_user = get_user_dao().read_user_by_referral_link_id(referral_id)
        if referral_user is not None:
            if referral_user.tg_id is not None and user.created_at > datetime.utcnow() - timedelta(minutes=15):
                reply_text += f"\n\n(And thanks to {referral_user.name} for recommending us to you!)"
                user.referred_by_tg_id = referral_user.tg_id
                get_user_dao().upsert_user(user)

    reply_text += "\n\nWhat you can do:"
    reply_text += "\nGo to /settings and update your preferences - this is really important! Once you update your " \
                  "settings, you will only receive relevant job opportunities"
    reply_text += "\nGo to /profile and update your profile - without this information we won't be able to recommend " \
                  "you any jobs"

    update.message.reply_photo(open("img/bot/hello.png", "rb"), reply_text)
