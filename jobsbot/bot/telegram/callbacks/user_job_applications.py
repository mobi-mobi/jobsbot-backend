from datetime import datetime
from telegram import Bot, Update
from telegram import ParseMode as TelegramParseMode
from telegram.ext import CallbackContext
from telegram.utils.helpers import escape_markdown

from jobsbot.bot.telegram.common.dao import get_user
from jobsbot.bot.telegram.common.messages import single_job_found_message_text, single_job_found_message_keyboard_markup
from jobsbot.core.enums import UserJobApplicationStatus, UserJobMatchType, UserJobApplicationMethod
from jobsbot.core.logging import get_logger
from jobsbot.core.models import UserJobApplication
from jobsbot.dao import get_user_dao
from jobsbot.settings import get_tg_token, get_env


def em(text: str):
    return escape_markdown(text, version=2)


def apply_for_job(update: Update, _: CallbackContext):
    logger = get_logger()
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    tg_token = get_tg_token(get_env())
    tg_bot = Bot(tg_token)

    prefix_one_click = "apply_for_job:"
    prefix_on_website = "apply_for_job_on_website:"
    if query.data.startswith(prefix_one_click):
        prefix = prefix_one_click
        application_method = UserJobApplicationMethod.ONE_CLICK
    elif query.data.startswith(prefix_on_website):
        prefix = prefix_on_website
        application_method = UserJobApplicationMethod.URL_REDIRECT
    else:
        return

    job_id = query.data[len(prefix):]
    if not job_id:
        return

    job = get_user_dao().read_job(job_id)
    if job is None:
        return

    company = get_user_dao().read_company(job.company_id)
    if company is None:
        return

    all_applications = list(get_user_dao().read_user_job_applications(tg_id, job_id, no_older_than=None))
    if all_applications:
        if len(all_applications) > 1:
            logger.error(f"Too many job applications found for user {tg_id} and job {job.job_id}")
        application = all_applications[0]
        if application.user_job_application_status == UserJobApplicationStatus.CANCELLED_BY_USER \
                and application.times_applied <= 1 and application_method == UserJobApplicationMethod.ONE_CLICK:
            application.last_time_applied_at = datetime.utcnow()
            application.user_job_application_status = UserJobApplicationStatus.NOT_REVIEWED
            application.times_applied += 1
            get_user_dao().create_user_job_application(application, replace_if_exists=True)
    else:
        get_user_dao().create_user_job_application(UserJobApplication(
            user_tg_id=tg_id,
            job_id=job.job_id,
            created_at=datetime.utcnow(),
            last_time_applied_at=datetime.utcnow(),
            times_applied=1,
            user_job_application_status=UserJobApplicationStatus.NOT_REVIEWED,
            user_job_application_method=application_method
        ), replace_if_exists=False)

    query = update.callback_query
    query.answer()

    text = single_job_found_message_text(job, company, user, is_applied=True, is_request_cancelled=False,
                                         is_too_many_requests=False)

    keyboard_markup = None
    if application_method == UserJobApplicationMethod.ONE_CLICK:
        keyboard_markup = single_job_found_message_keyboard_markup(job, show_cancel_button=True)
    elif application_method == UserJobApplicationMethod.URL_REDIRECT and job.job_url:
        job_matches = list(get_user_dao().read_user_job_matches(user_tg_id=user.tg_id, job_id=job.job_id))
        url = job.job_url
        if job_matches:
            url = f"http://go.jobsbot.io/a/{job_matches[0].match_hash}"
        keyboard_markup = single_job_found_message_keyboard_markup(
            job, show_apply_on_site_button=True, apply_on_website_url=url,
            apply_on_website_text=job.apply_on_text
        )

    query.edit_message_caption(
        caption=text,
        parse_mode=TelegramParseMode.MARKDOWN_V2,
        reply_markup=keyboard_markup
    )

    msg = em(f"You successfully applied for a job {job.title} @ {company.company_name}")
    tg_bot.send_message(
        chat_id=user.tg_chat_id,
        text=msg,
        parse_mode=TelegramParseMode.MARKDOWN_V2,
    )
    if application_method == UserJobApplicationMethod.ONE_CLICK:
        logger.info(f"User {user.tg_id} ({user.name}) applied for a job {job.job_id} ({job.title} @ " +
                    f"{company.company_name}) in one click")
    elif application_method == UserJobApplicationMethod.URL_REDIRECT:
        logger.info(f"User {user.tg_id} ({user.name}) applied for a job {job.job_id} ({job.title} @ " +
                    f"{company.company_name}) by going to a website")


def cancel_job_application(update: Update, _: CallbackContext):
    logger = get_logger()
    query = update.callback_query
    query.answer()
    tg_id = update.callback_query.from_user.id
    tg_chat_id = update.effective_chat
    if tg_chat_id is not None:
        tg_chat_id = tg_chat_id.id
    user = get_user(tg_id, tg_chat_id=tg_chat_id)

    prefix = "cancel_job_application:"
    if not query.data.startswith(prefix):
        return

    job_id = query.data[len(prefix):]
    if not job_id:
        return

    job = get_user_dao().read_job(job_id)
    if job is None:
        return

    company = get_user_dao().read_company(job.company_id)

    all_applications = list(get_user_dao().read_user_job_applications(
        user_tg_id=tg_id,
        job_id=job.job_id,
        no_older_than=None
    ))

    too_many_requests = False
    if len(all_applications) > 1:
        logger.error(f"Too many job applications found for user {tg_id} and job {job.job_id}")
        too_many_requests = True
    elif all_applications:
        application = all_applications[0]
        too_many_requests = application.times_applied >= 2
        if application.user_job_application_status in [UserJobApplicationStatus.NOT_REVIEWED,
                                                       UserJobApplicationStatus.ACCEPTED]:
            application.user_job_application_status = UserJobApplicationStatus.CANCELLED_BY_USER
            get_user_dao().create_user_job_application(application, replace_if_exists=True)

    query = update.callback_query
    query.answer()

    job_matches = list(get_user_dao().read_user_job_matches(user_tg_id=user.tg_id, job_id=job.job_id))

    show_apply_button = True
    show_url = False

    if not job_matches or job_matches[0].match_type != UserJobMatchType.FULL_MATCH:
        show_apply_button = False
    if too_many_requests:
        show_apply_button = False
    if not show_apply_button and job.job_url:
        show_url = True

    text = single_job_found_message_text(job, company, user, is_applied=False, is_request_cancelled=True,
                                         is_too_many_requests=too_many_requests)
    keyboard_markup = None
    if show_apply_button:
        keyboard_markup = single_job_found_message_keyboard_markup(job, show_apply_button=show_apply_button)
    if show_url:
        url = job.job_url
        if job_matches:
            url = f"http://go.jobsbot.io/a/{job_matches[0].match_hash}"
        keyboard_markup = single_job_found_message_keyboard_markup(
            job, show_apply_on_site_button=True, apply_on_website_url=url,
            apply_on_website_text=job.apply_on_text
        )

    query.edit_message_caption(
        caption=text,
        parse_mode=TelegramParseMode.MARKDOWN_V2,
        reply_markup=keyboard_markup
    )

    logger.info(f"User {user.name} (tg_id={user.tg_id}) cancelled job request for  "
                f"\"{job.title}\" (job_id={job.job_id})")
