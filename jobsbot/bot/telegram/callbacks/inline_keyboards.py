from typing import List, Optional

from jobsbot.core.enums import JobCategory, SkillsCategory, UserPreferences, UserCountry, \
    UserDesiredMinAnnualGrossSalary, UserNotificationOption, UserExperience
from jobsbot.core.enum_maps import JOBS_TREE, SKILLS_TREE, COUNTRY_COUNTRY_LOCATIONS_MAP
from jobsbot.core.models import User
from jobsbot.bot.telegram.common.emoji import CANCEL, CLOSE, CHECKBOX, MONEY, POSITIONS, RETURN, BELL, \
    WAVING_HAND, PREFERENCES, RIGHT, RELOCATION

from telegram import InlineKeyboardButton


__all__ = [
    "settings_keyboard",
    "jobs_categories_keyboard",
    "jobs_positions_keyboard",
    "skills_categories_keyboard",
    "skills_keyboard",
    "user_preferences_keyboard",
    "user_salary_keyboard",
    "user_notifications_keyboard",
    "profile_countries_keyboard",
    "settings_relocation_location_keyboard",
    "profile_experience_keyboard"
]


def settings_keyboard(user: User) -> List[List[InlineKeyboardButton]]:
    ready_for_relocation = "No"
    if user.user_relocation_locations:
        ready_for_relocation = f"Yes ({len(user.user_relocation_locations)})"

    return [
        [
            InlineKeyboardButton(f"{BELL} Enabled Notifications ({len(user.user_notification_options)})",
                                 callback_data="settings_notifications")
        ],
        [
            InlineKeyboardButton(f"{MONEY} Salary Expectations ({user.min_annual_salary.title()})",
                                 callback_data="settings_salary")
        ],
        [
            InlineKeyboardButton(f"{POSITIONS} Considered Positions ({len(user.desired_positions)})",
                                 callback_data="settings_job_categories")
        ],
        [
            InlineKeyboardButton(f"{PREFERENCES} Your Preferences ({len(user.preferences)})",
                                 callback_data="settings_preferences")
        ],
        [
            InlineKeyboardButton(f"{POSITIONS} Your Skills ({len(user.skills)})",
                                 callback_data="settings_skill_categories")
        ],
        [
            InlineKeyboardButton(f"{RELOCATION} Ready for relocation ({ready_for_relocation})",
                                 callback_data="settings_relocation")
        ],
        [
            InlineKeyboardButton(f"{WAVING_HAND} Refer a friend!", callback_data="settings_refer_a_friend")
        ],
        [
            InlineKeyboardButton(f"{CLOSE} Close", callback_data="settings_cancel"),
        ]
    ]


def jobs_categories_keyboard(user: User) -> List[List[InlineKeyboardButton]]:
    items = []
    positions_set = set(user.desired_positions)
    for category in JOBS_TREE.keys():
        if not JOBS_TREE[category]:
            continue

        title = category.title()
        positions_selected_in_category = 0
        for position in JOBS_TREE[category]:
            if position in positions_set:
                positions_selected_in_category += 1
        if positions_selected_in_category > 0:
            title = f"{CHECKBOX} ({positions_selected_in_category}) " + title
        items.append([
            InlineKeyboardButton(title, callback_data=f"settings_job_category:{category.value}")
        ])

    items.extend([[
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_main"),
        InlineKeyboardButton(f"{CANCEL} Close", callback_data="settings_cancel")
    ]])
    return items


def jobs_positions_keyboard(user: User, current_category: JobCategory) -> List[List[InlineKeyboardButton]]:
    items = [
        [
            InlineKeyboardButton((f"{CHECKBOX}️ " if position in user.desired_positions else "") + position.title(),
                                 callback_data=f"settings_job_position:{current_category.value}:{position.value}")
        ] for position in JOBS_TREE[current_category]
    ]
    items.extend([[
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_job_categories"),
        InlineKeyboardButton(f"{CANCEL} Close", callback_data="settings_cancel")
    ]])
    return items


def skills_categories_keyboard(user: User) -> List[List[InlineKeyboardButton]]:
    items = []
    skills_set = set(user.skills)
    for category in SKILLS_TREE.keys():
        if not SKILLS_TREE[category]:
            continue

        title = category.title()
        skills_selected_in_category = 0
        for skill in SKILLS_TREE[category]:
            if skill in skills_set:
                skills_selected_in_category += 1
        if skills_selected_in_category > 0:
            title = f"{CHECKBOX} ({skills_selected_in_category}) " + title
        items.append([
            InlineKeyboardButton(title, callback_data=f"settings_skills_category:{category.value}")
        ])

    items.extend([[
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_main"),
        InlineKeyboardButton(f"{CANCEL} Close", callback_data="settings_cancel")
    ]])
    return items


def skills_keyboard(user: User, current_category: SkillsCategory) -> List[List[InlineKeyboardButton]]:
    items = [
        [
            InlineKeyboardButton((f"{CHECKBOX}️ " if skill in user.skills else "") + skill.title(),
                                 callback_data=f"settings_skill:{current_category.value}:{skill.value}")
        ] for skill in SKILLS_TREE[current_category]
    ]
    items.extend([[
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_skill_categories"),
        InlineKeyboardButton(f"{CANCEL} Close", callback_data="settings_cancel")
    ]])
    return items


def user_preferences_keyboard(user: User) -> List[List[InlineKeyboardButton]]:
    return [
        [
            InlineKeyboardButton((f"{CHECKBOX}️ " if preference in user.preferences else "") + preference.title(),
                                 callback_data=f"settings_preferences:{preference.value}")
        ] for preference in UserPreferences
    ] + [
        [
            InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_main"),
            InlineKeyboardButton(f"{CANCEL} Close", callback_data="settings_cancel")
        ]
    ]


def user_salary_keyboard(user: User) -> List[List[InlineKeyboardButton]]:
    layout = [
        [
            UserDesiredMinAnnualGrossSalary.NO_PREFERENCES
        ],
        [
            UserDesiredMinAnnualGrossSalary.FROM_30,
            UserDesiredMinAnnualGrossSalary.FROM_40,
        ],
        [
            UserDesiredMinAnnualGrossSalary.FROM_50,
            UserDesiredMinAnnualGrossSalary.FROM_60,
        ],
        [
            UserDesiredMinAnnualGrossSalary.FROM_70,
            UserDesiredMinAnnualGrossSalary.FROM_80,
        ],
        [
            UserDesiredMinAnnualGrossSalary.FROM_90,
            UserDesiredMinAnnualGrossSalary.FROM_100,
        ],
        [
            UserDesiredMinAnnualGrossSalary.FROM_110,
            UserDesiredMinAnnualGrossSalary.FROM_130,
        ],
    ]

    return [[
        InlineKeyboardButton((f"{CHECKBOX}️ " if user.min_annual_salary == desired_salary else "")
                             + desired_salary.title(),
                             callback_data=f"settings_salary:{desired_salary.value}")
        for desired_salary in layout_line
    ] for layout_line in layout] + [[
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_main"),
        InlineKeyboardButton(f"{CANCEL} Close", callback_data="settings_cancel")
    ]]


def profile_countries_keyboard(user: User, country: Optional[UserCountry] = None) \
        -> List[List[InlineKeyboardButton]]:
    keyboard = list()

    if country is None:
        for country_option in UserCountry:
            checkbox_prefix = ""
            right_suffix = ""
            if user.user_country == country_option:
                checkbox_prefix = f"{CHECKBOX} "
            if country_option in COUNTRY_COUNTRY_LOCATIONS_MAP:
                right_suffix = f" {RIGHT}"
            keyboard.append([InlineKeyboardButton(f"{checkbox_prefix}{country_option.title()}{right_suffix}",
                                                  callback_data=f"profile:location:{country_option.value}")])
    else:
        for city in COUNTRY_COUNTRY_LOCATIONS_MAP.get(country, []):
            checkbox_prefix = ""
            if user.user_country_location == city:
                checkbox_prefix = f"{CHECKBOX} "
            keyboard.append([InlineKeyboardButton(f"{checkbox_prefix}{city.title()}",
                                                  callback_data=f"profile:location:{country.value}:{city.value}")])
        checkbox_prefix = ""
        if user.user_country_location is None and user.user_country == country:
            checkbox_prefix = f"{CHECKBOX} "
        keyboard.append([InlineKeyboardButton(f"{checkbox_prefix}Another city in {country.title()}",
                                              callback_data=f"profile:location:{country.value}:none")])

    return_callback_data = "profile"
    if country is not None:
        return_callback_data = "profile:location"
    keyboard.append([
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data=return_callback_data),
        InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel")
    ])
    return keyboard


def settings_relocation_location_keyboard(user: User, country: Optional[UserCountry] = None) \
        -> List[List[InlineKeyboardButton]]:
    keyboard = list()

    if country is None:
        checkbox_prefix = f"{CHECKBOX} " if not user.user_relocation_locations else ""
        keyboard.append([InlineKeyboardButton(f"{checkbox_prefix}Not ready for relocation",
                                              callback_data=f"settings_relocation:none")])
        for country_option in UserCountry:
            if country_option not in COUNTRY_COUNTRY_LOCATIONS_MAP:
                continue
            checkbox_prefix = ""
            right_suffix = ""
            if country_option in user.user_relocation_locations:
                checkbox_prefix = f"{CHECKBOX} "
            if country_option in COUNTRY_COUNTRY_LOCATIONS_MAP:
                right_suffix = f" {RIGHT}"
            keyboard.append([InlineKeyboardButton(f"{checkbox_prefix}{country_option.title()}{right_suffix}",
                                                  callback_data=f"settings_relocation:{country_option.value}")])
    else:
        checkbox_prefix = f"{CHECKBOX} " if user.user_relocation_locations.get(country) == [] else ""
        keyboard.append([InlineKeyboardButton(f"{checkbox_prefix}Any city in {country.title()}",
                                              callback_data=f"settings_relocation:{country.value}:any")])
        for city in COUNTRY_COUNTRY_LOCATIONS_MAP.get(country, []):
            checkbox_prefix = ""
            if city in user.user_relocation_locations.get(country, []):
                checkbox_prefix = f"{CHECKBOX} "
            keyboard.append([InlineKeyboardButton(f"{checkbox_prefix}{city.title()}",
                                                  callback_data=f"settings_relocation:{country.value}:{city.value}")])

    return_callback_data = "settings_main"
    if country is not None:
        return_callback_data = "settings_relocation"
    keyboard.append([
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data=return_callback_data),
        InlineKeyboardButton(f"{CLOSE} Close", callback_data="settings_cancel")
    ])
    return keyboard


def profile_experience_keyboard(user: User) -> List[List[InlineKeyboardButton]]:
    keyboard = list()

    experience_options = [e for e in UserExperience]
    while experience_options:
        keyboard_line = []
        line_experience_options, experience_options = experience_options[0:2], experience_options[2:]
        for experience in line_experience_options:
            checkbox_prefix = ""
            if user.experience == experience:
                checkbox_prefix = f"{CHECKBOX} "
            keyboard_line.append(InlineKeyboardButton(f"{checkbox_prefix}{experience.title()}",
                                                      callback_data=f"profile:experience:{experience.value}"))
        keyboard.append(keyboard_line)

    keyboard.append([
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data="profile"),
        InlineKeyboardButton(f"{CLOSE} Close", callback_data="profile_cancel")
    ])
    return keyboard


def user_notifications_keyboard(user: User):
    keyboard = []
    for option in (UserNotificationOption.MATCHED_JOBS_DAILY,
                   UserNotificationOption.MATCHED_JOBS_WEEKLY,
                   UserNotificationOption.FOLLOWED_COMPANIES_JOBS_DAILY,
                   UserNotificationOption.FOLLOWED_COMPANIES_JOBS_WEEKLY,
                   UserNotificationOption.BOT_FRIENDLY_REMINDERS,
                   UserNotificationOption.BOT_NEWS):
        checkbox_prefix = ""
        if option in user.user_notification_options:
            checkbox_prefix = f"{CHECKBOX} "
        keyboard.append([
            InlineKeyboardButton(f"{checkbox_prefix}{option.title()}",
                                 callback_data=f"settings_notifications:{option.value}")
        ])

    keyboard.append([
        InlineKeyboardButton(f"{RETURN}️ Return", callback_data="settings_main"),
        InlineKeyboardButton(f"{CLOSE} Close", callback_data="settings_cancel")
    ])
    return keyboard
