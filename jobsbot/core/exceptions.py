class JobsBotException(Exception):
    pass


class JobsBotDecodeException(JobsBotException):
    pass


class JobsBotSchemaValidationException(JobsBotException):
    pass
