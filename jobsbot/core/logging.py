import logging

__all__ = [
    "get_logger"
]

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)


def get_logger(name="tg_bot"):
    return logging.getLogger(name)
