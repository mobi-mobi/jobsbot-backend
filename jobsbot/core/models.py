from datetime import datetime, timedelta
from hashlib import sha256
from random import choices
from string import ascii_letters, digits
from typing import Any, Dict, List, Optional, Tuple

from .enums import AdminPermission, UserCountry, CountryLocations, JobPosition, UserJobApplicationStatus, \
    UserSkill, UserPreferences, UserDesiredMinAnnualGrossSalary, UserNotificationOption, \
    UserProfileValidationStatus, UserProfileRejectionReason, UserProfileRequiredPart, RecruiterPermission, \
    UserExperience, JobPostingStatus, JobPostingValidationStatus, UserJobApplicationMethod, UserJobMatchType, \
    JobCategory, UserJobMatchSource
from .interfaces import JobsBotModel


class User(JobsBotModel):
    def __init__(self, tg_id: int, tg_chat_id: Optional[int], tg_settings_message_id: Optional[int],
                 tg_profile_message_id: Optional[int], tg_subscriptions_message_id: Optional[int],
                 tg_notification_message_id: Optional[int], tg_feedback_message_id: Optional[int],
                 referred_by_tg_id: Optional[str],
                 created_at: datetime, updated_at: datetime, cv_upload_datetimes: List[datetime],
                 name: Optional[str], email: Optional[str],
                 current_position_title: Optional[str], current_company_name: Optional[str],
                 min_annual_salary: Optional[UserDesiredMinAnnualGrossSalary],
                 desired_positions: Optional[List[JobPosition]],
                 preferences: Optional[List[UserPreferences]], skills: Optional[List[UserSkill]],
                 experience: Optional[UserExperience], referral_link_id: Optional[str],
                 followed_companies_ids: List[str], followed_disabled_companies_ids: List[str],
                 user_country: Optional[UserCountry], user_country_location: Optional[CountryLocations],
                 user_relocation_locations: Dict[UserCountry, List[CountryLocations]],
                 user_notification_options: Optional[List[UserNotificationOption]],
                 missing_profile_parts: List[UserProfileRequiredPart],
                 changed_after_last_validation_profile_parts: List[UserProfileRequiredPart],
                 validation_status: Optional[UserProfileValidationStatus],
                 validation_status_update_sent: Optional[bool], validation_status_updated_at: Optional[datetime],
                 validation_rejection_reasons: Optional[List[UserProfileRejectionReason]],
                 last_interaction_time: Optional[datetime],
                 last_profile_not_ready_notification_time: Optional[datetime],
                 profile_not_ready_notifications_sent: int,
                 last_daily_jobs_sent_at: Optional[datetime],
                 last_weekly_jobs_sent_at: Optional[datetime],
                 last_user_feedback_sent_at: Optional[datetime]
                 ):
        self.tg_id = tg_id
        self.tg_chat_id = tg_chat_id
        self.tg_settings_message_id = tg_settings_message_id
        self.tg_profile_message_id = tg_profile_message_id
        self.tg_subscriptions_message_id = tg_subscriptions_message_id
        self.tg_notification_message_id = tg_notification_message_id
        self.tg_feedback_message_id = tg_feedback_message_id
        self.referred_by_tg_id = referred_by_tg_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.cv_upload_datetimes = cv_upload_datetimes
        self.name = name
        self.email = email
        self.current_position_title = current_position_title
        self.current_company_name = current_company_name
        self.min_annual_salary = min_annual_salary
        self.desired_positions = desired_positions
        self.preferences = preferences
        self.skills = skills
        self.experience = experience
        self.referral_link_id = referral_link_id
        self.followed_companies_ids = followed_companies_ids
        self.followed_disabled_companies_ids = followed_disabled_companies_ids
        self.user_country = user_country
        self.user_country_location = user_country_location
        self.user_relocation_locations = user_relocation_locations
        self.user_notification_options = user_notification_options
        self.missing_profile_parts = missing_profile_parts
        self.changed_after_last_validation_profile_parts = changed_after_last_validation_profile_parts
        self.validation_status = validation_status
        self.validation_status_update_sent = validation_status_update_sent
        self.validation_status_updated_at = validation_status_updated_at
        self.validation_rejection_reasons = validation_rejection_reasons
        self.last_interaction_time = last_interaction_time
        self.last_profile_not_ready_notification_time = last_profile_not_ready_notification_time
        self.profile_not_ready_notifications_sent = profile_not_ready_notifications_sent
        self.last_daily_jobs_sent_at = last_daily_jobs_sent_at
        self.last_weekly_jobs_sent_at = last_weekly_jobs_sent_at
        self.last_user_feedback_sent_at = last_user_feedback_sent_at

    def set_profile_detail_as_updated(self, user_profile_part: UserProfileRequiredPart):
        self.changed_after_last_validation_profile_parts = \
            list(set(self.changed_after_last_validation_profile_parts or []) | {user_profile_part})
        self.missing_profile_parts = [
            part for part in self.missing_profile_parts if part != user_profile_part
        ]
        if not self.missing_profile_parts:
            self.validation_status = UserProfileValidationStatus.WAITING_VERIFICATION
            self.validation_status_updated_at = datetime.utcnow()

    def message_ids(self) -> List[int]:
        return [m_id for m_id in (self.tg_settings_message_id,
                                  self.tg_profile_message_id,
                                  self.tg_subscriptions_message_id,
                                  self.tg_feedback_message_id,
                                  self.tg_notification_message_id) if m_id is not None]

    def reject_profile_details(
            self, rejected_profile_parts: List[UserProfileRequiredPart],
            rejection_reasons: List[UserProfileRejectionReason],
            user_validation_status: Optional[UserProfileValidationStatus]
            = UserProfileValidationStatus.REJECTED):
        self.changed_after_last_validation_profile_parts = []
        self.validation_rejection_reasons = rejection_reasons
        self.missing_profile_parts = list(set(self.missing_profile_parts + rejected_profile_parts))
        if user_validation_status is not None and self.validation_status != user_validation_status:
            self.validation_status_update_sent = False
            self.validation_status = user_validation_status
            self.validation_status_updated_at = datetime.utcnow()

    def accept_profile_details(self):
        self.changed_after_last_validation_profile_parts = []
        self.missing_profile_parts = []
        self.validation_rejection_reasons = []
        if self.validation_status != UserProfileValidationStatus.VERIFIED:
            self.validation_status_update_sent = False
            self.validation_status = UserProfileValidationStatus.VERIFIED
            self.validation_status_updated_at = datetime.utcnow()

    def as_dict(self) -> Dict[str, Any]:
        return {
            "tg_id": self.tg_id,
            "tg_chat_id": self.tg_chat_id,
            "tg_settings_message_id": self.tg_settings_message_id,
            "tg_profile_message_id": self.tg_profile_message_id,
            "tg_subscriptions_message_id": self.tg_subscriptions_message_id,
            "tg_feedback_message_id": self.tg_feedback_message_id,
            "tg_notification_message_id": self.tg_notification_message_id,
            "referred_by_tg_id": self.referred_by_tg_id,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "cv_upload_datetimes": self.cv_upload_datetimes,
            "name": self.name,
            "email": self.email,
            "current_position_title": self.current_position_title,
            "current_company_name": self.current_company_name,
            "min_annual_salary": self.min_annual_salary,
            "desired_positions": self.desired_positions,
            "preferences": self.preferences,
            "skills": self.skills,
            "experience": self.experience,
            "referral_link_id": self.referral_link_id,
            "followed_companies_ids": self.followed_companies_ids,
            "followed_disabled_companies_ids": self.followed_disabled_companies_ids,
            "user_country": self.user_country,
            "user_country_location": self.user_country_location,
            "user_relocation_locations": {str(c.value): cities for c, cities in self.user_relocation_locations.items()},
            "user_notification_options": self.user_notification_options,
            "changed_after_last_validation_profile_parts": self.changed_after_last_validation_profile_parts,
            "missing_profile_parts": self.missing_profile_parts,
            "validation_status": self.validation_status,
            "validation_status_update_sent": self.validation_status_update_sent,
            "validation_status_updated_at": self.validation_status_updated_at,
            "validation_rejection_reasons": self.validation_rejection_reasons,
            "last_interaction_time": self.last_interaction_time,
            "last_profile_not_ready_notification_time": self.last_profile_not_ready_notification_time,
            "profile_not_ready_notifications_sent": self.profile_not_ready_notifications_sent,
            "last_daily_jobs_sent_at": self.last_daily_jobs_sent_at,
            "last_weekly_jobs_sent_at": self.last_weekly_jobs_sent_at,
            "last_user_feedback_sent_at": self.last_user_feedback_sent_at
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'User':
        desired_positions: List[Optional[JobPosition]] = [
            cls._parse_enum(JobPosition, pos, null_if_can_not_parse=True) for pos in d.get("desired_positions", [])
        ]
        preferences: List[Optional[UserPreferences]] = [
            cls._parse_enum(UserPreferences, preference, null_if_can_not_parse=True)
            for preference in d.get("preferences", [])
        ]
        skills: List[Optional[UserSkill]] = [
            cls._parse_enum(UserSkill, skill, null_if_can_not_parse=True) for skill in d.get("skills", [])
        ]
        user_notification_options: List[Optional[UserNotificationOption]] = [
            cls._parse_enum(UserNotificationOption, option, null_if_can_not_parse=True)
            for option in d.get("user_notification_options", [])
        ]
        missing_profile_parts: List[Optional[UserProfileRequiredPart]] = [
            cls._parse_enum(UserProfileRequiredPart, part, null_if_can_not_parse=True)
            for part in d.get("missing_profile_parts", [])
        ]
        changed_after_last_validation_profile_parts: List[Optional[UserProfileRequiredPart]] = [
            cls._parse_enum(UserProfileRequiredPart, part, null_if_can_not_parse=True)
            for part in d.get("changed_after_last_validation_profile_parts", [])
        ]
        validation_rejection_reasons: List[Optional[UserProfileRejectionReason]] = [
            cls._parse_enum(UserProfileRejectionReason, reason, null_if_can_not_parse=True)
            for reason in d.get("validation_rejection_reasons", [])
        ]
        user_relocation_locations: Dict[UserCountry, List[CountryLocations]] = dict()
        for country_str, cities in d.get("user_relocation_locations", dict()).items():
            country: Optional[UserCountry] = cls._parse_enum(UserCountry, country_str, null_if_can_not_parse=True)
            cities: List[Optional[CountryLocations]] = [cls._parse_enum(CountryLocations, city,
                                                                        null_if_can_not_parse=True) for city in cities]
            cities: List[CountryLocations] = [city for city in cities if city is not None]
            if country is not None:
                user_relocation_locations[country] = cities

        return User(
            tg_id=d["tg_id"],
            tg_chat_id=d.get("tg_chat_id"),
            tg_settings_message_id=d.get("tg_settings_message_id"),
            tg_profile_message_id=d.get("tg_profile_message_id"),
            tg_subscriptions_message_id=d.get("tg_subscriptions_message_id"),
            tg_feedback_message_id=d.get("tg_feedback_message_id"),
            tg_notification_message_id=d.get("tg_notification_message_id"),
            referred_by_tg_id=d.get("referred_by_tg_id"),
            created_at=cls._parse_datetime(d["created_at"]),
            updated_at=cls._parse_datetime(d["updated_at"]),
            cv_upload_datetimes=[cls._parse_datetime(dt) for dt in d.get("cv_upload_datetimes", [])],
            name=d.get("name"),
            email=d.get("email"),
            current_position_title=d.get("current_position_title"),
            current_company_name=d.get("current_company_name"),
            min_annual_salary=cls._parse_enum(
                enum_class=UserDesiredMinAnnualGrossSalary,
                value=d.get("min_annual_salary"),
                default_value_if_can_not_parse=UserDesiredMinAnnualGrossSalary.NO_PREFERENCES
            ),
            desired_positions=[pos for pos in desired_positions if pos is not None],
            preferences=[preference for preference in preferences if preference is not None],
            skills=[skill for skill in skills if skill is not None],
            experience=cls._parse_enum(
                enum_class=UserExperience,
                value=d.get("experience"),
                null_if_can_not_parse=True
            ),
            referral_link_id=d.get("referral_link_id"),
            followed_companies_ids=d.get("followed_companies_ids") or [],
            followed_disabled_companies_ids=d.get("followed_disabled_companies_ids") or [],
            user_country=cls._parse_enum(UserCountry, value=d.get("user_country"), null_if_can_not_parse=True),
            user_country_location=cls._parse_enum(CountryLocations, value=d.get("user_country_location"),
                                                  null_if_can_not_parse=True),
            user_relocation_locations=user_relocation_locations,
            user_notification_options=[option for option in user_notification_options if option is not None],
            missing_profile_parts=[part for part in missing_profile_parts if part is not None],
            changed_after_last_validation_profile_parts=[part for part in changed_after_last_validation_profile_parts
                                                         if part is not None],
            validation_status=cls._parse_enum(UserProfileValidationStatus, d.get("validation_status"),
                                              default_value_if_can_not_parse=UserProfileValidationStatus.NOT_READY),
            validation_status_update_sent=d.get("validation_status_update_sent"),
            validation_status_updated_at=d.get("validation_status_updated_at"),
            validation_rejection_reasons=[reason for reason in validation_rejection_reasons if reason is not None],
            last_interaction_time=d.get("last_interaction_time"),
            last_profile_not_ready_notification_time=d.get("last_profile_not_ready_notification_time"),
            profile_not_ready_notifications_sent=d.get("profile_not_ready_notifications_sent", 0),
            last_daily_jobs_sent_at=d.get("last_daily_jobs_sent_at"),
            last_weekly_jobs_sent_at=d.get("last_weekly_jobs_sent_at"),
            last_user_feedback_sent_at=None if d.get("last_user_feedback_sent_at") is None
            else cls._parse_datetime(d.get("last_user_feedback_sent_at"))
        )

    @classmethod
    def new_empty_user(cls, tg_id: int) -> 'User':
        return User(
            tg_id=tg_id,
            tg_chat_id=tg_id,
            tg_settings_message_id=None,
            tg_profile_message_id=None,
            tg_subscriptions_message_id=None,
            tg_feedback_message_id=None,
            tg_notification_message_id=None,
            referred_by_tg_id=None,
            created_at=datetime.utcnow(),
            updated_at=datetime.utcnow(),
            cv_upload_datetimes=[],
            name=None,
            email=None,
            current_position_title=None,
            current_company_name=None,
            min_annual_salary=UserDesiredMinAnnualGrossSalary.NO_PREFERENCES,
            desired_positions=[],
            preferences=[UserPreferences.NO_INTERNSHIP, UserPreferences.NO_AGENCY, UserPreferences.NO_TEMPORARY,
                         UserPreferences.READY_FOR_HYBRID, UserPreferences.READY_FOR_REMOTE,
                         UserPreferences.READY_FOR_OFFICE],
            skills=[],
            experience=None,
            referral_link_id=None,
            followed_companies_ids=[],
            followed_disabled_companies_ids=[],
            user_country=None,
            user_country_location=None,
            user_relocation_locations=dict(),
            user_notification_options=[option for option in UserNotificationOption],
            missing_profile_parts=[part for part in UserProfileRequiredPart],
            changed_after_last_validation_profile_parts=[],
            validation_status=UserProfileValidationStatus.NOT_READY,
            validation_status_update_sent=None,
            validation_status_updated_at=None,
            validation_rejection_reasons=[],
            last_interaction_time=datetime.utcnow(),
            last_profile_not_ready_notification_time=None,
            profile_not_ready_notifications_sent=0,
            last_daily_jobs_sent_at=None,
            last_weekly_jobs_sent_at=None,
            last_user_feedback_sent_at=None
        )


class UserCv(JobsBotModel):
    def __init__(self, tg_id: int, file: bytes, filename: str, upload_date: datetime):
        self.tg_id = tg_id
        self.file = file
        self.filename = filename
        self.upload_date = upload_date

    def as_dict(self) -> Dict[str, Any]:
        return {
            "tg_id": self.tg_id,
            "file": self.file,
            "filename": self.filename,
            "upload_date": self.upload_date
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'UserCv':
        return UserCv(
            tg_id=d["tg_id"],
            file=d["file"],
            filename=d["filename"],
            upload_date=d["upload_date"]
        )


class Company(JobsBotModel):
    def __init__(self, company_id: Optional[str], company_name: str, referral_link_id: Optional[str], is_agency: bool,
                 is_consulting: bool, is_startup: bool):
        self.company_id = company_id
        self.company_name = company_name
        self.referral_link_id = referral_link_id
        self.is_agency = is_agency
        self.is_consulting = is_consulting
        self.is_startup = is_startup

    def as_dict(self) -> Dict[str, Any]:
        return {
            "company_id": self.company_id,
            "company_name": self.company_name,
            "referral_link_id": self.referral_link_id,
            "is_agency": self.is_agency,
            "is_consulting": self.is_consulting,
            "is_startup": self.is_startup
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Company':
        return Company(
            company_id=d.get("company_id"),
            company_name=d["company_name"],
            referral_link_id=d.get("referral_link_id"),
            is_agency=d.get("is_agency", False),
            is_consulting=d.get("is_consulting", False),
            is_startup=d.get("is_startup", False)
        )


class Job(JobsBotModel):
    def __init__(self, job_id: Optional[str], job_posting_status: JobPostingStatus,
                 job_posting_validation_status: JobPostingValidationStatus, job_url: Optional[str],
                 allow_one_click_applications: bool, allow_non_complete_profiles: bool,
                 created_at: datetime, updated_at: datetime,
                 created_by: str, company_id: str, position: JobPosition, title: str,
                 min_experience: UserExperience, must_have_skills: List[UserSkill],
                 better_to_have_skills: List[UserSkill], hiring_manager_name: Optional[str],
                 department_name: Optional[str], description: str, about_company: str, technologies: Optional[str],
                 locations: List[CountryLocations],
                 remote_work_from_countries: List[UserCountry], annual_salary_min: int,
                 annual_salary_max: int, show_annual_salary: bool, is_temporary: bool, is_internship: bool,
                 with_conferences_budget: bool,
                 fully_remote_allowed: bool, partially_remote_allowed: bool, work_from_office_is_not_allowed: bool,
                 with_relocation: bool, relocation_from_countries: List[UserCountry], stocks_available: bool):
        self.job_id = job_id
        self.job_posting_status = job_posting_status
        self.job_posting_validation_status = job_posting_validation_status
        self.job_url = job_url
        self.allow_one_click_applications = allow_one_click_applications
        self.allow_non_complete_profiles = allow_non_complete_profiles
        self.created_at = created_at
        self.updated_at = updated_at
        self.created_by = created_by
        self.company_id = company_id
        self.position = position
        self.title = title
        self.min_experience = min_experience
        self.must_have_skills = must_have_skills
        self.better_to_have_skills = better_to_have_skills
        self.hiring_manager_name = hiring_manager_name
        self.department_name = department_name
        self.description = description
        self.about_company = about_company
        self.technologies = technologies
        self.locations = locations
        self.remote_work_from_countries = remote_work_from_countries
        self.annual_salary_min = annual_salary_min
        self.annual_salary_max = annual_salary_max
        self.show_annual_salary = show_annual_salary
        self.is_temporary = is_temporary
        self.is_internship = is_internship
        self.with_conferences_budget = with_conferences_budget
        self.fully_remote_allowed = fully_remote_allowed
        self.partially_remote_allowed = partially_remote_allowed
        self.work_from_office_is_not_allowed = work_from_office_is_not_allowed
        self.with_relocation = with_relocation
        self.relocation_from_countries = relocation_from_countries
        self.stocks_available = stocks_available

    @property
    def apply_on_title(self) -> Optional[str]:
        if not self.job_url:
            return None
        sites = {
            "linkedin.": "Linkedin"
        }
        prefixes = ["http://", "https://", "http://www.", "https://www."]
        for site, title in sites.items():
            for prefix in prefixes:
                if self.job_url.startswith(f"{prefix}{site}"):
                    return title
        return "website"

    @property
    def apply_on_text(self) -> Optional[str]:
        _apply_on_title = self.apply_on_title
        if _apply_on_title is None:
            return None
        if _apply_on_title == "website":
            return "Apply on a website"
        else:
            return f"Apply on {_apply_on_title}"

    def as_dict(self) -> Dict[str, Any]:
        return {
            "job_id": self.job_id,
            "job_posting_status": self.job_posting_status,
            "job_posting_validation_status": self.job_posting_validation_status,
            "job_url": self.job_url,
            "allow_one_click_applications": self.allow_one_click_applications,
            "allow_non_complete_profiles": self.allow_non_complete_profiles,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "created_by": self.created_by,
            "company_id": self.company_id,
            "position": self.position,
            "title": self.title,
            "min_experience": self.min_experience,
            "must_have_skills": self.must_have_skills,
            "better_to_have_skills": self.better_to_have_skills,
            "hiring_manager_name": self.hiring_manager_name,
            "department_name": self.department_name,
            "description": self.description,
            "about_company": self.about_company,
            "technologies": self.technologies,
            "locations": self.locations,
            "remote_work_from_countries": self.remote_work_from_countries,
            "annual_salary_min": self.annual_salary_min,
            "annual_salary_max": self.annual_salary_max,
            "show_annual_salary": self.show_annual_salary,
            "is_temporary": self.is_temporary,
            "is_internship": self.is_internship,
            "with_conferences_budget": self.with_conferences_budget,
            "fully_remote_allowed": self.fully_remote_allowed,
            "partially_remote_allowed": self.partially_remote_allowed,
            "work_from_office_is_not_allowed": self.work_from_office_is_not_allowed,
            "with_relocation": self.with_relocation,
            "relocation_from_countries": self.relocation_from_countries,
            "stocks_available": self.stocks_available
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Job':
        must_have_skills: List[Optional[UserSkill]] = [
            cls._parse_enum(enum_class=UserSkill, value=skill, null_if_can_not_parse=True)
            for skill in d.get("must_have_skills", [])
        ]

        better_to_have_skills: List[Optional[UserSkill]] = [
            cls._parse_enum(enum_class=UserSkill, value=skill, null_if_can_not_parse=True)
            for skill in d.get("better_to_have_skills", [])
        ]

        locations: List[Optional[CountryLocations]] = [
            cls._parse_enum(enum_class=CountryLocations, value=location, null_if_can_not_parse=True)
            for location in d.get("locations", [])
        ]

        remote_work_from_countries: List[Optional[UserCountry]] = [
            cls._parse_enum(enum_class=UserCountry, value=country, null_if_can_not_parse=True)
            for country in d.get("remote_work_from_countries", [])
        ]

        relocation_from_countries: List[Optional[UserCountry]] = [
            cls._parse_enum(enum_class=UserCountry, value=country, null_if_can_not_parse=True)
            for country in d.get("relocation_from_countries", [])
        ]

        return Job(
            job_id=d.get("job_id"),
            job_posting_status=JobPostingStatus.parse(d["job_posting_status"]),
            job_posting_validation_status=JobPostingValidationStatus.parse(d["job_posting_validation_status"]),
            job_url=d.get("job_url"),
            allow_one_click_applications=d.get("allow_one_click_applications", True),
            allow_non_complete_profiles=d.get("allow_non_complete_profiles", True),
            created_at=None if "created_at" not in d else cls._parse_datetime(d["created_at"]),
            updated_at=None if "updated_at" not in d else cls._parse_datetime(d["updated_at"]),
            created_by=d.get("created_by", d.get("created_by_recruiter_id", d.get("created_by_admin_id", "n/a"))),
            company_id=d["company_id"],
            position=JobPosition.parse(d["position"]),
            title=d["title"],
            min_experience=UserExperience.parse(d.get("min_experience", 0)),
            must_have_skills=[skill for skill in must_have_skills if skill is not None],
            better_to_have_skills=[skill for skill in better_to_have_skills if skill is not None],
            hiring_manager_name=d.get("hiring_manager_name"),
            department_name=d.get("department_name"),
            description=d["description"],
            about_company=d["about_company"],
            technologies=d.get("technologies"),
            locations=[location for location in locations if location is not None],
            remote_work_from_countries=[country for country in remote_work_from_countries if country is not None],
            annual_salary_min=d["annual_salary_min"],
            annual_salary_max=d["annual_salary_max"],
            show_annual_salary=d["show_annual_salary"],
            is_temporary=d["is_temporary"],
            is_internship=d["is_internship"],
            with_conferences_budget=d["with_conferences_budget"],
            fully_remote_allowed=d["fully_remote_allowed"],
            partially_remote_allowed=d["partially_remote_allowed"],
            work_from_office_is_not_allowed=d.get("work_from_office_is_not_allowed", False),
            with_relocation=d["with_relocation"],
            relocation_from_countries=[country for country in relocation_from_countries if country is not None],
            stocks_available=d.get("stocks_available", False)
        )


class JobStatistics(JobsBotModel):
    def __init__(self, job_id: str, counter_total_matches: Dict[UserJobMatchType, int],
                 counter_total_applications_by_method: Dict[UserJobApplicationMethod, int],
                 counter_total_applications_by_status: Dict[UserJobApplicationStatus, int],
                 chart_matches_last_30_days: Dict[UserJobMatchType, List[Tuple[datetime, int]]],
                 chart_daily_notifications_sent_last_30_days: Dict[UserJobMatchType, List[Tuple[datetime, int]]],
                 chart_weekly_notifications_sent_last_30_days: Dict[UserJobMatchType, List[Tuple[datetime, int]]],
                 chart_applications_by_method_last_30_days: Dict[UserJobApplicationMethod, List[Tuple[datetime, int]]],
                 chart_applications_by_status_last_30_days: Dict[UserJobApplicationStatus, List[Tuple[datetime, int]]],
                 counter_total_daily_notification_sent: Dict[UserJobMatchType, int],
                 counter_total_weekly_notification_sent: Dict[UserJobMatchType, int]):
        self.job_id = job_id
        self.counter_total_matches = counter_total_matches
        self.counter_total_applications_by_method = counter_total_applications_by_method
        self.counter_total_applications_by_status = counter_total_applications_by_status
        self.chart_matches_last_30_days = chart_matches_last_30_days
        self.chart_daily_notifications_sent_last_30_days = chart_daily_notifications_sent_last_30_days
        self.chart_weekly_notifications_sent_last_30_days = chart_weekly_notifications_sent_last_30_days
        self.chart_applications_by_method_last_30_days = chart_applications_by_method_last_30_days
        self.chart_applications_by_status_last_30_days = chart_applications_by_status_last_30_days
        self.counter_total_daily_notification_sent = counter_total_daily_notification_sent
        self.counter_total_weekly_notification_sent = counter_total_weekly_notification_sent

    @classmethod
    def empty_statistics(cls, job_id: str) -> 'JobStatistics':
        return JobStatistics(
            job_id=job_id,
            counter_total_matches={mt: 0 for mt in UserJobMatchType},
            counter_total_applications_by_status={m: 0 for m in UserJobApplicationStatus},
            counter_total_applications_by_method={s: 0 for s in UserJobApplicationMethod},
            chart_matches_last_30_days={mt: [] for mt in UserJobMatchType},
            chart_daily_notifications_sent_last_30_days={mt: [] for mt in UserJobMatchType},
            chart_weekly_notifications_sent_last_30_days={mt: [] for mt in UserJobMatchType},
            chart_applications_by_status_last_30_days={s: [] for s in UserJobApplicationStatus},
            chart_applications_by_method_last_30_days={m: [] for m in UserJobApplicationMethod},
            counter_total_daily_notification_sent={mt: 0 for mt in UserJobMatchType},
            counter_total_weekly_notification_sent={mt: 0 for mt in UserJobMatchType}
        )

    def as_dict(self) -> Dict[str, Any]:
        data = dict()
        for field_name in ("counter_total_matches", "counter_total_applications_by_method",
                           "counter_total_applications_by_status", "chart_matches_last_30_days",
                           "chart_daily_notifications_sent_last_30_days",
                           "chart_weekly_notifications_sent_last_30_days",
                           "chart_applications_by_method_last_30_days", "chart_applications_by_status_last_30_days",
                           "counter_total_daily_notification_sent", "counter_total_weekly_notification_sent"):
            data[field_name] = {k.name: v for k, v in getattr(self, field_name).items()}
        return {
            "job_id": self.job_id,
            **data
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'JobStatistics':
        counters = dict()
        for field_name, parse_class in (
                ("counter_total_matches", UserJobMatchType),
                ("counter_total_applications_by_method", UserJobApplicationMethod),
                ("counter_total_applications_by_status", UserJobApplicationStatus),
                ("counter_total_daily_notification_sent", UserJobMatchType),
                ("counter_total_weekly_notification_sent", UserJobMatchType)
        ):
            counters[field_name] = dict()
            for method, value in d[field_name].items():
                counters[field_name][parse_class.parse(method)] = value

        graphs = dict()
        for field_name, parse_class in (
                ("chart_matches_last_30_days", UserJobMatchType),
                ("chart_daily_notifications_sent_last_30_days", UserJobMatchType),
                ("chart_weekly_notifications_sent_last_30_days", UserJobMatchType),
                ("chart_applications_by_method_last_30_days", UserJobApplicationMethod),
                ("chart_applications_by_status_last_30_days", UserJobApplicationStatus)
        ):
            graphs[field_name] = dict()
            for method, value in d[field_name].items():
                graphs[field_name][parse_class.parse(method)] = [(dt, c) for dt, c in value]

        return JobStatistics(
            job_id=d["job_id"],
            **counters,
            **graphs
        )


class UserJobMatch(JobsBotModel):
    def __init__(self, user_tg_id: int, job_id: str, matched_on: datetime,
                 match_type: UserJobMatchType, match_source: UserJobMatchSource, daily_notification_sent: bool,
                 daily_notification_sent_at: Optional[datetime], weekly_notification_sent: bool,
                 weekly_notification_sent_at: Optional[datetime]):
        self.user_tg_id = user_tg_id
        self.job_id = job_id
        self.matched_on = matched_on
        self.match_type = match_type
        self.match_source = match_source
        self.daily_notification_sent = daily_notification_sent
        self.daily_notification_sent_at = daily_notification_sent_at
        self.weekly_notification_sent = weekly_notification_sent
        self.weekly_notification_sent_at = weekly_notification_sent_at

    @property
    def is_user_notified(self) -> bool:
        return self.daily_notification_sent or self.weekly_notification_sent

    def as_dict(self) -> Dict[str, Any]:
        return {
            "user_tg_id": self.user_tg_id,
            "job_id": self.job_id,
            "matched_on": self.matched_on,
            "match_type": self.match_type,
            "match_source": self.match_source,
            "daily_notification_sent": self.daily_notification_sent,
            "daily_notification_sent_at": self.daily_notification_sent_at,
            "weekly_notification_sent": self.weekly_notification_sent,
            "weekly_notification_sent_at": self.weekly_notification_sent_at,
            "match_hash": self.match_hash
        }

    @property
    def match_hash(self):
        return sha256(("JOB_ID:" + str(self.job_id) + "USER_ID:" + str(self.user_tg_id)).encode("utf-8")).hexdigest()

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'UserJobMatch':
        return UserJobMatch(
            user_tg_id=d["user_tg_id"],
            job_id=d["job_id"],
            matched_on=d["matched_on"],
            match_type=UserJobMatchType.parse(d.get("match_type", UserJobMatchType.FULL_MATCH)),
            match_source=UserJobMatchSource.parse(d.get("match_source", UserJobMatchSource.JOB_MATCH)),
            daily_notification_sent=d["daily_notification_sent"],
            daily_notification_sent_at=None if "daily_notification_sent_at" not in d
                                               or d["daily_notification_sent_at"] is None
            else cls._parse_datetime(d["daily_notification_sent_at"]),
            weekly_notification_sent_at=None if "weekly_notification_sent_at" not in d
                                                or d["weekly_notification_sent_at"] is None
            else cls._parse_datetime(d["weekly_notification_sent_at"]),
            weekly_notification_sent=d["weekly_notification_sent"]
        )


class UserJobApplication(JobsBotModel):
    def __init__(self, user_tg_id: int, job_id: str, created_at: datetime, last_time_applied_at: datetime,
                 user_job_application_status: UserJobApplicationStatus, times_applied: int,
                 user_job_application_method: UserJobApplicationMethod):
        self.user_tg_id = user_tg_id
        self.job_id = job_id
        self.created_at = created_at
        self.last_time_applied_at = last_time_applied_at
        self.user_job_application_status = user_job_application_status
        self.times_applied = times_applied
        self.user_job_application_method = user_job_application_method

    def as_dict(self) -> Dict[str, Any]:
        return {
            "user_tg_id": self.user_tg_id,
            "job_id": self.job_id,
            "created_at": self.created_at,
            "last_time_applied_at": self.last_time_applied_at,
            "user_job_application_status": self.user_job_application_status,
            "user_job_application_method": self.user_job_application_method,
            "times_applied": self.times_applied
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'UserJobApplication':
        return UserJobApplication(
            user_tg_id=d["user_tg_id"],
            job_id=d["job_id"],
            created_at=d["created_at"],
            last_time_applied_at=d.get("last_time_applied_at") or d["created_at"],
            user_job_application_status=UserJobApplicationStatus.parse(d["user_job_application_status"]),
            user_job_application_method=UserJobApplicationMethod.parse(d.get("user_job_application_method")
                                                                       or UserJobApplicationMethod.ONE_CLICK),
            times_applied=d.get("times_applied") or 1
        )


class UserSubscription(JobsBotModel):
    def __init__(self, user_tg_id: int, company_id: str, job_categories: List[JobCategory],
                 job_positions: List[JobPosition], all_matching_positions: bool,
                 muted_until: Optional[datetime] = None):
        self.user_tg_id = user_tg_id
        self.company_id = company_id
        self.job_categories = job_categories
        self.job_positions = job_positions
        self.all_matching_positions = all_matching_positions
        self.muted_until = muted_until

    def as_dict(self) -> Dict[str, Any]:
        return {
            "user_tg_id": self.user_tg_id,
            "company_id": self.company_id,
            "job_categories": self.job_categories,
            "job_positions": self.job_positions,
            "all_matching_positions": self.all_matching_positions,
            "muted_until": self.muted_until
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'UserSubscription':
        job_categories: List[Optional[JobCategory]] = [
            cls._parse_enum(enum_class=JobCategory, value=category, null_if_can_not_parse=True)
            for category in d.get("job_categories", [])
        ]
        job_positions: List[Optional[JobPosition]] = [
            cls._parse_enum(enum_class=JobPosition, value=position, null_if_can_not_parse=True)
            for position in d.get("job_positions", [])
        ]

        return UserSubscription(
            user_tg_id=d["user_tg_id"],
            company_id=d["company_id"],
            job_categories=[category for category in job_categories if category is not None],
            job_positions=[position for position in job_positions if position is not None],
            all_matching_positions=d["all_matching_positions"],
            muted_until=cls._parse_datetime(d["muted_until"])
        )


class Credentials(JobsBotModel):
    def __init__(self, email: str, hashed_password: Optional[str] = None, salt: Optional[str] = None,
                 raw_password: Optional[str] = None):
        self.email = email
        self.hashed_password = None
        self.salt = None
        if raw_password is not None:
            if hashed_password is not None or salt is not None:
                raise ValueError()
            self.salt = self._generate_salt()
            self.hashed_password = self.hash_password(raw_password, self.salt)
        elif hashed_password is not None and salt is not None:
            self.hashed_password = hashed_password
            self.salt = salt
        else:
            raise ValueError("Either raw password or hashed password should be set while creating Credentials")

    @classmethod
    def _generate_salt(cls, salt_len: int = 16) -> str:
        return "".join(choices(ascii_letters + digits, k=salt_len))

    @classmethod
    def canonize_email(cls, email: str):
        return email.lower()

    @property
    def canonized_email(self):
        return self.canonize_email(self.email)

    @classmethod
    def hash_password(cls, raw_password: str, salt: str) -> str:
        return sha256((salt + raw_password + salt).encode("utf-8")).hexdigest()

    def update_password(self, new_raw_password):
        self.salt = self._generate_salt()
        self.hashed_password = self.hash_password(new_raw_password, self.salt)

    def is_password_correct(self, raw_password):
        return self.hashed_password == self.hash_password(raw_password, self.salt)

    def as_dict(self) -> Dict[str, Any]:
        return {
            "email": self.email,
            "canonized_email": self.canonized_email,
            "hashed_password": self.hashed_password,
            "salt": self.salt
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Credentials':
        return Credentials(
            email=d["email"],
            raw_password=d.get("raw_password"),
            hashed_password=d.get("hashed_password"),
            salt=d.get("salt")
        )


class Admin(JobsBotModel):
    def __init__(self, email: str, name: str, admin_permissions: List[AdminPermission]):
        self.email = email
        self.name = name
        self.admin_permissions = admin_permissions

    @property
    def canonized_email(self):
        return Credentials.canonize_email(self.email)

    def as_dict(self) -> Dict[str, Any]:
        return {
            "email": self.email,
            "name": self.name,
            "canonized_email": self.canonized_email,
            "admin_permissions": self.admin_permissions
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Admin':
        permissions: List[Optional[AdminPermission]] = [
            cls._parse_enum(enum_class=AdminPermission, value=permission, null_if_can_not_parse=True)
            for permission in d.get("admin_permissions", [])
        ]
        return Admin(
            email=d["email"],
            name=d.get("name", ""),
            admin_permissions=[permission for permission in permissions if permission is not None]
        )


class Recruiter(JobsBotModel):
    def __init__(self, email: str, name: str, company_id: str, recruiter_permissions: List[RecruiterPermission]):
        self.email = email
        self.name = name
        self.company_id = company_id
        self.recruiter_permissions = recruiter_permissions

    @property
    def canonized_email(self):
        return Credentials.canonize_email(self.email)

    def as_dict(self) -> Dict[str, Any]:
        return {
            "email": self.email,
            "name": self.name,
            "company_id": self.company_id,
            "canonized_email": self.canonized_email,
            "recruiter_permissions": self.recruiter_permissions
        }

    @classmethod
    def from_recruiter_invitation(cls, recruiter_invitation: 'RecruiterInvitation') -> 'Recruiter':
        return Recruiter(
            email=recruiter_invitation.email,
            name=recruiter_invitation.name,
            company_id=recruiter_invitation.company_id,
            recruiter_permissions=recruiter_invitation.recruiter_permissions
        )

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Recruiter':
        recruiter_permissions: List[Optional[RecruiterPermission]] = [
            cls._parse_enum(enum_class=RecruiterPermission, value=permission, null_if_can_not_parse=True)
            for permission in d.get("recruiter_permissions", [])
        ]

        return Recruiter(
            email=d["email"],
            name=d.get("name", ""),
            company_id=d["company_id"],
            recruiter_permissions=[permission for permission in recruiter_permissions if permission is not None]
        )


class RecruiterInvitation(JobsBotModel):
    EXPIRED_AFTER = timedelta(days=3)

    def __init__(self, email: str, name: str, company_id: str, recruiter_permissions: List[RecruiterPermission],
                 invited_by: str, invited_at: datetime, invitation_token: Optional[str], invitation_sent: bool):
        self.email = email
        self.name = name
        self.company_id = company_id
        self.recruiter_permissions = recruiter_permissions
        self.invited_by = invited_by
        self.invited_at = invited_at
        self.invitation_token = invitation_token
        self.invitation_sent = invitation_sent

    @classmethod
    def from_recruiter(cls, recruiter: Recruiter, invited_by: str, invited_at: datetime = None) \
            -> 'RecruiterInvitation':
        return RecruiterInvitation(
            email=recruiter.email,
            name=recruiter.name,
            company_id=recruiter.company_id,
            recruiter_permissions=recruiter.recruiter_permissions,
            invited_by=invited_by,
            invited_at=invited_at or datetime.utcnow(),
            invitation_token=None,
            invitation_sent=False
        )

    @property
    def canonized_email(self) -> str:
        return Credentials.canonize_email(self.email)

    @property
    def expired(self) -> bool:
        return self.invited_at < datetime.utcnow() - self.EXPIRED_AFTER

    def as_dict(self) -> Dict[str, Any]:
        return {
            "email": self.email,
            "canonized_email": self.canonized_email,
            "name": self.name,
            "company_id": self.company_id,
            "recruiter_permissions": self.recruiter_permissions,
            "invited_by": self.invited_by,
            "invited_at": self.invited_at,
            "invitation_token": self.invitation_token,
            "invitation_sent": self.invitation_sent
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'RecruiterInvitation':
        recruiter_permissions: List[Optional[RecruiterPermission]] = [
            cls._parse_enum(enum_class=RecruiterPermission, value=permission, null_if_can_not_parse=True)
            for permission in d.get("recruiter_permissions", [])
        ]

        return RecruiterInvitation(
            email=d["email"],
            name=d["name"],
            company_id=d["company_id"],
            recruiter_permissions=[permission for permission in recruiter_permissions if permission is not None],
            invited_by=d["invited_by"],
            invited_at=cls._parse_datetime(d["invited_at"]),
            invitation_token=d["invitation_token"],
            invitation_sent=d["invitation_sent"]
        )


class Session(JobsBotModel):
    def __init__(self, email: str, token: str, expires_at: datetime):
        self.email = email
        self.token = token
        self.expires_at = expires_at

    @property
    def canonized_email(self):
        return Credentials.canonize_email(self.email)

    def as_dict(self) -> Dict[str, Any]:
        return {
            "email": self.email,
            "canonized_email": self.canonized_email,
            "token": self.token,
            "expires_at": self.expires_at
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Session':
        expires_at = cls._parse_datetime(d["expires_at"])
        if expires_at is None:
            raise ValueError("Data is corrupted: expires_at is not defined")

        return Session(
            email=d["email"],
            token=d["token"],
            expires_at=expires_at
        )


class UserFeedback(JobsBotModel):
    def __init__(self, tg_id: int, message: str, sent_at: datetime):
        self.tg_id = tg_id
        self.message = message
        self.sent_at = sent_at

    def as_dict(self) -> Dict[str, Any]:
        return {
            "tg_id": self.tg_id,
            "message": self.message,
            "sent_at": self.sent_at
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'UserFeedback':
        return UserFeedback(
            tg_id=d["tg_id"],
            message=d["message"],
            sent_at=cls._parse_datetime(d["sent_at"])
        )
