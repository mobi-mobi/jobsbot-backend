import pickle

from abc import ABC, abstractmethod
from datetime import datetime
from enum import Enum
from functools import lru_cache
from typing import Any, Dict, List, Optional, Set, Type, Union


class HrBotEnum(Enum):
    @classmethod
    def _titles(cls) -> Dict['HrBotEnum', str]:
        return {}

    @classmethod
    @lru_cache
    def _cached_titles(cls) -> Dict['HrBotEnum', str]:
        return cls._titles()

    def title(self) -> str:
        return self._cached_titles()[self]

    @classmethod
    def parse(cls, value: Union[int, str, 'HrBotEnum']):
        def raise_can_not_parse():
            raise ValueError(f"Can not parse value \"{value}\"") from None

        def enum_from_int(_value_int: int) -> 'HrBotEnum':
            try:
                inst = cls
                inst.__init__(value)
                return cls(_value_int)
            except ValueError:
                raise_can_not_parse()

        if isinstance(value, HrBotEnum):
            return value
        if isinstance(value, int):
            return enum_from_int(value)
        if isinstance(value, str):
            try:
                value_int = int(value)
                if value == str(value_int):
                    return enum_from_int(value_int)
            except ValueError:
                pass

            try:
                return cls[value.upper()]
            except KeyError:
                raise_can_not_parse()

        raise_can_not_parse()

    @classmethod
    def _exclusive_groups(cls) -> List[Set['HrBotEnum']]:
        return []

    @classmethod
    def add_new_item(cls, items: List['HrBotEnum'], new_item: 'HrBotEnum', unique: bool = True) -> List['HrBotEnum']:
        to_remove = set()
        for exclusive_group in cls._exclusive_groups():
            if new_item in exclusive_group:
                to_remove.update(i for i in exclusive_group if i != new_item)
        result = [item for item in items + [new_item] if item not in to_remove]
        if unique:
            return list(set(result))
        return result

    @classmethod
    def remove_item(cls, items: List['HrBotEnum'], to_remove: 'HrBotEnum', unique: bool = True) -> List['HrBotEnum']:
        result = [item for item in items if item != to_remove]
        if unique:
            return list(set(result))
        return result

    def __lt__(self, other):
        return self.value < other.value


class JobsBotModel(ABC):
    NOT_COMPARABLE_FIELD_NAMES = set()
    DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

    @classmethod
    def _parse_datetime(cls, value: Any) -> Optional[datetime]:
        if value is None:
            return None
        if isinstance(value, datetime):
            return value
        try:
            return datetime.strptime(value, cls.DATE_FORMAT)
        except Exception:
            raise ValueError(f"Can not parse datetime {value}") from None

    @classmethod
    def _datetime_to_str(cls, d: datetime) -> str:
        return d.strftime(cls.DATE_FORMAT)

    @classmethod
    def _parse_enum(cls, enum_class: Type[HrBotEnum], value: Any, null_if_can_not_parse: bool = False,
                    default_value_if_can_not_parse: HrBotEnum = None) \
            -> Optional[HrBotEnum]:
        if null_if_can_not_parse and default_value_if_can_not_parse is not None:
            raise ValueError("Parameters null_if_can_not_parse and default_value_if_can_not_parse "
                             "are mutually exclusive")
        try:
            return enum_class.parse(value)
        except ValueError:
            if null_if_can_not_parse:
                return None
            elif default_value_if_can_not_parse is not None:
                return default_value_if_can_not_parse
            raise

    @abstractmethod
    def as_dict(self) -> Dict[str, Any]:
        pass

    @classmethod
    def _jsonify_value(cls, v) -> Any:
        if isinstance(v, JobsBotModel):
            return cls._jsonify_value(v.as_dict())
        elif isinstance(v, HrBotEnum):
            return v.value
        elif isinstance(v, dict):
            return {k: cls._jsonify_value(v) for k, v in v.items()}
        elif isinstance(v, list) or isinstance(v, tuple):
            return [cls._jsonify_value(item) for item in v]
        elif isinstance(v, datetime):
            return cls._datetime_to_str(v)
        elif isinstance(v, bytes):
            return "<bytes>"
        elif v is None or isinstance(v, str) or isinstance(v, int) or isinstance(v, float):
            return v
        raise ValueError(f"Impossible to jsonify a value of type {type(v)}")

    def as_json_dict(self) -> Dict[str, Any]:
        return self._jsonify_value(self.as_dict())

    @classmethod
    def _bsonify_value(cls, v) -> Any:
        if isinstance(v, JobsBotModel):
            return cls._bsonify_value(v.as_dict())
        elif isinstance(v, HrBotEnum):
            return v.value
        elif isinstance(v, dict):
            return {k: cls._bsonify_value(v) for k, v in v.items()}
        elif isinstance(v, list) or isinstance(v, tuple):
            return [cls._bsonify_value(item) for item in v]
        elif isinstance(v, bytes):
            return v
        elif v is None or isinstance(v, str) or isinstance(v, int) or isinstance(v, float) or isinstance(v, datetime):
            return v
        raise ValueError(f"Impossible to bsonify a value of type {type(v)}")

    def as_bson_dict(self) -> Dict[str, Any]:
        return self._bsonify_value(self.as_dict())

    def as_bytes(self) -> bytes:
        return pickle.dumps(self.as_bson_dict())

    @classmethod
    @abstractmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'JobsBotModel':
        pass

    @classmethod
    def from_bytes(cls, b: bytes) -> 'JobsBotModel':
        return cls.from_dict(pickle.loads(b))

    def __str__(self):
        return str(self.as_json_dict())

    def __repr__(self):
        return str(self.as_json_dict())

    def __eq__(self, other):
        self_as_dict = self.as_dict()
        other_as_dict = other.as_dict()
        for key in set(self_as_dict.keys()).union(other_as_dict.keys()):
            if key in self.NOT_COMPARABLE_FIELD_NAMES:
                continue

            if key not in self_as_dict or key not in other_as_dict:
                return False

            if type(self_as_dict[key]) == list:
                if type(other_as_dict[key]) != list:
                    return False

                if len(self_as_dict[key]) != len(other_as_dict[key]):
                    return False

                try:
                    if sorted(self_as_dict[key]) != sorted(other_as_dict[key]):
                        return False
                except TypeError:
                    # Not comparable => not sortable
                    for pos in range(len(self_as_dict[key])):
                        if self_as_dict[key][pos] != other_as_dict[key][pos]:
                            return False

            else:
                if isinstance(self_as_dict[key], datetime):
                    if not isinstance(other_as_dict[key], datetime):
                        return False
                    if self_as_dict[key].strftime("%Y-%m-%d %H:%M:%S") != \
                            other_as_dict[key].strftime("%Y-%m-%d %H:%M:%S"):
                        return False

                elif self_as_dict[key] != other_as_dict[key]:
                    return False

        return True

    def __ne__(self, other):
        return not self.__eq__(other)
