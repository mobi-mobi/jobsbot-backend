from typing import Dict, List, Set

from .interfaces import HrBotEnum

__all__ = [
    "CountryLocations",
    "JobCategory",
    "JobPosition",
    "UserProfileRequiredPart",
    "UserProfileValidationStatus",
    "UserProfileRejectionReason",
    "UserCvStatus",
    "UserJobMatchSource",
    "UserJobMatchType",
    "UserJobApplicationStatus",
    "UserJobApplicationMethod",
    "UserExperience",
    "UserSkill",
    "SkillsCategory",
    "UserPreferences",
    "UserDesiredMinAnnualGrossSalary",
    "UserLanguages",
    "UserCountry",
    "UserNotificationOption",
    "ApiPermission",
    "AdminPermission",
    "RecruiterPermission",
    "JobPostingStatus",
    "JobPostingValidationStatus"
]


class UserLanguages(HrBotEnum):
    ENGLISH = 1
    GERMAN = 2
    FRENCH = 3

    @classmethod
    def _titles(cls) -> Dict['UserLanguages', str]:
        return {
            UserLanguages.ENGLISH: "English",
            UserLanguages.GERMAN: "German",
            UserLanguages.FRENCH: "French"
        }


class UserCountry(HrBotEnum):
    FRANCE = 1
    GERMANY = 2
    IRELAND = 7
    ITALY = 8
    NETHERLANDS = 6
    PORTUGAL = 4
    SPAIN = 5
    UK = 3

    OTHER_EU_COUNTRY = 1000
    OTHER_NON_EU_COUNTRY = 1001

    @classmethod
    def _titles(cls) -> Dict['UserCountry', str]:
        return {
            UserCountry.FRANCE: "France",
            UserCountry.GERMANY: "Germany",
            UserCountry.IRELAND: "Ireland",
            UserCountry.ITALY: "Italy",
            UserCountry.NETHERLANDS: "Netherlands",
            UserCountry.PORTUGAL: "Portugal",
            UserCountry.SPAIN: "Spain",
            UserCountry.UK: "United Kingdom",
            UserCountry.OTHER_EU_COUNTRY: "Other EU Country",
            UserCountry.OTHER_NON_EU_COUNTRY: "Other Non-EU Country"
        }


class CountryLocations(HrBotEnum):
    PARIS = 1
    LYON = 2
    MARSEILLE = 3
    TOULOUSE = 4
    LILLE = 5
    BORDEAUX = 6
    NICE = 7
    NANTES = 8
    GRENOBLE = 9
    MONTPELLIER = 10
    SOPHIA_ANTIPOLIS = 11

    BERLIN = 101
    HAMBURG = 102
    MUNICH = 103
    COLOGNE = 104
    FRANKFURT = 105

    DUBLIN = 601
    CORK = 602

    ROME = 701
    MILAN = 702
    NAPLES = 703
    TURIN = 704
    PALERMO = 705
    GENOA = 706

    AMSTERDAM = 501
    ROTTERDAM = 502
    THE_HAUGE = 503
    UTRECHT = 504

    LISBON = 301
    PORTO = 302
    AMADORA = 303
    BRAGA = 304
    SETUBAL = 305

    MADRID = 401
    BARCELONA = 402
    VALENCIA = 403
    SEVILLE = 404
    ZARAGOZA = 405
    MALAGA = 406
    MURCIA = 407
    PALMA_SPAIN = 408
    LAS_PALMAS = 409
    BILBAO = 410

    LONDON = 201
    MANCHESTER = 202
    OXFORD = 203
    CAMBRIDGE = 204
    GLASGOW = 205

    @classmethod
    def _titles(cls) -> Dict['CountryLocations', str]:
        return {
            CountryLocations.PARIS: "Paris",
            CountryLocations.LYON: "Lyon",
            CountryLocations.MARSEILLE: "Marseille",
            CountryLocations.TOULOUSE: "Toulouse",
            CountryLocations.LILLE: "Lille",
            CountryLocations.BORDEAUX: "Bordeaux",
            CountryLocations.NICE: "Nice",
            CountryLocations.NANTES: "Nantes",
            CountryLocations.GRENOBLE: "Grenoble",
            CountryLocations.MONTPELLIER: "Montpellier",
            CountryLocations.SOPHIA_ANTIPOLIS: "Sophia Antipolis",

            CountryLocations.BERLIN: "Berlin",
            CountryLocations.HAMBURG: "Hamburg",
            CountryLocations.MUNICH: "Munich",
            CountryLocations.COLOGNE: "Cologne",
            CountryLocations.FRANKFURT: "Frankfurt am Main",

            CountryLocations.DUBLIN: "Dublin",
            CountryLocations.CORK: "Cork",

            CountryLocations.ROME: "Rome",
            CountryLocations.MILAN: "Milan",
            CountryLocations.NAPLES: "Naples",
            CountryLocations.TURIN: "Turin",
            CountryLocations.PALERMO: "Palermo",
            CountryLocations.GENOA: "Genoa",

            CountryLocations.AMSTERDAM: "Amsterdam",
            CountryLocations.ROTTERDAM: "Rotterdam",
            CountryLocations.THE_HAUGE: "The Hauge",
            CountryLocations.UTRECHT: "Utrecht",

            CountryLocations.LISBON: "Lisbon",
            CountryLocations.PORTO: "Porto",
            CountryLocations.AMADORA: "Amadora",
            CountryLocations.BRAGA: "Braga",
            CountryLocations.SETUBAL: "Setúbal",

            CountryLocations.MADRID: "Madrid",
            CountryLocations.BARCELONA: "Barcelona",
            CountryLocations.VALENCIA: "Valencia",
            CountryLocations.SEVILLE: "Seville",
            CountryLocations.ZARAGOZA: "Zaragoza",
            CountryLocations.MALAGA: "Málaga",
            CountryLocations.MURCIA: "Murcia",
            CountryLocations.PALMA_SPAIN: "Palma",
            CountryLocations.LAS_PALMAS: "Las Palmas de Gran Canaria",
            CountryLocations.BILBAO: "Bilbao",

            CountryLocations.LONDON: "London",
            CountryLocations.MANCHESTER: "Manchester",
            CountryLocations.OXFORD: "Oxford",
            CountryLocations.CAMBRIDGE: "Cambridge",
            CountryLocations.GLASGOW: "Glasgow"
        }


class JobCategory(HrBotEnum):
    SOFTWARE_ENGINEERING = 1
    SRE_INFRA = 2
    MANAGEMENT = 3
    LEADERSHIP = 4
    DATA_AND_AI = 5
    DESIGN = 6
    BUSINESS_OPS = 7
    QA = 8
    SECURITY = 9
    PEOPLE_OPS = 10
    SALES = 11

    @classmethod
    def _titles(cls) -> Dict['JobCategory', str]:
        return {
            JobCategory.SOFTWARE_ENGINEERING: "Software Engineering",
            JobCategory.SRE_INFRA: "SRE / Infrastructure",
            JobCategory.MANAGEMENT: "Management",
            JobCategory.LEADERSHIP: "Leadership",
            JobCategory.DATA_AND_AI: "Data and A.I.",
            JobCategory.DESIGN: "Design",
            JobCategory.BUSINESS_OPS: "Business Ops",
            JobCategory.QA: "QA",
            JobCategory.SECURITY: "Security",
            JobCategory.PEOPLE_OPS: "People Ops",
            JobCategory.SALES: "Sales"
        }


class JobPosition(HrBotEnum):
    # JobCategories.SOFTWARE_ENGINEER
    SWE_BACKEND = 1
    SWE_FRONTEND = 2
    SWE_FULLSTACK = 3
    SWE_ML = 4
    SWE_IOS = 5
    SWE_ANDROID = 6
    SWE_ARCHITECT = 7
    SWE_DATA = 8

    # JobCategories.SRE_INFRA
    SRE_INFRA_INFRASTRUCTURE_ENG = 101
    SRE_INFRA_DEVOPS_ENG = 102

    # JobCategories.MANAGEMENT
    MANAGEMENT_PROJECT = 201
    MANAGEMENT_PROGRAM = 202
    MANAGEMENT_PRODUCT = 203
    MANAGEMENT_TECHNICAL_PROGRAM = 204
    MANAGEMENT_TECHNICAL_PRODUCT = 205

    # JobCategories.LEADERSHIP
    LEADERSHIP_TEAM_LEAD = 301
    LEADERSHIP_EMG = 302
    LEADERSHIP_VP = 303
    LEADERSHIP_CTO = 304
    LEADERSHIP_CPO = 305

    # JobCategories.DATA_AND_AI
    DATA_SCIENTIST = 401
    DATA_ANALYST = 402

    # JobCategories.DESIGN
    DESIGN_UX = 501
    DESIGN_UI = 502
    DESIGN_GRAPHIC = 503

    # JobsCategories.BUSINESS_OPS
    BUSINESS_ANALYST = 601

    # JobsCategories.QA
    QA_MANUAL = 701
    QU_AUTO = 702

    # JobsCategories.SECURITY
    SECURITY_ANALYST = 801
    SECURITY_ENGINEER = 802

    # JobsCategories.PEOPLE_OPS
    PEOPLE_OPS_HR = 901
    PEOPLE_OPS_RECRUITER = 902
    PEOPLE_HEAD_OF_HR = 903
    PEOPLE_HEAD_OF_RECRUITMENT = 904

    # JobCategory.SALES
    SALES_ACCOUNT_EXECUTIVE = 1001
    SALES_ACCOUNT_MANAGER = 1002
    SALES_ENGINEER = 1003
    SALES_SOLUTIONS_ARCHITECT = 1004
    SALES_DIRECTOR_OF_SALES = 1005
    SALES_VP_VP_OF_SALES = 1006

    @classmethod
    def _titles(cls) -> Dict['JobPosition', str]:
        return {
            JobPosition.SWE_BACKEND: "Backend Engineer",
            JobPosition.SWE_FRONTEND: "Frontend Engineer",
            JobPosition.SWE_FULLSTACK: "Fullstack Engineer",
            JobPosition.SWE_ML: "ML Engineer",
            JobPosition.SWE_IOS: "Mobile (iOS) Engineer",
            JobPosition.SWE_ANDROID: "Mobile (Android) Engineer",
            JobPosition.SWE_ARCHITECT: "Software Architect",
            JobPosition.SWE_DATA: "Data Engineer",

            JobPosition.SRE_INFRA_INFRASTRUCTURE_ENG: "Infrastructure Engineer",
            JobPosition.SRE_INFRA_DEVOPS_ENG: "DevOps Engineer",

            JobPosition.MANAGEMENT_PROJECT: "Project Manager",
            JobPosition.MANAGEMENT_PROGRAM: "Program Manager",
            JobPosition.MANAGEMENT_PRODUCT: "Product Manager",
            JobPosition.MANAGEMENT_TECHNICAL_PROGRAM: "Technical Program Manager",
            JobPosition.MANAGEMENT_TECHNICAL_PRODUCT: "Technical Product Manager",

            JobPosition.LEADERSHIP_TEAM_LEAD: "Team Lead",
            JobPosition.LEADERSHIP_EMG: "Engineering Manager",
            JobPosition.LEADERSHIP_VP: "VP",
            JobPosition.LEADERSHIP_CPO: "CPO",
            JobPosition.LEADERSHIP_CTO: "CTO",

            JobPosition.DATA_ANALYST: "Data Analyst",
            JobPosition.DATA_SCIENTIST: "Data Scientist",

            JobPosition.DESIGN_UI: "UI Designer",
            JobPosition.DESIGN_UX: "UX Designer",
            JobPosition.DESIGN_GRAPHIC: "Graphic Designer",

            JobPosition.BUSINESS_ANALYST: "Business Analyst",

            JobPosition.QA_MANUAL: "Manual QA",
            JobPosition.QU_AUTO: "Auto QA",

            JobPosition.SECURITY_ANALYST: "Security Analyst",
            JobPosition.SECURITY_ENGINEER: "Security Engineer",

            JobPosition.PEOPLE_OPS_HR: "HR",
            JobPosition.PEOPLE_OPS_RECRUITER: "Recruiter",
            JobPosition.PEOPLE_HEAD_OF_HR: "Head of HR",
            JobPosition.PEOPLE_HEAD_OF_RECRUITMENT: "Head of Recruitment",

            JobPosition.SALES_VP_VP_OF_SALES: "VP of Sales",
            JobPosition.SALES_DIRECTOR_OF_SALES: "Director of Sales",
            JobPosition.SALES_ACCOUNT_EXECUTIVE: "Account Executive",
            JobPosition.SALES_ACCOUNT_MANAGER: "Account Manager",
            JobPosition.SALES_ENGINEER: "Sales Engineer",
            JobPosition.SALES_SOLUTIONS_ARCHITECT: "Solutions Architect",
        }


class SkillsCategory(HrBotEnum):
    PROGRAMMING_LANGUAGES = 1
    FRAMEWORKS = 2
    SRE_INFRA = 3
    DATABASES = 4
    DATA_AND_ML = 5
    SOFT_SKILLS = 6
    ARCHITECTURE = 7

    @classmethod
    def _titles(cls) -> Dict['SkillsCategory', str]:
        return {
            SkillsCategory.PROGRAMMING_LANGUAGES: "Programming Languages",
            SkillsCategory.FRAMEWORKS: "Frameworks",
            SkillsCategory.SRE_INFRA: "SRE / Infra",
            SkillsCategory.DATABASES: "Databases",
            SkillsCategory.DATA_AND_ML: "Data and ML/AI",
            SkillsCategory.SOFT_SKILLS: "Soft Skills",
            SkillsCategory.ARCHITECTURE: "Architecture"
        }


class UserSkill(HrBotEnum):
    SWE_JAVA = 1
    SWE_PYTHON = 2
    SWE_JAVASCRIPT = 3
    SWE_NODE_JS = 4
    SWE_C_CPP = 5
    SWE_KOTLIN = 6
    SWE_C_SHARP = 7
    SWE_PHP = 8
    SWE_GOLANG = 9
    SWE_SCALA = 10
    SWE_RUST = 11
    SWE_SWIFT = 12,

    FRAMEWORKS_REACT = 101
    FRAMEWORKS_ANGULAR = 102
    FRAMEWORKS_RUBY_ON_RAILS = 103
    FRAMEWORKS_DJANGO = 104
    FRAMEWORKS_VUE_JS = 105
    FRAMEWORKS_DRUPAL = 106

    SRE_INFRA_NETWORKS = 201
    SRE_INFRA_HARDWARE = 202
    SRE_INFRA_CI_CD = 203
    SRE_INFRA_DATABASES_ADMINISTRATION = 204
    SRE_INFRA_MONITORING = 205
    SRE_INFRA_DATA_PIPELINES = 206
    SRE_INFRA_DATA_LAKES = 207
    SRE_INFRA_DOCKER = 208
    SRE_INFRA_KUBERNETES = 209

    DATABASES_SQL_DATABASES = 301
    DATABASES_NOSQL_DATABASES = 302
    DATABASES_IN_MEMORY_DATABASES = 303

    DATA_AND_ML_TENSORFLOW = 401
    DATA_AND_ML_PYTORCH = 402
    DATA_AND_ML_KERAS = 403

    SOFT_SKILLS_MENTORING = 501
    SOFT_SKILLS_PEOPLE_MANAGEMENT = 502
    SOFT_SKILLS_AGILE = 503
    SOFT_SKILLS_SCRUM = 504
    SOFT_SKILLS_COUCHING = 505

    ARCHITECTURE_CLOUD_SOLUTIONS = 601
    ARCHITECTURE_HIGH_LOAD_SOLUTIONS = 602
    ARCHITECTURE_BIG_DATA_SOLUTIONS = 603
    ARCHITECTURE_GPU_SOLUTIONS = 604

    @classmethod
    def _titles(cls) -> Dict['UserSkill', str]:
        return {
            UserSkill.SWE_JAVA: "Java",
            UserSkill.SWE_PYTHON: "Python",
            UserSkill.SWE_JAVASCRIPT: "JavaScript",
            UserSkill.SWE_NODE_JS: "Node.js",
            UserSkill.SWE_C_CPP: "C/C++",
            UserSkill.SWE_KOTLIN: "Kotlin",
            UserSkill.SWE_C_SHARP: "C#",
            UserSkill.SWE_PHP: "PHP",
            UserSkill.SWE_GOLANG: "Go",
            UserSkill.SWE_SCALA: "Scala",
            UserSkill.SWE_RUST: "Rust",
            UserSkill.SWE_SWIFT: "Swift",

            UserSkill.FRAMEWORKS_REACT: "React",
            UserSkill.FRAMEWORKS_ANGULAR: "Angular",
            UserSkill.FRAMEWORKS_RUBY_ON_RAILS: "Ruby On Rails",
            UserSkill.FRAMEWORKS_DJANGO: "Django",
            UserSkill.FRAMEWORKS_VUE_JS: "Vue.js",
            UserSkill.FRAMEWORKS_DRUPAL: "Drupal",

            UserSkill.SRE_INFRA_NETWORKS: "Networks",
            UserSkill.SRE_INFRA_HARDWARE: "Hardware",
            UserSkill.SRE_INFRA_CI_CD: "CI/CD Pipelines",
            UserSkill.SRE_INFRA_DATABASES_ADMINISTRATION: "Databases Administration",
            UserSkill.SRE_INFRA_MONITORING: "Monitoring",
            UserSkill.SRE_INFRA_DATA_PIPELINES: "Data Pipelines",
            UserSkill.SRE_INFRA_DATA_LAKES: "Data Lakes",
            UserSkill.SRE_INFRA_DOCKER: "Docker",
            UserSkill.SRE_INFRA_KUBERNETES: "Kubernetes",

            UserSkill.DATABASES_SQL_DATABASES: "SQL Databases",
            UserSkill.DATABASES_NOSQL_DATABASES: "NoSQL Databases",
            UserSkill.DATABASES_IN_MEMORY_DATABASES: "In-memory Databases",

            UserSkill.DATA_AND_ML_TENSORFLOW: "TensorFlow",
            UserSkill.DATA_AND_ML_PYTORCH: "PyTorch",
            UserSkill.DATA_AND_ML_KERAS: "Keras",

            UserSkill.SOFT_SKILLS_MENTORING: "Mentoring",
            UserSkill.SOFT_SKILLS_PEOPLE_MANAGEMENT: "People Management",
            UserSkill.SOFT_SKILLS_AGILE: "Agile",
            UserSkill.SOFT_SKILLS_SCRUM: "Scrum",
            UserSkill.SOFT_SKILLS_COUCHING: "Couching",

            UserSkill.ARCHITECTURE_CLOUD_SOLUTIONS: "Cloud Solutions (AWS/GCP/...)",
            UserSkill.ARCHITECTURE_HIGH_LOAD_SOLUTIONS: "High Load Solutions",
            UserSkill.ARCHITECTURE_BIG_DATA_SOLUTIONS: "Big Data Solutions",
            UserSkill.ARCHITECTURE_GPU_SOLUTIONS: "GPU Solutions"
        }


class UserProfileValidationStatus(HrBotEnum):
    NOT_READY = 1
    WAITING_VERIFICATION = 2
    VERIFIED = 3
    REJECTED = 4

    @classmethod
    def _titles(cls) -> Dict['UserProfileValidationStatus', str]:
        return {
            UserProfileValidationStatus.NOT_READY: "Not Ready",
            UserProfileValidationStatus.WAITING_VERIFICATION: "Waiting Verification",
            UserProfileValidationStatus.VERIFIED: "Verified",
            UserProfileValidationStatus.REJECTED: "Rejected"
        }


class UserProfileRejectionReason(HrBotEnum):
    WRONG_LOCATION_IN_CV = 1
    WRONG_NAME_IN_CV = 2
    CV_IS_NOT_READABLE = 3
    BAD_CONTENT_IN_CV = 4
    WRONG_CURRENT_POSITION_TITLE = 5
    WRONG_CURRENT_COMPANY_NAME = 6

    @classmethod
    def _titles(cls) -> Dict['UserProfileRejectionReason', str]:
        return {
            UserProfileRejectionReason.WRONG_LOCATION_IN_CV: "Location in CV does not match profile location",
            UserProfileRejectionReason.WRONG_NAME_IN_CV: "Name in CV does not match profile name",
            UserProfileRejectionReason.CV_IS_NOT_READABLE: "CV is not readable (i.e., wrong format)",
            UserProfileRejectionReason.BAD_CONTENT_IN_CV: "Content in CV looks wrong",
            UserProfileRejectionReason.WRONG_CURRENT_POSITION_TITLE: "Current position title is wrong",
            UserProfileRejectionReason.WRONG_CURRENT_COMPANY_NAME: "Current company name is wrong"
        }


class UserProfileRequiredPart(HrBotEnum):
    CV = 1
    NAME = 2
    EMAIL = 3
    LOCATION = 4
    EXPERIENCE = 5
    CURRENT_POSITION_TITLE = 6
    CURRENT_COMPANY_NAME = 7

    @classmethod
    def _titles(cls) -> Dict['UserProfileRequiredPart', str]:
        return {
            UserProfileRequiredPart.CV: "CV",
            UserProfileRequiredPart.NAME: "Name",
            UserProfileRequiredPart.EMAIL: "Email",
            UserProfileRequiredPart.LOCATION: "Location",
            UserProfileRequiredPart.EXPERIENCE: "Experience",
            UserProfileRequiredPart.CURRENT_POSITION_TITLE: "Current Position Title",
            UserProfileRequiredPart.CURRENT_COMPANY_NAME: "Current Company Name"
        }


class UserCvStatus(HrBotEnum):
    NOT_UPLOADED = 1
    NOT_VERIFIED = 2
    VERIFIED = 3
    REJECTED = 4

    @classmethod
    def _titles(cls) -> Dict['UserCvStatus', str]:
        return {
            UserCvStatus.NOT_UPLOADED: "Not Uploaded",
            UserCvStatus.NOT_VERIFIED: "Not Verified",
            UserCvStatus.VERIFIED: "Verified",
            UserCvStatus.REJECTED: "Rejected"
        }


class UserJobMatchSource(HrBotEnum):
    JOB_MATCH = 1
    SUBSCRIPTION = 2

    @classmethod
    def _titles(cls) -> Dict['UserJobMatchSource', str]:
        return {
            UserJobMatchSource.JOB_MATCH: "Job Match",
            UserJobMatchSource.SUBSCRIPTION: "Subscription"
        }


class UserJobMatchType(HrBotEnum):
    NO_MATCH = 1
    PARTIAL_MATCH = 2
    FULL_MATCH = 3

    @classmethod
    def _titles(cls) -> Dict['UserJobMatchType', str]:
        return {
            UserJobMatchType.NO_MATCH: "No Match",
            UserJobMatchType.PARTIAL_MATCH: "Partial Match",
            UserJobMatchType.FULL_MATCH: "Full Match",
        }


class UserJobApplicationStatus(HrBotEnum):
    NOT_REVIEWED = 1
    ACCEPTED = 2
    CANCELLED_BY_USER = 3
    REJECTED_BY_HRBOT = 4
    REJECTED_BY_COMPANY = 5

    @classmethod
    def _titles(cls) -> Dict['UserJobApplicationStatus', str]:
        return {
            UserJobApplicationStatus.NOT_REVIEWED: "Not reviewed",
            UserJobApplicationStatus.ACCEPTED: "Accepted",
            UserJobApplicationStatus.CANCELLED_BY_USER: "Cancelled by User",
            UserJobApplicationStatus.REJECTED_BY_HRBOT: "Rejected by HrBot",
            UserJobApplicationStatus.REJECTED_BY_COMPANY: "Rejected by Company"
        }


class UserJobApplicationMethod(HrBotEnum):
    ONE_CLICK = 1
    URL_REDIRECT = 2

    @classmethod
    def _titles(cls) -> Dict['UserJobApplicationMethod', str]:
        return {
            UserJobApplicationMethod.ONE_CLICK: "One Click",
            UserJobApplicationMethod.URL_REDIRECT: "Url Redirect",
        }


class UserExperience(HrBotEnum):
    Y_0 = 0
    Y_1 = 1
    Y_2 = 2
    Y_3 = 3
    Y_4 = 4
    Y_5 = 5
    Y_6 = 6
    Y_7 = 7
    Y_8 = 8
    Y_9 = 9
    Y_10_PLUS = 10

    @classmethod
    def _titles(cls) -> Dict['UserExperience', str]:
        return {
            UserExperience.Y_0: "0 years",
            UserExperience.Y_1: "1 year",
            UserExperience.Y_2: "2 years",
            UserExperience.Y_3: "3 years",
            UserExperience.Y_4: "4 years",
            UserExperience.Y_5: "5 years",
            UserExperience.Y_6: "6 years",
            UserExperience.Y_7: "7 years",
            UserExperience.Y_8: "8 years",
            UserExperience.Y_9: "9 years",
            UserExperience.Y_10_PLUS: "10+ years"
        }


class UserDesiredMinAnnualGrossSalary(HrBotEnum):
    NO_PREFERENCES = 0
    FROM_30 = 30
    FROM_40 = 40
    FROM_50 = 50
    FROM_60 = 60
    FROM_70 = 70
    FROM_80 = 80
    FROM_90 = 90
    FROM_100 = 100
    FROM_110 = 110
    FROM_130 = 130

    @classmethod
    def _titles(cls) -> Dict['UserDesiredMinAnnualGrossSalary', str]:
        return {
            UserDesiredMinAnnualGrossSalary.NO_PREFERENCES: "No preferences",
            UserDesiredMinAnnualGrossSalary.FROM_30: "30K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_40: "40K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_50: "50K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_60: "60K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_70: "70K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_80: "80K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_90: "90K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_100: "100K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_110: "110K+ EUR",
            UserDesiredMinAnnualGrossSalary.FROM_130: "130K+ EUR",
        }

    @classmethod
    def _exclusive_groups(cls) -> List[Set['HrBotEnum']]:
        # As all salaries are mutually exclusive
        return [
            {salary for salary in UserDesiredMinAnnualGrossSalary}
        ]


class UserPreferences(HrBotEnum):
    READY_FOR_REMOTE = 14
    READY_FOR_HYBRID = 15
    READY_FOR_OFFICE = 16
    NO_AGENCY = 1
    NO_CONSULTING = 2
    NO_STARTUPS = 3
    NO_TEMPORARY = 4
    ONLY_INTERNSHIP = 5
    NO_INTERNSHIP = 6
    WITH_STOCKS = 7
    WITH_CONFERENCES = 8
    RELOCATION = 13

    @classmethod
    def _titles(cls) -> Dict['UserPreferences', str]:
        return {
            UserPreferences.NO_AGENCY: "No agencies, please",
            UserPreferences.NO_CONSULTING: "No consulting, please",
            UserPreferences.NO_STARTUPS: "No startups, please",
            UserPreferences.NO_TEMPORARY: "No temporary contracts, please",
            UserPreferences.ONLY_INTERNSHIP: "Only internship, please",
            UserPreferences.NO_INTERNSHIP: "No internship, please",
            UserPreferences.WITH_STOCKS: "Only with RSU/Stocks plan, please",
            UserPreferences.WITH_CONFERENCES: "Only with Conferences budget, please",
            UserPreferences.READY_FOR_REMOTE: "Ready for fully remote",
            UserPreferences.READY_FOR_HYBRID: "Ready for partially remote",
            UserPreferences.READY_FOR_OFFICE: "Ready for non-remote work",
            UserPreferences.RELOCATION: "Only where relocation is possible, please",
        }

    @classmethod
    def _exclusive_groups(cls) -> List[Set['UserPreferences']]:
        return [
            {
                UserPreferences.ONLY_INTERNSHIP,
                UserPreferences.NO_INTERNSHIP
            }
        ]


class UserNotificationOption(HrBotEnum):
    MATCHED_JOBS_DAILY = 1
    MATCHED_JOBS_WEEKLY = 2

    FOLLOWED_COMPANIES_JOBS_DAILY = 10
    FOLLOWED_COMPANIES_JOBS_WEEKLY = 11

    BOT_FRIENDLY_REMINDERS = 21
    BOT_NEWS = 22

    @classmethod
    def _titles(cls) -> Dict['UserNotificationOption', str]:
        return {
            UserNotificationOption.MATCHED_JOBS_DAILY: "Recommended Jobs: daily",
            UserNotificationOption.MATCHED_JOBS_WEEKLY: "Recommended Jobs: weekly",
            UserNotificationOption.FOLLOWED_COMPANIES_JOBS_DAILY: "Followed Companies: daily",
            UserNotificationOption.FOLLOWED_COMPANIES_JOBS_WEEKLY: "Followed Companies: weekly",
            UserNotificationOption.BOT_FRIENDLY_REMINDERS: "Friendly Reminders from JobsBot.io",
            UserNotificationOption.BOT_NEWS: "News from JobsBot.io"
        }


class ApiPermission(HrBotEnum):
    pass


class AdminPermission(ApiPermission):
    MANAGE_ADMINS = 1

    VIEW_CANDIDATES = 11
    VALIDATE_CANDIDATES = 12

    VIEW_COMPANIES = 21
    VALIDATE_COMPANIES = 22
    DELETE_COMPANIES = 23

    CREATE_COMPANIES = 31

    CREATE_JOB = 41
    VIEW_JOBS = 42
    VALIDATE_JOBS = 43

    CREATE_RECRUITER = 51
    VIEW_RECRUITERS = 52
    DELETE_RECRUITER = 53

    @classmethod
    def _titles(cls) -> Dict['AdminPermission', str]:
        return {
            AdminPermission.MANAGE_ADMINS: "Manage Admins",
            AdminPermission.VIEW_CANDIDATES: "View Candidates",
            AdminPermission.VALIDATE_CANDIDATES: "Validate Candidates",
            AdminPermission.VIEW_COMPANIES: "View Companies",
            AdminPermission.VALIDATE_COMPANIES: "Validate Companies",
            AdminPermission.DELETE_COMPANIES: "Delete Companies",
            AdminPermission.CREATE_COMPANIES: "Create Companies",
            AdminPermission.CREATE_JOB: "Create Jobs",
            AdminPermission.VIEW_JOBS: "View Jobs",
            AdminPermission.VALIDATE_JOBS: "Validate Jobs",
            AdminPermission.CREATE_RECRUITER: "Create Recruiters",
            AdminPermission.VIEW_RECRUITERS: "View Recruiters",
            AdminPermission.DELETE_RECRUITER: "Delete Recruiters"
        }


class RecruiterPermission(ApiPermission):
    MANAGE_RECRUITERS = 1

    VIEW_ALL_JOBS = 10
    CREATE_JOBS = 11

    MODIFY_ALL_JOBS = 21

    @classmethod
    def _titles(cls) -> Dict['RecruiterPermission', str]:
        return {
            RecruiterPermission.MANAGE_RECRUITERS: "Manage Recruiters",
            RecruiterPermission.VIEW_ALL_JOBS: "View All Jobs",
            RecruiterPermission.CREATE_JOBS: "Create Jobs",
            RecruiterPermission.MODIFY_ALL_JOBS: "Modify All Jobs"
        }


class JobPostingStatus(HrBotEnum):
    ACTIVE = 1
    ARCHIVED = 10

    @classmethod
    def _titles(cls) -> Dict['JobPostingStatus', str]:
        return {
            JobPostingStatus.ACTIVE: "Active",
            JobPostingStatus.ARCHIVED: "Archived"
        }


class JobPostingValidationStatus(HrBotEnum):
    NOT_READY = 1
    WAITING_VERIFICATION = 2
    VERIFIED = 3
    REJECTED = 4

    @classmethod
    def _titles(cls) -> Dict['JobPostingValidationStatus', str]:
        return {
            JobPostingValidationStatus.NOT_READY: "Not Ready",
            JobPostingValidationStatus.WAITING_VERIFICATION: "Waiting Verification",
            JobPostingValidationStatus.VERIFIED: "Verified",
            JobPostingValidationStatus.REJECTED: "Rejected"
        }
