from collections import defaultdict
from typing import Dict, List, Optional

from jobsbot.core import CountryLocations, JobCategory, JobPosition, SkillsCategory, UserSkill
from jobsbot.core.enums import UserCountry

COUNTRY_COUNTRY_LOCATIONS_MAP: Dict[UserCountry, List[CountryLocations]] = {
    UserCountry.FRANCE: [
        CountryLocations.PARIS,
        CountryLocations.LYON,
        CountryLocations.MARSEILLE,
        CountryLocations.TOULOUSE,
        CountryLocations.LILLE,
        CountryLocations.BORDEAUX,
        CountryLocations.NICE,
        CountryLocations.NANTES,
        CountryLocations.GRENOBLE,
        CountryLocations.MONTPELLIER,
        CountryLocations.SOPHIA_ANTIPOLIS
    ],

    UserCountry.GERMANY: [
        CountryLocations.BERLIN,
        CountryLocations.HAMBURG,
        CountryLocations.MUNICH,
        CountryLocations.COLOGNE,
        CountryLocations.FRANKFURT
    ],

    UserCountry.IRELAND: [
        CountryLocations.DUBLIN,
        CountryLocations.CORK
    ],

    UserCountry.ITALY: [
        CountryLocations.ROME,
        CountryLocations.MILAN,
        CountryLocations.NAPLES,
        CountryLocations.TURIN,
        CountryLocations.PALERMO,
        CountryLocations.GENOA,
    ],

    UserCountry.NETHERLANDS: [
        CountryLocations.AMSTERDAM,
        CountryLocations.ROTTERDAM,
        CountryLocations.THE_HAUGE,
        CountryLocations.UTRECHT
    ],

    UserCountry.PORTUGAL: [
        CountryLocations.LISBON,
        CountryLocations.PORTO,
        CountryLocations.AMADORA,
        CountryLocations.BRAGA,
        CountryLocations.SETUBAL
    ],

    UserCountry.SPAIN: [
        CountryLocations.MADRID,
        CountryLocations.BARCELONA,
        CountryLocations.VALENCIA,
        CountryLocations.SEVILLE,
        CountryLocations.ZARAGOZA,
        CountryLocations.MALAGA,
        CountryLocations.MURCIA,
        CountryLocations.PALMA_SPAIN,
        CountryLocations.LAS_PALMAS,
        CountryLocations.BILBAO
    ],

    UserCountry.UK: [
        CountryLocations.LONDON,
        CountryLocations.MANCHESTER,
        CountryLocations.OXFORD,
        CountryLocations.CAMBRIDGE,
        CountryLocations.GLASGOW
    ]
}

JOBS_TREE: Dict[JobCategory, List[JobPosition]] = {
    JobCategory.SOFTWARE_ENGINEERING: [
        JobPosition.SWE_BACKEND,
        JobPosition.SWE_FRONTEND,
        JobPosition.SWE_FULLSTACK,
        JobPosition.SWE_ML,
        JobPosition.SWE_IOS,
        JobPosition.SWE_ANDROID,
        JobPosition.SWE_ARCHITECT,
        JobPosition.SALES_ENGINEER,
        JobPosition.SWE_DATA
    ],

    JobCategory.SRE_INFRA: [
        JobPosition.SRE_INFRA_INFRASTRUCTURE_ENG,
        JobPosition.SRE_INFRA_DEVOPS_ENG
    ],

    JobCategory.MANAGEMENT: [
        JobPosition.MANAGEMENT_PROJECT,
        JobPosition.MANAGEMENT_PROGRAM,
        JobPosition.MANAGEMENT_PRODUCT,
        JobPosition.MANAGEMENT_TECHNICAL_PROGRAM,
        JobPosition.MANAGEMENT_TECHNICAL_PRODUCT
    ],

    JobCategory.LEADERSHIP: [
        JobPosition.LEADERSHIP_TEAM_LEAD,
        JobPosition.LEADERSHIP_EMG,
        JobPosition.LEADERSHIP_VP,
        JobPosition.LEADERSHIP_CPO,
        JobPosition.LEADERSHIP_CTO
    ],

    JobCategory.DATA_AND_AI: [
        JobPosition.DATA_ANALYST,
        JobPosition.DATA_SCIENTIST,
        JobPosition.SWE_ML,
        JobPosition.SWE_DATA
    ],

    JobCategory.DESIGN: [
        JobPosition.DESIGN_UI,
        JobPosition.DESIGN_UX,
        JobPosition.DESIGN_GRAPHIC
    ],

    JobCategory.BUSINESS_OPS: [
        JobPosition.BUSINESS_ANALYST,
    ],

    JobCategory.QA: [
        JobPosition.QA_MANUAL,
        JobPosition.QU_AUTO
    ],

    JobCategory.SECURITY: [
        JobPosition.SECURITY_ANALYST,
        JobPosition.SECURITY_ENGINEER
    ],

    JobCategory.PEOPLE_OPS: [
        JobPosition.PEOPLE_OPS_HR,
        JobPosition.PEOPLE_OPS_RECRUITER,
        JobPosition.PEOPLE_HEAD_OF_HR,
        JobPosition.PEOPLE_HEAD_OF_RECRUITMENT
    ],

    JobCategory.SALES: [
        JobPosition.SALES_ACCOUNT_EXECUTIVE,
        JobPosition.SALES_ACCOUNT_MANAGER,
        JobPosition.SALES_ENGINEER,
        JobPosition.SALES_SOLUTIONS_ARCHITECT,
        JobPosition.SALES_DIRECTOR_OF_SALES,
        JobPosition.SALES_VP_VP_OF_SALES,
    ]
}

JOBS_TREE_INVERTED = defaultdict(list)

for category, positions in JOBS_TREE.items():
    for position in positions:
        JOBS_TREE_INVERTED[position].append(category)


SKILLS_TREE: Dict[SkillsCategory, List[UserSkill]] = {
    SkillsCategory.PROGRAMMING_LANGUAGES: [
        UserSkill.SWE_JAVA,
        UserSkill.SWE_PYTHON,
        UserSkill.SWE_JAVASCRIPT,
        UserSkill.SWE_NODE_JS,
        UserSkill.SWE_C_CPP,
        UserSkill.SWE_KOTLIN,
        UserSkill.SWE_C_SHARP,
        UserSkill.SWE_PHP,
        UserSkill.SWE_GOLANG,
        UserSkill.SWE_SCALA,
        UserSkill.SWE_RUST,
        UserSkill.SWE_SWIFT
    ],

    SkillsCategory.FRAMEWORKS: [
        UserSkill.FRAMEWORKS_REACT,
        UserSkill.FRAMEWORKS_ANGULAR,
        UserSkill.FRAMEWORKS_RUBY_ON_RAILS,
        UserSkill.FRAMEWORKS_DJANGO,
        UserSkill.FRAMEWORKS_VUE_JS,
        UserSkill.FRAMEWORKS_DRUPAL
    ],

    SkillsCategory.SRE_INFRA: [
        UserSkill.SRE_INFRA_NETWORKS,
        UserSkill.SRE_INFRA_HARDWARE,
        UserSkill.SRE_INFRA_CI_CD,
        UserSkill.SRE_INFRA_DATABASES_ADMINISTRATION,
        UserSkill.SRE_INFRA_MONITORING,
        UserSkill.SRE_INFRA_DATA_PIPELINES,
        UserSkill.SRE_INFRA_DATA_LAKES,
        UserSkill.SRE_INFRA_DOCKER,
        UserSkill.SRE_INFRA_KUBERNETES
    ],

    SkillsCategory.DATABASES: [
        UserSkill.DATABASES_SQL_DATABASES,
        UserSkill.DATABASES_NOSQL_DATABASES,
        UserSkill.DATABASES_IN_MEMORY_DATABASES
    ],

    SkillsCategory.DATA_AND_ML: [
        UserSkill.DATA_AND_ML_TENSORFLOW,
        UserSkill.DATA_AND_ML_PYTORCH,
        UserSkill.DATA_AND_ML_KERAS
    ],

    SkillsCategory.SOFT_SKILLS: [
        UserSkill.SOFT_SKILLS_MENTORING,
        UserSkill.SOFT_SKILLS_PEOPLE_MANAGEMENT,
        UserSkill.SOFT_SKILLS_AGILE,
        UserSkill.SOFT_SKILLS_SCRUM,
        UserSkill.SOFT_SKILLS_COUCHING
    ],

    SkillsCategory.ARCHITECTURE: [
        UserSkill.ARCHITECTURE_CLOUD_SOLUTIONS,
        UserSkill.ARCHITECTURE_HIGH_LOAD_SOLUTIONS,
        UserSkill.ARCHITECTURE_BIG_DATA_SOLUTIONS,
        UserSkill.ARCHITECTURE_GPU_SOLUTIONS
    ]
}
