from dataclasses import dataclass

from jobsbot.settings.enums import Env


__all__ = [
    "MongoDbConnectionParams",
    "get_mongodb_connection_params"
]


@dataclass
class MongoDbConnectionParams:
    host: str
    port: int
    database: str
    replica_set: str
    username: str
    password: str
    auth_source: str
    ssl_: bool
    ssl_certfile: str

    def __str__(self):
        return str({
            "host": self.host,
            "port": self.port,
            "database": self.database,
            "replica_set": self.replica_set,
            "username": self.username
        })

    def __repr__(self):
        return str(self)


def get_mongodb_connection_params(env: Env) -> MongoDbConnectionParams:
    host = "localhost"
    port = 27017
    database = "jobsbot"
    replica_set = None
    username = None
    password = None
    auth_source = None
    ssl_ = False
    ssl_certfile = None

    if env in (Env.PROD, Env.PREPROD):
        port = 27018
        username = "jobsbot"
        password = "jdfn83$23cn!ndf#afd"
        ssl_ = True
        ssl_certfile = "/usr/local/share/ca-certificates/Yandex/YandexInternalRootCA.crt"

        if env == Env.PROD:
            host = "rc1a-g324ykm4uoxxr4vg.mdb.yandexcloud.net"
            replica_set = "rs01"
            database = "jobsbot"
            auth_source = "jobsbot"
        else:
            host = "rc1a-g324ykm4uoxxr4vg.mdb.yandexcloud.net"
            replica_set = "rs01"
            database = "jobsbot-preprod"
            auth_source = "jobsbot-preprod"

    return MongoDbConnectionParams(
        host=host,
        port=port,
        database=database,
        replica_set=replica_set,
        username=username,
        password=password,
        auth_source=auth_source,
        ssl_=ssl_,
        ssl_certfile=ssl_certfile
    )
