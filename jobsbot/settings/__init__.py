from jobsbot.settings.enums import Env, get_env
from jobsbot.settings.telegram import get_tg_botname, get_tg_token
