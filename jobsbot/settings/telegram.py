from jobsbot.settings.enums import Env


__all__ = [
    "get_tg_botname",
    "get_tg_token"
]


def get_tg_token(env: Env) -> str:
    def no_token():
        raise ValueError(f"We have no token for env {env.name}")

    if env == Env.PROD:
        return "1698553100:AAHteZkoZtRhBDWb9NdPpbb0ZS4h1XOYIvk"
    elif env == Env.PREPROD:
        return "1698655388:AAHupsSbEp3VzeONsLgkZkdgrAqr2Ir_40M"
    elif env == Env.DEV:
        return "1600031286:AAHKCfo7nZndde9Ox_aNAgiKEjWGSqvJPms"
    else:
        no_token()


def get_tg_botname(env: Env) -> str:
    def no_name():
        raise ValueError(f"We have no telegram name for env {env.name}")

    if env == Env.PROD:
        return "JobsBotIoBot"
    elif env == Env.PREPROD:
        return "PreprodJBIOBot"
    elif env == Env.DEV:
        return "DevJBIOBot"
    else:
        no_name()
