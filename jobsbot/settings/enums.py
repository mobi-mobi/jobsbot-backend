import os

from enum import Enum


__all__ = [
    "Env",
    "get_env",
]


class Env(Enum):
    DEV = 1
    GITLAB = 2
    PREPROD = 3
    PROD = 4


def get_env(default: Env = Env.DEV) -> Env:
    _env = os.getenv("JOBSBOT_ENV")
    if _env is None:
        return default
    env = _env.lower().strip()
    if env.lower() == "dev":
        return Env.DEV
    if env.lower() == "gitlab":
        return Env.GITLAB
    if env.lower() == "preprod":
        return Env.PREPROD
    if env.lower() == "prod":
        return Env.PROD
    raise ValueError(f"Wrong jobsbot environment provided: {_env}")
