import dataclasses
import logging
import traceback

from datetime import datetime, timedelta
from random import choice
from time import sleep
from typing import Dict, List, Optional, Tuple

from telegram import Bot, ParseMode as TelegramParseMode, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.error import Unauthorized as TelegramUnauthorized, BadRequest

from jobsbot.bot.telegram.common.emoji import CANCEL, CHECKBOX
from jobsbot.dao import get_user_dao
from jobsbot.bot.telegram.common.messages import single_job_found_message_text, \
    single_job_found_message_keyboard_markup, single_job_found_message_image
from jobsbot.core.enums import JobPostingStatus, JobPostingValidationStatus, UserProfileValidationStatus, \
    UserPreferences, UserJobMatchType, UserNotificationOption, UserJobMatchSource
from jobsbot.core.enum_maps import JOBS_TREE_INVERTED
from jobsbot.core.logging import get_logger
from jobsbot.core.models import Company, Job, User, UserJobMatch
from jobsbot.services.email_sender import send_recruiter_invitation_email
from jobsbot.settings import get_tg_token, get_env


get_logger().info("Background service is started")
__TEST_IN_PROD_MODE__ = False
__TEST_IN_PROD_TG_USER_ID__ = 3623183


def send_user_status_updates_notifications():
    user_dao = get_user_dao()
    tg_token = get_tg_token(get_env())
    tg_bot = Bot(tg_token)
    logger = get_logger()

    for user in user_dao.read_users_with_validation_status_update(user_statuses=[
        UserProfileValidationStatus.VERIFIED,
        UserProfileValidationStatus.REJECTED
    ]):
        if user.tg_chat_id is None:
            continue
        try:
            if user.validation_status == UserProfileValidationStatus.REJECTED:
                text = "Sorry, but your profile has been rejected because of the next problems:\n"
                for reason in user.validation_rejection_reasons:
                    text += "\n * " + reason.title()
                text += "\n\nPlease, fix issues with the data you provided and we will be happy " \
                        "to review your profile one more time :)"
                text += "\n\nBy the way, we really want you to update these parts of your profile:\n"
                for profile_part in user.missing_profile_parts:
                    text += "\n * " + profile_part.title()
                tg_bot.send_photo(user.tg_chat_id, open("img/bot/problem.png", "rb"), text)
                logger.info(f"User {user.tg_id} has been rejected. Notification is sent.")
            else:
                text = "Hurrah! You profile has been successfully validated! " \
                       "You will soon start receiving job opportunities!"
                tg_bot.send_photo(user.tg_chat_id, open("img/bot/validated.png", "rb"), text)
                logger.info(f"User {user.tg_id} has been validated. Notification is sent.")
            user_dao.update_user_validation_status_update_info(user.tg_id)
        except Exception as e:
            if isinstance(e, TelegramUnauthorized) and "bot was blocked" in e.message:
                logger.info(f"Can not send a message to user {user.tg_id} ({user.name}): {e}. Deleting user")
                user_dao.delete_user(user.tg_id)
            else:
                logger.error(f"Error while processing user {user.tg_id} (send_user_status_updates_notifications)"
                             f":\n{traceback.format_exc()}")


def send_reminder_message(bot: Bot, user: User, reminder_type: str):
    @dataclasses.dataclass
    class ReminderConfig:
        yes_callback_data: str
        no_callback_data: str
        text: str
        yes_text: str = f"{CHECKBOX} Let's do it!"
        no_text: str = f"{CANCEL} Not now :("

    _CONFIG = {
        "job_categories": ReminderConfig(
            yes_callback_data="quick_settings_job_categories",
            no_callback_data="cancel_quick_settings_job_categories",
            text="Hello! You have not selected any position that interests you, "
                 "so the opportunities we offer you may not be of interest to you.\n\n"
                 "Why don't you choose the job positions that interest you right now? "
                 "It only takes 1 minute of your time!"
        ),
        "user_location": ReminderConfig(
            yes_callback_data="quick_profile_location",
            no_callback_data="cancel_quick_profile_location",
            text="Hey! Looks like you didn't give us your location. "
                 "That's why you do not receive job opportunities that best suit your needs.\n\n"
                 "Take just 1 minute to let us know where you are and you will start getting better job opportunities!"
        ),
        "user_profile": ReminderConfig(
            yes_callback_data="quick_user_profile",
            no_callback_data="cancel_quick_user_profile",
            text="Did you know that the best job opportunities are hidden from you "
                 "because your profile is not completed?\n\n"
                 "Take just a couple of minutes to update your profile and get access to the best job opportunities!"
        ),
    }

    user_dao = get_user_dao()
    tg_token = get_tg_token(get_env())
    tg_bot = Bot(tg_token)
    logger = get_logger()

    if reminder_type not in _CONFIG:
        raise ValueError(f"Unknown reminder_type: {reminder_type}")
    reminder_config: ReminderConfig = _CONFIG[reminder_type]
    reply_markup = InlineKeyboardMarkup([
        [InlineKeyboardButton(reminder_config.yes_text, callback_data=reminder_config.yes_callback_data),
         InlineKeyboardButton(reminder_config.no_text, callback_data=reminder_config.no_callback_data)]
    ])
    try:
        if user.tg_notification_message_id:
            try:
                bot.delete_message(user.tg_chat_id or user.tg_id, user.tg_notification_message_id)
            except BadRequest:
                pass
        message = tg_bot.send_message(user.tg_id, reminder_config.text, reply_markup=reply_markup)
        if message:
            user_dao.update_user_tg_notification_message_id(user.tg_id, message.message_id)
            user_dao.update_user_last_profile_not_ready_notification_time(user.tg_id)
            user_dao.update_user_profile_not_ready_notifications_sent(user.tg_id,
                                                                      user.profile_not_ready_notifications_sent + 1)
    except Exception as e:
        if isinstance(e, TelegramUnauthorized) and "bot was blocked" in e.message:
            logger.info(f"Can not send a message to user {user.tg_id} as used has blocked the bot: {e}. Deleting user.")
            user_dao.delete_user(user.tg_id)
        else:
            logger.error(f"Error while sending \"{reminder_type}\" reminder to user {user.tg_id}"
                         f":\n{traceback.format_exc()}")
    else:
        logger.info(f"Reminder message of type \"{reminder_type}\" was successfully sent to user {user.tg_id}")


def send_user_profile_is_not_ready_notifications():
    user_dao = get_user_dao()
    tg_token = get_tg_token(get_env())
    tg_bot = Bot(tg_token)
    logger = get_logger()

    # if __TEST_IN_PROD_MODE__:
    #     send_reminder_message(tg_bot, user_dao.read_user(__TEST_IN_PROD_TG_USER_ID__),
    #                           choice(["job_categories", "user_location", "user_profile"]))

    for user in user_dao.read_users_with_not_ready_profile(
            last_notification_min_delta=timedelta(hours=24),
            last_interaction_min_delta=timedelta(hours=1),
            max_profile_not_ready_notifications_sent=1000
    ):
        if __TEST_IN_PROD_MODE__ and user.tg_id != __TEST_IN_PROD_TG_USER_ID__:
            continue
        if user.tg_chat_id is None:
            continue
        try:
            for last_interaction_at in (user.created_at, user.updated_at, user.last_interaction_time):
                if last_interaction_at is not None and datetime.utcnow() - last_interaction_at < timedelta(hours=1):
                    continue

            if user.last_profile_not_ready_notification_time is not None \
                    and datetime.utcnow() - user.last_profile_not_ready_notification_time < timedelta(hours=24):
                continue

            if user.last_daily_jobs_sent_at is not None \
                    and datetime.utcnow() - user.last_daily_jobs_sent_at < timedelta(hours=2):
                continue

            if (user.last_daily_jobs_sent_at is None
                    or datetime.utcnow() - user.last_daily_jobs_sent_at > timedelta(days=7)) \
                    and user.profile_not_ready_notifications_sent > 5:
                if user.last_profile_not_ready_notification_time is not None \
                        and datetime.utcnow() - user.last_profile_not_ready_notification_time < timedelta(days=3):
                    continue

            possible_notification_types = []
            if not user.desired_positions:
                possible_notification_types.append("job_categories")
            if user.user_country is None and user.user_country_location is None:
                possible_notification_types.append("user_location")
            if not possible_notification_types and user.missing_profile_parts:
                possible_notification_types.append("user_profile")
            if possible_notification_types:
                notification_type = choice(possible_notification_types)
                send_reminder_message(tg_bot, user, notification_type)
            else:
                logger.warning(f"Something is wrong: User {user.tg_id} was retrieved as a user with no full "
                               f"profile info, but we couldn't find missing information: {user}")
        except Exception:
            logger.error(f"Error while processing user {user.tg_id} (send_user_profile_is_not_ready_notifications)"
                         f":\n{traceback.format_exc()}")


def is_job_user_match(user: User, job: Job, company: Company, allow_partial_match: bool = True) -> UserJobMatchType:
    def log(msg: str):
        pass

    logger = get_logger()

    log(f"Matching user {user.tg_id} and job {job.job_id}")

    user_preferences = user.preferences or []

    if UserPreferences.NO_AGENCY in user_preferences and company.is_agency:
        return UserJobMatchType.NO_MATCH
    if UserPreferences.NO_CONSULTING in user_preferences and company.is_consulting:
        return UserJobMatchType.NO_MATCH
    if UserPreferences.NO_STARTUPS in user_preferences and company.is_startup:
        return UserJobMatchType.NO_MATCH
    if UserPreferences.NO_TEMPORARY in user_preferences and job.is_temporary:
        return UserJobMatchType.NO_MATCH
    if UserPreferences.NO_INTERNSHIP in user_preferences and job.is_internship:
        return UserJobMatchType.NO_MATCH
    if UserPreferences.ONLY_INTERNSHIP in user_preferences and not job.is_internship:
        return UserJobMatchType.NO_MATCH
    if UserPreferences.WITH_STOCKS in user_preferences and not job.stocks_available:
        return UserJobMatchType.NO_MATCH
    if UserPreferences.WITH_CONFERENCES in user_preferences and not job.with_conferences_budget:
        return UserJobMatchType.NO_MATCH

    log("Simple filters passed")

    log(f"User preferences: {user_preferences}")

    office_remote_match = False
    if UserPreferences.READY_FOR_REMOTE in user_preferences and job.fully_remote_allowed:
        office_remote_match = True
    if UserPreferences.READY_FOR_HYBRID in user_preferences and job.partially_remote_allowed:
        office_remote_match = True
    if UserPreferences.READY_FOR_OFFICE in user_preferences and not job.work_from_office_is_not_allowed:
        office_remote_match = True
    if not office_remote_match:
        return UserJobMatchType.NO_MATCH

    log("Office remote match filters passed")

    if user.min_annual_salary is not None and int(user.min_annual_salary.value) > job.annual_salary_max:
        return UserJobMatchType.NO_MATCH

    log("Salary filters passed")

    partial_match = False

    if user.desired_positions and job.position not in user.desired_positions:
        return UserJobMatchType.NO_MATCH
    elif not user.desired_positions and allow_partial_match:
        partial_match = True

    log("Desired positions filters passed")

    for skill in job.must_have_skills:
        if user.skills and skill not in user.skills:
            return UserJobMatchType.NO_MATCH
        elif not user.skills and allow_partial_match:
            partial_match = True

    log("Skills filters passed")

    location_matches = False

    if user.user_country_location is not None and user.user_country_location in job.locations:
        location_matches = True

    if user.user_country is not None \
            and user.user_relocation_locations \
            and user.user_country in job.relocation_from_countries \
            and ((UserPreferences.RELOCATION in user_preferences and job.with_relocation)
                 or (UserPreferences.RELOCATION not in user_preferences)):
        for job_location in job.locations:
            for country, country_locations in user.user_relocation_locations.items():
                if not country_locations or job_location in country_locations:
                    location_matches = True

    if user.user_country is not None:
        if job.fully_remote_allowed and UserPreferences.READY_FOR_REMOTE in user_preferences:
            if user.user_country in job.remote_work_from_countries:
                location_matches = True

    if not location_matches:
        if allow_partial_match and not user.user_country:
            partial_match = True
        else:
            return UserJobMatchType.NO_MATCH

    log("Location filters passed")

    if user.experience is None:
        user_experience = 0
    else:
        user_experience = int(user.experience.value)
    if job.min_experience.value > user_experience:
        if user_experience is None and allow_partial_match:
            partial_match = True
        else:
            return UserJobMatchType.NO_MATCH

    log("User experience positions filters passed")

    if partial_match:
        return UserJobMatchType.PARTIAL_MATCH

    return UserJobMatchType.FULL_MATCH


def find_user_jobs_matches():
    def add_user_match(user: User, job: Job, user_job_match_type: UserJobMatchType,
                       user_job_match_source: UserJobMatchSource):
        existing_matches = list(user_dao.read_user_job_matches(user_tg_id=user.tg_id, job_id=job.job_id,
                                                               no_older_than=None))
        if len(existing_matches) > 1:
            logging.error(f"Something is wrong: we found more than one match for user_tg_id={user.tg_id}, "
                          f"job_id={job.job_id}")
            return
        if existing_matches:
            match = existing_matches[0]
            if match.is_user_notified:
                return
            if match.match_type == user_job_match_type:
                return
            if match.match_type != UserJobMatchType.PARTIAL_MATCH:
                return
        user_dao.create_user_job_match(UserJobMatch(
            user_tg_id=user.tg_id,
            job_id=job.job_id,
            matched_on=datetime.utcnow(),
            match_type=user_job_match_type,
            match_source=user_job_match_source,
            daily_notification_sent=False,
            daily_notification_sent_at=None,
            weekly_notification_sent=False,
            weekly_notification_sent_at=None,
        ), replace_if_exists=True)

    # TODO: Optimize me!
    user_dao = get_user_dao()
    for job in user_dao.read_all_jobs(created_after=datetime.utcnow() - timedelta(days=30),
                                      job_posting_status=JobPostingStatus.ACTIVE,
                                      job_posting_validation_status=JobPostingValidationStatus.VERIFIED):
        company = user_dao.read_company(job.company_id)
        if company is None:
            continue
        job_categories = JOBS_TREE_INVERTED[job.position]
        for subscription in user_dao.read_matching_user_subscriptions(job):
            user = user_dao.read_user(subscription.user_tg_id)
            if job.position in subscription.job_positions \
                    or ([jc for jc in job_categories if jc in subscription.job_categories]) \
                    or (subscription.all_matching_positions and job.position in user.desired_positions):
                user_job_match_type = is_job_user_match(user, job, company)
                if user_job_match_type == UserJobMatchType.NO_MATCH:
                    continue
                if user_job_match_type == UserJobMatchType.PARTIAL_MATCH and not job.allow_non_complete_profiles:
                    continue
                add_user_match(user, job, user_job_match_type, UserJobMatchSource.SUBSCRIPTION)
        for user in user_dao.read_all_users():
            if __TEST_IN_PROD_MODE__ and user.tg_id != __TEST_IN_PROD_TG_USER_ID__:
                continue
            user_job_match_type = is_job_user_match(user, job, company)
            if user_job_match_type == UserJobMatchType.NO_MATCH:
                continue
            if user_job_match_type == UserJobMatchType.PARTIAL_MATCH and not job.allow_non_complete_profiles:
                continue
            add_user_match(user, job, user_job_match_type, UserJobMatchSource.JOB_MATCH)


def get_best_user_jobs_matches(user_: User, job_job_matches: List[Tuple[Job, UserJobMatch]], n: int) \
        -> List[Tuple[Job, UserJobMatch]]:
    def user_job_score(user: User, job: Job, job_match: UserJobMatch) -> float:
        # TODO: Make me better!
        age_score = job.updated_at.timestamp() / datetime.utcnow().timestamp()
        return float(1 if job_match.match_type == UserJobMatchType.FULL_MATCH else 0) + age_score

    job_scores: Dict[str, float] = {job.job_id: user_job_score(user_, job, job_match)
                                    for job, job_match in job_job_matches}
    return sorted(job_job_matches, key=lambda job_job_match: job_scores[job_job_match[0].job_id], reverse=True)[0:n]


def send_job_to_user(user: User, job: Job, company: Company, job_match: UserJobMatch):
    tg_token = get_tg_token(get_env())
    tg_bot = Bot(tg_token)
    logger = get_logger()

    text = single_job_found_message_text(job, company, user, is_applied=False, is_request_cancelled=False,
                                         is_too_many_requests=False)
    keyboard_markup = None
    if job_match.match_type == UserJobMatchType.FULL_MATCH and job.allow_one_click_applications:
        keyboard_markup = single_job_found_message_keyboard_markup(job, show_apply_button=True)
    elif job.job_url:
        url = f"http://go.jobsbot.io/a/{job_match.match_hash}"
        keyboard_markup = single_job_found_message_keyboard_markup(job, show_apply_on_site_button=True,
                                                                   apply_on_website_url=url,
                                                                   apply_on_website_text=job.apply_on_text)
    photo = single_job_found_message_image(job, company)

    msg = tg_bot.send_photo(
        chat_id=user.tg_chat_id,
        photo=photo,
        caption=text,
        parse_mode=TelegramParseMode.MARKDOWN_V2,
        reply_markup=keyboard_markup
    )

    if msg is None:
        raise Exception(f"Job {job.job_id} ({job.title}) was not sent to user {user.tg_id} ({user.name}) "
                        f"because of some reasons on Telegram side. We will try again later :(")

    logger.info(f"We've sent a job {job.job_id} ({job.title}) to user {user.tg_id} ({user.name})")


def send_daily_jobs_matches():
    # TODO: Optimize me!
    user_dao = get_user_dao()
    logger = get_logger()

    user_job_matches: Dict[int, List[str]] = dict()

    for user_job_match in user_dao.read_user_job_matches(daily_notification_sent=False):
        if user_job_match.user_tg_id not in user_job_matches:
            user_job_matches[user_job_match.user_tg_id] = [user_job_match.job_id]
        else:
            user_job_matches[user_job_match.user_tg_id].append(user_job_match.job_id)

    companies: Dict[str, Optional[Company]] = dict()

    for user_tg_id, job_ids in user_job_matches.items():
        if __TEST_IN_PROD_MODE__ and user_tg_id != __TEST_IN_PROD_TG_USER_ID__:
            continue
        user = user_dao.read_user(user_tg_id)
        if user is None:
            logger.warning(f"Can not find user: {user_tg_id}")
            continue

        if UserNotificationOption.MATCHED_JOBS_DAILY not in user.user_notification_options:
            continue

        for last_interaction_at in (user.created_at, user.updated_at, user.last_interaction_time):
            if last_interaction_at is not None and datetime.utcnow() - last_interaction_at < timedelta(minutes=15):
                continue

        sent_matches = list(user_dao.read_user_job_matches(user_tg_id=user.tg_id, no_older_than=timedelta(days=1),
                                                           daily_notification_sent=True))

        if len(sent_matches) >= 3:
            continue

        if len(list(filter(lambda ujm: ujm.daily_notification_sent_at > datetime.utcnow() - timedelta(hours=5),
                           sent_matches))) >= 2:
            continue

        if user.last_daily_jobs_sent_at is not None \
                and datetime.utcnow() - user.last_daily_jobs_sent_at < timedelta(hours=1, minutes=15):
            continue

        logger.info(f"Trying to pick the best jobs from {job_ids} to send them to user {user_tg_id}")

        jobs = []
        job_matches_dict = {
            job_match.job_id: job_match
            for job_match in user_dao.read_user_job_matches(user_tg_id, no_older_than=timedelta(days=31))
        }
        job_matches = []
        for job_id in job_ids:
            job = user_dao.read_job(job_id)
            if job is None:
                continue
            if job.job_id not in job_matches_dict:
                continue
            if job.company_id not in companies:
                companies[job.company_id] = user_dao.read_company(job.company_id)
            if companies[job.company_id] is None:
                logger.warning(f"Can not find company {job.company_id}")
                continue
            jobs.append(job)
            job_matches.append(job_matches_dict[job.job_id])
        if not jobs:
            logger.warning(f"Can not find jobs: {job_ids}")
            continue

        job_job_matches_to_send = get_best_user_jobs_matches(user, list(zip(jobs, job_matches)), n=1)
        if not job_job_matches_to_send:
            logger.warning(f"No jobs to send: {job_ids}")
            continue
        jobs_to_log = ", ".join(map(lambda x: str((x[0].title, x[0].job_id)), job_job_matches_to_send))
        logger.info(f"Trying to send this jobs to user {user_tg_id}: {jobs_to_log}")

        for job, job_match in job_job_matches_to_send:
            try:
                send_job_to_user(user, job, companies[job.company_id], job_match)
            except Exception as e:
                if isinstance(e, TelegramUnauthorized) and "bot was blocked" in e.message:
                    logger.info(f"Can not send a message to user {user.tg_id} ({user.name}): {e}. Deleting user")
                    user_dao.delete_user(user.tg_id)
                else:
                    logger.error(f"Error while sending job {job.job_id} to user {user.tg_id}"
                                 f":\n{traceback.format_exc()}")
            else:
                user_dao.update_user_job_match_sent_status(user_tg_id=user.tg_id, job_id=job.job_id,
                                                           daily_notification_sent=True,
                                                           daily_notification_sent_at=datetime.utcnow())
                user_dao.update_user_jobs_notifications_time(tg_id=user.tg_id,
                                                             last_daily_jobs_sent_at=datetime.utcnow())


def send_weekly_jobs_matches():
    pass


def send_invitations_to_recruiters():
    user_dao = get_user_dao()
    logger = get_logger()
    for invitation in user_dao.read_recruiter_invitations(sent=False, expired=False, limit=10):
        logger.info(f"Found unsent invitation for {invitation.name} <{invitation.email}>")
        company = user_dao.read_company(invitation.company_id)
        if company is None:
            logger.warning(f"Company {invitation.company_id} does not exists. Deleting invitation.")
            user_dao.delete_recruiter_invitation_by_token(invitation.invitation_token)
            continue
        send_recruiter_invitation_email(
            company_name=company.company_name,
            recruiter_email=invitation.email,
            recruiter_name=invitation.name,
            inviter_name=invitation.invited_by,
            join_link=f"http://go.jobsbot.io/ir/{invitation.invitation_token}"
        )
        invitation.invitation_sent = True
        user_dao.update_recruiter_invitation_sent_status(token=invitation.invitation_token, sent=True)
        logger.info(f"Notification to {invitation.email} was successfully sent")

    user_dao.delete_expired_recruiter_invitations()


def calculate_jobs_statistics():
    # TODO: Improve me!
    user_dao = get_user_dao()

    for job in user_dao.read_all_jobs():
        statistics = user_dao.calc_job_stats(job_id=job.job_id)
        user_dao.upsert_job_statistics(statistics)


def main():
    while True:
        find_user_jobs_matches()

        if (9 <= datetime.utcnow().hour <= 19) or __TEST_IN_PROD_MODE__:
            send_user_status_updates_notifications()
            send_user_profile_is_not_ready_notifications()
            send_daily_jobs_matches()
            if datetime.utcnow().weekday() == 4:  # Friday
                send_weekly_jobs_matches()

        # send_invitations_to_recruiters()
        calculate_jobs_statistics()
        sleep(10)


if __name__ == "__main__":
    main()
