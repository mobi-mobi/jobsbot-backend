import smtplib
import pathlib

from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.utils import formatdate, make_msgid

from jobsbot.core.logging import get_logger

_recruiter_invitation_file = pathlib.Path(__file__).parent.absolute().joinpath("templates")\
    .joinpath("recruiter_invitation").joinpath("email.html")
with open(_recruiter_invitation_file, "r") as fp:
    _recruiter_invitation_template = fp.read()


_jobs_bot_logo_file = pathlib.Path(__file__).parent.absolute().joinpath("templates").joinpath("recruiter_invitation")\
    .joinpath("jobsbot-logo.png")
with open(_jobs_bot_logo_file, "rb") as fp:
    _jobs_bot_logo_str = fp.read()


def nice_float(f: float, preciseness: int = 2) -> str:
    s = f"%.{preciseness}f"
    return s % f


def nice_int(i: int) -> str:
    negative = i < 0
    i = abs(i)
    if i < 1000:
        result = str(i)
    elif i < 1000000:
        result = nice_float(i / 1000, preciseness=1) + "K"
    else:
        result = nice_float(i / 1000000, preciseness=1) + "M"
    if negative:
        result = "-" + result
    return result


def html_special_chars(s: str) -> str:
    return s.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")


def send_email(from_email: str, from_name: str, from_password: str, to_email: str, to_name: str, subject: str,
               html_content: str):
    get_logger().info(f"Sending email to {to_email}...")
    msg = MIMEMultipart('related')
    msg['Subject'] = subject
    msg['From'] = f"{from_name} <{from_email}>"
    msg['To'] = f"{to_name} <{to_email}>"
    # msg['Date'] = formatdate()
    # msg['Message-ID'] = make_msgid(domain='jobsbot.io')

    html_part = MIMEText(html_content, 'html')
    msg.attach(html_part)

    msg_image = MIMEImage(_jobs_bot_logo_str)
    msg_image.add_header('Content-ID', '<image_logo>')
    msg.attach(msg_image)

    mail = smtplib.SMTP('smtp.gmail.com', 587)
    mail.ehlo()
    mail.starttls()
    mail.login(from_email, from_password)
    mail.sendmail(from_email, to_email, msg.as_string())
    mail.quit()
    get_logger().info(f"Message to {to_email} sent!")
    del mail


def build_recruiter_invitation_email(name: str, inviter_name: str, company_name: str, join_link: str) -> str:
    return _recruiter_invitation_template\
        .replace("%USERNAME%", html_special_chars(name))\
        .replace("%COMPANY_ADMIN%", html_special_chars(inviter_name))\
        .replace("%COMPANY_NAME%", html_special_chars(company_name))\
        .replace("%JOIN_LINK%", join_link)\
        .replace("%LOGO_IMAGE_SRC%", "cid:image_logo")


def send_recruiter_invitation_email(recruiter_email: str, recruiter_name: str, company_name: str, inviter_name: str,
                                    join_link: str):
    send_email(
        from_email="sergey@jobsbot.io",
        from_name="JobsBot.io",
        from_password="5B1AGkX1V1uL8Ocvh9",
        to_email=recruiter_email,
        to_name=recruiter_name,
        subject=f"Invitation to join {company_name} @ JobsBot.io",
        html_content=build_recruiter_invitation_email(
            name=recruiter_name,
            inviter_name=inviter_name,
            company_name=company_name,
            join_link=join_link
        )
    )
