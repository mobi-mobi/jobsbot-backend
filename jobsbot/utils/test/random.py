from datetime import datetime
from random import choice
from string import ascii_lowercase, ascii_uppercase, ascii_letters
from typing import List

from jobsbot.core.enums import JobPosition, UserSkill, UserCountry, CountryLocations,\
    JobPostingStatus, JobPostingValidationStatus, UserExperience, UserJobApplicationMethod
from jobsbot.core.models import Job


def random_string(len_: int = 10, alphabet: str = None):
    assert len_ > 0
    alphabet = alphabet or ascii_lowercase + ascii_uppercase + ascii_letters
    return "".join(choice(alphabet) for _ in range(len_))


def random_job(job_id: str = None, job_url: str = None,
               allow_non_complete_profiles: bool = False, allow_one_click_applications: bool = True,
               created_by: str = None,
               company_id: str = None, position: JobPosition = JobPosition.SWE_FULLSTACK, title: str = None,
               min_experience: UserExperience = UserExperience.Y_0, must_have_skills: List[UserSkill] = None,
               better_to_have_skills: List[UserSkill] = None, hiring_manager_name: str = None,
               department_name: str = None, description: str = None, about_company: str = None,
               technologies: str = None, locations: List[CountryLocations] = None,
               remote_work_from_countries: List[UserCountry] = None, annual_salary_min: int = 50000,
               annual_salary_max: int = 100000, show_annual_salary: bool = True, is_temporary: bool = False,
               is_internship: bool = False, with_conferences_budget: bool = True,
               fully_remote_allowed: bool = False, partially_remote_allowed: bool = True,
               work_from_office_is_not_allowed: bool = False,
               with_relocation: bool = True, relocation_from_countries: List[UserCountry] = None,
               stocks_available: bool = False) -> Job:
    return Job(
        job_id=job_id or random_string(),
        job_posting_status=JobPostingStatus.ACTIVE,
        job_posting_validation_status=JobPostingValidationStatus.NOT_READY,
        job_url=job_url,
        allow_non_complete_profiles=allow_non_complete_profiles,
        allow_one_click_applications=allow_one_click_applications,
        created_at=datetime.utcnow(),
        updated_at=datetime.utcnow(),
        created_by=created_by or random_string(),
        company_id=company_id or random_string(),
        position=position,
        title=title or random_string(),
        min_experience=min_experience,
        must_have_skills=must_have_skills or [],
        better_to_have_skills=better_to_have_skills or [],
        hiring_manager_name=hiring_manager_name or random_string(),
        department_name=department_name or random_string(),
        description=description or random_string(),
        about_company=about_company or random_string(),
        technologies=technologies,
        locations=locations or [CountryLocations.PARIS],
        remote_work_from_countries=remote_work_from_countries or [],
        annual_salary_min=annual_salary_min,
        annual_salary_max=annual_salary_max,
        show_annual_salary=show_annual_salary,
        is_temporary=is_temporary,
        is_internship=is_internship,
        with_conferences_budget=with_conferences_budget,
        fully_remote_allowed=fully_remote_allowed,
        partially_remote_allowed=partially_remote_allowed,
        work_from_office_is_not_allowed=work_from_office_is_not_allowed,
        with_relocation=with_relocation,
        relocation_from_countries=relocation_from_countries or [UserCountry.GERMANY, UserCountry.OTHER_EU_COUNTRY],
        stocks_available=stocks_available
    )
