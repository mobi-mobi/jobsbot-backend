Create a new user:

```
sudo useradd -m -d /home/jobsbot -s /bin/bash jobsbot
```

Checkout repository:

```
sudo su - jobsbot
git clone https://<token>:<password>@gitlab.com/mobi-mobi/jobsbot-backend.git
exit
```

Here are credentials you can use:

```
Login: jobsbot-backend
Password: yUiikcKXbGMqJqNkKf_Y
```

Install python 3.7

From here: https://jcutrer.com/linux/upgrade-python37-ubuntu1810

```
sudo apt-get install python3.7
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
sudo update-alternatives --config python3  # Enter 2
```

Install pip3:

```
sudo apt install python3-pip
sudo pip3 install gunicorn
```

Install packages:

```
sudo su - jobsbot
cd jobsbot-backend
pip3 install -rrequirements.txt
exit
```

#### Configure Telegram Bot Process:

```
sudo mkdir -p /var/log/jobsbot/tg_bot
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/tg_bot

sudo touch /var/log/jobsbot/tg_bot/stdout.log
sudo chown syslog:adm /var/log/jobsbot/tg_bot/stdout.log

cat > /etc/rsyslog.d/tg_bot.conf << ENDOFFILE
if $programname == 'tg_bot' then /var/log/jobsbot/tg_bot/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/tg_bot.service << ENDOFFILE
[Unit]
Description=Jobsbot Telegram Bot
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PROD"
ExecStart=python3 -m jobsbot.bot.telegram.__init__
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=9
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=tg_bot

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Telegram Bot process:
```
sudo systemctl start tg_bot
```

#### Configure Telegram Background Process:

```
sudo mkdir -p /var/log/jobsbot/tg_background
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/tg_background

sudo touch /var/log/jobsbot/tg_background/stdout.log
sudo chown syslog:adm /var/log/jobsbot/tg_background/stdout.log

cat > /etc/rsyslog.d/tg_background.conf << ENDOFFILE
if $programname == 'tg_background' then /var/log/jobsbot/tg_background/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/tg_background.service << ENDOFFILE
[Unit]
Description=Jobsbot Telegram Background
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PROD"
ExecStart=python3 -m jobsbot.services.background
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=9
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=tg_background

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Telegram Background process:
```
sudo systemctl start tg_background
```

#### Configure Pre-production Telegram Bot Process:

```
sudo mkdir -p /var/log/jobsbot/tg_preprod_bot
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/tg_preprod_bot

sudo touch /var/log/jobsbot/tg_preprod_bot/stdout.log
sudo chown syslog:adm /var/log/jobsbot/tg_preprod_bot/stdout.log

cat > /etc/rsyslog.d/tg_preprod_bot.conf << ENDOFFILE
if $programname == 'tg_preprod_bot' then /var/log/jobsbot/tg_preprod_bot/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/tg_preprod_bot.service << ENDOFFILE
[Unit]
Description=Jobsbot Preprod Telegram Bot
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PREPROD"
ExecStart=python3 -m jobsbot.bot.telegram.__init__
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=9
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=tg_preprod_bot

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Telegram Bot process:
```
sudo systemctl start tg_preprod_bot
```

#### Configure Telegram Preprod Background Process:

```
sudo mkdir -p /var/log/jobsbot/tg_preprod_background
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/tg_preprod_background

sudo touch /var/log/jobsbot/tg_preprod_background/stdout.log
sudo chown syslog:adm /var/log/jobsbot/tg_preprod_background/stdout.log

cat > /etc/rsyslog.d/tg_preprod_background.conf << ENDOFFILE
if $programname == 'tg_preprod_background' then /var/log/jobsbot/tg_preprod_background/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/tg_preprod_background.service << ENDOFFILE
[Unit]
Description=Jobsbot Telegram Preprod Background
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PREPROD"
ExecStart=python3 -m jobsbot.service.background
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=9
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=tg_preprod_background

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Telegram Background process:
```
sudo systemctl start tg_preprod_background
```

#### Configure Go/Redirection API:

```
sudo mkdir -p /var/log/jobsbot/go_api
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/go_api

sudo touch /var/log/jobsbot/go_api/stdout.log
sudo chown syslog:adm /var/log/jobsbot/go_api/stdout.log

cat > /etc/rsyslog.d/go_api.conf << ENDOFFILE
if $programname == 'go_api' then /var/log/jobsbot/go_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/go_api.service << ENDOFFILE
[Unit]
Description=Jobsbot Go Api
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/jobsbot/go_api.sock "jobsbot.web.api.wsgi:go_api()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=go_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Admin Api process:
```
sudo systemctl start go_api
```

#### Configure Admin API:

```
sudo mkdir -p /var/log/jobsbot/admin_api
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/admin_api

sudo touch /var/log/jobsbot/admin_api/stdout.log
sudo chown syslog:adm /var/log/jobsbot/admin_api/stdout.log

cat > /etc/rsyslog.d/admin_api.conf << ENDOFFILE
if $programname == 'admin_api' then /var/log/jobsbot/admin_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/admin_api.service << ENDOFFILE
[Unit]
Description=Jobsbot Admin Api
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/jobsbot/admin_api.sock "jobsbot.web.api.wsgi:admin_api()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=admin_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Admin Api process:
```
sudo systemctl start admin_api
```

#### Configure Pre-production Admin API:

```
sudo mkdir -p /var/log/jobsbot/admin_preprod_api
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/admin_preprod_api

sudo touch /var/log/jobsbot/admin_preprod_api/stdout.log
sudo chown syslog:adm /var/log/jobsbot/admin_preprod_api/stdout.log

cat > /etc/rsyslog.d/admin_preprod_api.conf << ENDOFFILE
if $programname == 'admin_preprod_api' then /var/log/jobsbot/admin_preprod_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/admin_preprod_api.service << ENDOFFILE
[Unit]
Description=Jobsbot Preprod Admin Api
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PREPROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/jobsbot/admin_preprod_api.sock "jobsbot.web.api.wsgi:admin_api()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=admin_preprod_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Admin Api process:
```
sudo systemctl start admin_preprod_api
```

#### Configure Console API:

```
sudo mkdir -p /var/log/jobsbot/console_api
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/console_api

sudo touch /var/log/jobsbot/console_api/stdout.log
sudo chown syslog:adm /var/log/jobsbot/console_api/stdout.log

cat > /etc/rsyslog.d/console_api.conf << ENDOFFILE
if $programname == 'console_api' then /var/log/jobsbot/console_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/console_api.service << ENDOFFILE
[Unit]
Description=Jobsbot Console Api
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/jobsbot/console_api.sock "jobsbot.web.api.wsgi:console_api()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=console_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Admin Api process:
```
sudo systemctl start console_api
```

#### Configure Pre-production Console API:

```
sudo mkdir -p /var/log/jobsbot/console_preprod_api
sudo chown -R jobsbot:jobsbot /var/log/jobsbot/console_preprod_api

sudo touch /var/log/jobsbot/console_preprod_api/stdout.log
sudo chown syslog:adm /var/log/jobsbot/console_preprod_api/stdout.log

cat > /etc/rsyslog.d/console_preprod_api.conf << ENDOFFILE
if $programname == 'console_preprod_api' then /var/log/jobsbot/console_preprod_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/console_preprod_api.service << ENDOFFILE
[Unit]
Description=Jobsbot Preprod Console Api
After=network.target

[Service]
User=jobsbot
Group=www-data
WorkingDirectory=/home/jobsbot/jobsbot-backend
Environment="JOBSBOT_ENV=PREPROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/jobsbot/console_preprod_api.sock "jobsbot.web.api.wsgi:console_api()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=console_preprod_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Admin Api process:
```
sudo systemctl start console_preprod_api
```

### Configure logs rotation

```
sudo vim /etc/logrotate.d/rsyslog
```

```
/var/log/jobsbot/tg_bot/stdout.log
/var/log/jobsbot/tg_preprod_bot/stdout.log
/var/log/jobsbot/tg_background/stdout.log
/var/log/jobsbot/tg_preprod_bot/stdout.log
/var/log/jobsbot/go_api/stdout.log
/var/log/jobsbot/admin_api/stdout.log
/var/log/jobsbot/admin_preprod_api/stdout.log
/var/log/jobsbot/console_api/stdout.log
/var/log/jobsbot/console_preprod_api/stdout.log
{
        rotate 4
        weekly
        missingok
        notifempty
        compress
        delaycompress
        sharedscripts
        postrotate
                /usr/lib/rsyslog/rsyslog-rotate
        endscript
}
```

Install nginx:

```
sudo apt install nginx
```

Setup a firewall:

```
sudo ufw enable
sudo ufw allow 'Nginx HTTP'
sudo ufw allow 'Nginx HTTPS'
sudo ufw allow 'OpenSSH'
sudo ufw status
```

Create backend-jobsbot.io config:

```
sudo vim /etc/nginx/sites-available/backend-jobsbot.io
```

Put the next content there:

```
server {
    listen 80;
    server_name go.jobsbot.io;
    charset utf-8;

    location /api/v1/status {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/go_api.sock;
    }

    location /a/ {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/go_api.sock;
    }
    
    location / {
        default_type application/json;
        return 404 '{"status":"ERROR","error":{"code":404,"description":"Not Found"}}';
    }
}

server {
    listen 80;
    server_name admin.jobsbot.io;
    charset utf-8;

    location /api/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/admin_api.sock;
    }

    location /page/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/admin_api.sock;
    }
    
    location / {
        default_type application/json;
        return 404 '{"status":"ERROR","error":{"code":404,"description":"Not Found"}}';
    }
}

server {
    listen 80;
    server_name admin-preprod.jobsbot.io;
    charset utf-8;

    location /api/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/admin_preprod_api.sock;
    }

    location /page/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/admin_preprod_api.sock;
    }
    
    location / {
        default_type application/json;
        return 404 '{"status":"ERROR","error":{"code":404,"description":"Not Found"}}';
    }
}

server {
    listen 80;
    server_name console.jobsbot.io;
    charset utf-8;

    location /api/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/console_api.sock;
    }

    location /page/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/console_api.sock;
    }
    
    location / {
        default_type application/json;
        return 404 '{"status":"ERROR","error":{"code":404,"description":"Not Found"}}';
    }
}

server {
    listen 80;
    server_name console-preprod.jobsbot.io;
    charset utf-8;

    location /api/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/console_preprod_api.sock;
    }

    location /page/v1 {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/jobsbot/console_preprod_api.sock;
    }
    
    location / {
        default_type application/json;
        return 404 '{"status":"ERROR","error":{"code":404,"description":"Not Found"}}';
    }
}
```

Create a symlink:

```
sudo ln -s /etc/nginx/sites-available/backend-jobsbot.io /etc/nginx/sites-enabled/backend-jobsbot.io
```

Restart nginx:

```
sudo /etc/init.d/nginx restart
```
