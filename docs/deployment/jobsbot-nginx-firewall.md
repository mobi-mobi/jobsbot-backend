Install nginx:

```
sudo apt install nginx
```

Setup a firewall:

```
sudo ufw enable
sudo ufw allow 'Nginx HTTP'
sudo ufw allow 'Nginx HTTPS'
sudo ufw allow 'OpenSSH'
sudo ufw status
```

Create all.jobsbot.io config:

```
sudo vim /etc/nginx/sites-available/all.jobsbot.io
```

Put the next content there:

```
server {
    listen 80;
    server_name admin.jobsbot.io;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 8080;
    server_name admin.jobsbot.io;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 80;
    server_name admin-preprod.jobsbot.io;

    # auth_basic "Preprod";
    # auth_basic_user_file /etc/nginx/htpasswd/.htpasswd;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 8080;
    server_name admin-preprod.jobsbot.io;

    # auth_basic "Preprod";
    # auth_basic_user_file /etc/nginx/htpasswd/.htpasswd;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 80;
    server_name console.jobsbot.io;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 8080;
    server_name console.jobsbot.io;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 80;
    server_name console-preprod.jobsbot.io;

    # auth_basic "Preprod";
    # auth_basic_user_file /etc/nginx/htpasswd/.htpasswd;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 8080;
    server_name console-preprod.jobsbot.io;

    # auth_basic "Preprod";
    # auth_basic_user_file /etc/nginx/htpasswd/.htpasswd;

    location /api/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
    
    location /page/v1 {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }

    location / {
        # 10.130.0.5 == jobsbot-frontend
        proxy_pass http://10.130.0.5$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 80;
    server_name go.jobsbot.io;

    # auth_basic "Preprod";
    # auth_basic_user_file /etc/nginx/htpasswd/.htpasswd;

    location / {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
}

server {
    listen 8080;
    server_name go.jobsbot.io;

    # auth_basic "Preprod";
    # auth_basic_user_file /etc/nginx/htpasswd/.htpasswd;

    location / {
        # 10.130.0.15 == jobsbot-backend
        proxy_pass http://10.130.0.15$request_uri;
        proxy_set_header Host $host;
    }
}
```

Create a symlink:

```
sudo ln -s /etc/nginx/sites-available/all.jobsbot.io /etc/nginx/sites-enabled/all.jobsbot.io
```

Install apache2-utils:

```
sudo apt-get install apache2-utils
```

Crate a folder for htpasswd:

```
/etc/nginx/htpasswd
```

Create a password for "preprod" user:

Use password: kn98y!3rhajs$dn23

```
sudo htpasswd -c /etc/nginx/htpasswd/.htpasswd preprod
```

Restart nginx:

```
sudo /etc/init.d/nginx restart
```
