Install nginx:

```
sudo apt install nginx
```

Setup a firewall:

```
sudo ufw enable
sudo ufw allow 'Nginx HTTP'
sudo ufw allow 'Nginx HTTPS'
sudo ufw allow 'OpenSSH'
sudo ufw status
```

Create all.jobsbot.io config:

```
sudo vim /etc/nginx/sites-available/frontent-jobsbot.io
```

Put the next content there:

```
server {
    listen 80;
    server_name admin.jobsbot.io;

    root /home/feltiko/dashboards/admin;

    location / {
        if (!-e $request_filename){
                rewrite ^(.*)$ /index.html break;
        }
    }
}

server {
    listen 80;
    server_name admin-preprod.jobsbot.io;

    root /home/feltiko/dashboards/admin-preprod;

    location / {
        if (!-e $request_filename){
                rewrite ^(.*)$ /index.html break;
        }
    }
}

server {
    listen 80;
    server_name console.jobsbot.io;

    root /home/feltiko/dashboards/console;

    location / {
        if (!-e $request_filename){
                rewrite ^(.*)$ /index.html break;
        }
    }
}

server {
    listen 80;
    server_name console-preprod.jobsbot.io;

    root /home/feltiko/dashboards/console-preprod;

    location / {
        if (!-e $request_filename){
                rewrite ^(.*)$ /index.html break;
        }
    }
}
```

Create a symlink:

```
sudo ln -s /etc/nginx/sites-available/frontent-jobsbot.io /etc/nginx/sites-enabled/frontent-jobsbot.io
```

Restart nginx:

```
sudo /etc/init.d/nginx restart
```
