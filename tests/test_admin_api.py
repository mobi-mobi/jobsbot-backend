import json
import pytest
import random
import string

from datetime import datetime, timedelta
from flask import testing

from jobsbot.dao import get_user_dao, UserDao
from jobsbot.core.enums import UserProfileValidationStatus, UserProfileRequiredPart, UserProfileRejectionReason, \
    RecruiterPermission, UserJobMatchType, UserJobApplicationMethod, UserJobApplicationStatus, UserJobMatchSource, \
    JobCategory, JobPosition
from jobsbot.core.models import Admin, AdminPermission, Company, User, UserCv, Credentials, Recruiter, UserJobMatch, \
    UserSubscription
from jobsbot.services.background import calculate_jobs_statistics
from jobsbot.utils.test import random_job
from jobsbot.web.api.admin import AdminApi


TEST_ADMIN_EMAILS_PREFIX = "__test__admin__email__"
TEST_CANDIDATE_TG_ID_PREFIX = "42424242"
TEST_COMPANY_ID_PREFIX = "__test_company__"
TEST_RECRUITER_EMAIL_PREFIX = "__test_recruiter__"
TEST_RECRUITER_RAW_PASSWORD = "__test_recruiter_raw_password__"
TEST_JOB_ID_PREFIX = "__test_job__"

TEST_ADMIN_NAME = "Unittest Admin"
TEST_ADMIN_EMAIL = "__test_admin_email__"
TEST_ADMIN_RAW_PASSWORD = "__test_admin_password__"


class TestAdminApiClient(testing.FlaskClient):
    def __init__(self, *args, **kwargs):
        self.jobsbot_token = None
        if "jobsbot_token" in kwargs:
            self.jobsbot_token = kwargs["jobsbot_token"]
            del kwargs["jobsbot_token"]

        super().__init__(*args, **kwargs)

    def open(self, *args, **kwargs):
        if self.jobsbot_token is not None:
            headers = kwargs.get('headers', {})
            headers['Authorization'] = "Bearer " + self.jobsbot_token
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


@pytest.fixture(scope="session")
def user_dao() -> UserDao:
    return get_user_dao()


@pytest.fixture(scope='session', autouse=True)
def clear_database(user_dao):
    def _delete_all_admins():
        for admin in user_dao.read_all_admins():
            if admin.email.startswith(TEST_ADMIN_EMAILS_PREFIX):
                user_dao.delete_all_sessions(admin.email)
                user_dao.delete_admin(admin.email)

    def _delete_all_candidates():
        for candidate in user_dao.read_all_users():
            if str(candidate.tg_id).startswith(TEST_CANDIDATE_TG_ID_PREFIX):
                user_dao.delete_user_job_applications(user_tg_id=candidate.tg_id)
                user_dao.delete_user_job_unsent_matches(user_tg_id=candidate.tg_id)
                user_dao.delete_user_job_matches(user_tg_id=candidate.tg_id)
                user_dao.delete_user(candidate.tg_id)

    def _delete_all_companies():
        for company in user_dao.read_all_companies():
            if company.company_id.startswith(TEST_COMPANY_ID_PREFIX):
                user_dao.delete_company(company.company_id)

    def _delete_all_jobs():
        for job in user_dao.read_all_jobs():
            if job.job_id.startswith(TEST_JOB_ID_PREFIX):
                user_dao.delete_job(job.job_id)

    # Will be executed before the first test
    _delete_all_admins()
    _delete_all_candidates()
    _delete_all_jobs()
    _delete_all_companies()
    yield
    # Will be executed after the last test
    _delete_all_admins()
    _delete_all_candidates()
    _delete_all_jobs()
    _delete_all_companies()


@pytest.fixture
def unexisting_candidate_tg_id(user_dao):
    alphabet = string.digits
    for _ in range(100):
        tg_id = int(TEST_CANDIDATE_TG_ID_PREFIX + "".join(random.choice(alphabet) for _ in range(10)))
        if user_dao.read_user(tg_id) is not None:
            continue
        yield tg_id
        try:
            user_dao.delete_user(tg_id)
        except Exception:
            pass
        break
    else:
        raise ValueError("Could not find tg_id that is not in use")


@pytest.fixture
def existing_candidate(user_dao, unexisting_candidate_tg_id):
    candidate = User.new_empty_user(unexisting_candidate_tg_id)
    user_dao.upsert_user(candidate)
    yield candidate
    try:
        user_dao.delete_user(unexisting_candidate_tg_id)
    except Exception:
        pass


@pytest.fixture
def unexisting_admin_email(user_dao):
    alphabet = string.ascii_lowercase + string.digits
    for _ in range(100):
        email = TEST_ADMIN_EMAILS_PREFIX + "".join(random.choice(alphabet) for _ in range(20))
        admin = user_dao.read_admin(email)
        if admin is not None:
            continue
        yield email
        try:
            user_dao.delete_admin(email, delete_credentials_if_possible=True)
        except Exception:
            pass
        break
    else:
        raise ValueError("Could not find an email that is not in use")


@pytest.fixture(scope="session")
def valid_super_admin(user_dao):
    user_dao.delete_all_sessions(TEST_ADMIN_EMAIL)
    user_dao.delete_admin(TEST_ADMIN_EMAIL, delete_credentials_if_possible=True)
    credentials = Credentials(
        email=TEST_ADMIN_EMAIL,
        raw_password=TEST_ADMIN_RAW_PASSWORD
    )
    user_dao.create_credentials(credentials, replace_if_exists=True)
    admin = Admin(
        email=TEST_ADMIN_EMAIL,
        name=TEST_ADMIN_NAME,
        admin_permissions=[AdminPermission.parse(p) for p in AdminPermission]
    )
    user_dao.create_admin(admin, replace_if_exists=True)
    yield admin
    user_dao.delete_all_sessions(TEST_ADMIN_EMAIL)
    user_dao.delete_admin(TEST_ADMIN_EMAIL, delete_credentials_if_possible=True)
    user_dao.delete_recruiters(TEST_ADMIN_EMAIL, delete_credentials_if_possible=True)


@pytest.fixture
def valid_super_admin_session(user_dao, valid_super_admin):
    session = user_dao.create_session(valid_super_admin.email, timedelta(days=1))
    yield session
    user_dao.delete_session_by_token(session.token)


@pytest.fixture
def valid_super_admin_token(valid_super_admin_session):
    yield valid_super_admin_session.token


@pytest.fixture
def existing_admin(user_dao, unexisting_admin_email):
    credentials = Credentials(
        email=unexisting_admin_email,
        raw_password="test password"
    )
    user_dao.create_credentials(credentials, replace_if_exists=True)
    admin = Admin(
        email=unexisting_admin_email,
        name="Test Valid Admin",
        admin_permissions=[],
    )
    user_dao.create_admin(admin, replace_if_exists=True)
    yield admin
    try:
        user_dao.delete_admin(admin.email, delete_credentials_if_possible=True)
    except Exception:
        pass
    try:
        user_dao.delete_recruiters(admin.email, delete_credentials_if_possible=True)
    except Exception:
        pass


@pytest.fixture
def admin_api_client(valid_super_admin_token):
    api = AdminApi(name="AdminApi")
    api.app.test_client_class = TestAdminApiClient
    client = api.app.test_client(jobsbot_token=valid_super_admin_token)
    yield client


@pytest.fixture
def unexisting_company(user_dao):
    alphabet = string.digits
    for _ in range(100):
        company_id = TEST_COMPANY_ID_PREFIX + "".join(random.choice(alphabet) for _ in range(10))
        referral_link_id = "".join(random.choice(alphabet) for _ in range(10))
        if user_dao.read_company(company_id) is not None:
            continue
        if user_dao.read_company_by_referral_link_id(referral_link_id) is not None:
            continue
        yield Company(
            company_id=company_id,
            company_name="Test Company",
            referral_link_id=referral_link_id,
            is_consulting=False,
            is_agency=False,
            is_startup=False
        )
        try:
            user_dao.delete_company(company_id)
        except Exception:
            pass
        break
    else:
        raise ValueError("Could not find company_id and/or referral_link_id that is not in use")


@pytest.fixture
def existing_company(user_dao, unexisting_company):
    user_dao.upsert_company(unexisting_company)
    yield unexisting_company
    try:
        user_dao.delete_company(unexisting_company.company_id)
    except Exception:
        pass


@pytest.fixture
def unexisting_recruiter(user_dao):
    alphabet = string.digits
    for _ in range(100):
        recruiter_email = TEST_RECRUITER_EMAIL_PREFIX + "".join(random.choice(alphabet) for _ in range(10))
        if user_dao.read_admin(recruiter_email) is not None:
            continue
        yield Recruiter(
            name="Unittest Recruiter",
            company_id="Unexisting Company Id",
            email=recruiter_email,
            recruiter_permissions=[]
        )
        try:
            user_dao.delete_recruiters(recruiter_email, delete_credentials_if_possible=True)
        except Exception:
            pass
        try:
            user_dao.delete_admin(recruiter_email, delete_credentials_if_possible=True)
        except Exception:
            pass
        break
    else:
        raise ValueError("Could not find recruiter email that is not in use")


@pytest.fixture
def existing_recruiter(user_dao, unexisting_recruiter, existing_company):
    credentials = Credentials(
        email=unexisting_recruiter.email,
        raw_password=TEST_RECRUITER_RAW_PASSWORD
    )
    unexisting_recruiter.name = f"Unittest Recruiter {unexisting_recruiter.email}",
    unexisting_recruiter.company_id = existing_company

    user_dao.create_credentials(credentials)
    user_dao.create_recruiter(unexisting_recruiter)
    yield unexisting_recruiter
    try:
        user_dao.delete_recruiters(unexisting_recruiter.email, delete_credentials_if_possible=True)
    except Exception:
        pass
    try:
        user_dao.delete_admin(unexisting_recruiter.email, delete_credentials_if_possible=True)
    except Exception:
        pass


def test_admin_api_status(admin_api_client):
    rv = admin_api_client.get("/api/v1/status")
    assert rv.data == b'{"status": "OK"}'


def test_get_my_profile(admin_api_client):
    rv = admin_api_client.get("/api/v1/my_profile")
    assert rv.status_code == 200


def test_create_read_update_delete_admin(admin_api_client, unexisting_admin_email):
    new_admin_password = "somerandompassword"

    rv = admin_api_client.get(f"/api/v1/admin?not_an_email={unexisting_admin_email}")
    assert rv.status_code == 400
    assert b'email' in rv.data
    assert b'must be set' in rv.data

    rv = admin_api_client.get(f"/api/v1/admin?email={unexisting_admin_email}")
    assert rv.status_code == 404
    assert b'Can not find an object' in rv.data

    admin_data = {
        "name": "New admin",
        "email": unexisting_admin_email,
        "admin_permissions": [],
    }
    rv = admin_api_client.post("/api/v1/admin", data=json.dumps(admin_data))
    assert rv.status_code == 400
    assert b'Please, provide credentials in your request' in rv.data

    admin_data["credentials"] = {"password": new_admin_password}
    rv = admin_api_client.post("/api/v1/admin", data=json.dumps(admin_data))
    assert rv.status_code == 200

    rv = admin_api_client.get("/page/v1/admins")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert len(response["data"]["admins"]) == 2
    assert len(response["data"]["possible_admin_permissions"]) == 13

    new_admin_data = {
        "name": "Updated name",
        "credentials": {
            "password": "12345678"
        }
    }
    admin_data["password"] = new_admin_password
    rv = admin_api_client.patch(f"/api/v1/admin?email={unexisting_admin_email}", data=json.dumps(new_admin_data))
    assert rv.status_code == 200
    rv = admin_api_client.get(f"/api/v1/admin?email={unexisting_admin_email}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert response["data"]["email"] == unexisting_admin_email
    assert response["data"]["name"] == "Updated name"

    rv = admin_api_client.delete(f"/api/v1/admin?email={unexisting_admin_email}")
    assert rv.status_code == 200
    rv = admin_api_client.delete(f"/api/v1/admin?email={unexisting_admin_email}")
    assert rv.status_code == 200

    rv = admin_api_client.get(f"/api/v1/admin?email={unexisting_admin_email}")
    assert rv.status_code == 404
    assert b'Can not find an object' in rv.data


def test_read_user_cv(admin_api_client, user_dao, existing_candidate):
    rv = admin_api_client.get(f"/api/v1/candidate/cv?wrong_tg_id_param={existing_candidate.tg_id}")
    assert rv.status_code == 400
    assert b'tg_id' in rv.data
    assert b'must be set' in rv.data

    rv = admin_api_client.get(f"/api/v1/candidate/cv?tg_id={existing_candidate.tg_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert not response["data"]["cv_uploaded"]

    user_dao.update_user_cv(existing_candidate.tg_id, UserCv(existing_candidate.tg_id, b'123', "cv.pdf",
                                                             datetime.utcnow()))
    assert user_dao.is_cv_uploaded(existing_candidate.tg_id)
    assert user_dao.read_user(existing_candidate.tg_id)

    rv = admin_api_client.get(f"/api/v1/candidate/cv?tg_id={existing_candidate.tg_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert response["data"]["cv_uploaded"]
    assert response["data"]["cv"]
    assert response["data"]["cv"]["file_base64"]


def test_reject_candidate(admin_api_client, user_dao, existing_candidate):
    assert existing_candidate.validation_status == UserProfileValidationStatus.NOT_READY

    existing_candidate.set_profile_detail_as_updated(UserProfileRequiredPart.CV)
    assert existing_candidate.validation_status == UserProfileValidationStatus.NOT_READY

    for profile_part in UserProfileRequiredPart:
        existing_candidate.set_profile_detail_as_updated(profile_part)
    assert existing_candidate.validation_status == UserProfileValidationStatus.WAITING_VERIFICATION

    user_dao.save_user(existing_candidate)

    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?some_wrong_tg_id_param={existing_candidate.tg_id}")
    assert rv.status_code == 400
    assert b'tg_id' in rv.data
    assert b'must be set' in rv.data

    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}")
    assert rv.status_code == 400
    assert b'Request data is empty' in rv.data

    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}",
                              data=json.dumps({}))
    assert rv.status_code == 400
    assert b'is a required property' in rv.data

    data = {
        "validation_status": UserProfileValidationStatus.REJECTED.name
    }

    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}",
                              data=json.dumps(data))
    assert rv.status_code == 400
    assert b'rejection_reasons field is missing for REJECTED status' in rv.data

    data["rejection_reasons"] = []
    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}",
                              data=json.dumps(data))
    assert rv.status_code == 400
    assert b'Can not parse request data: [] is too short' in rv.data

    data["rejection_reasons"] = [UserProfileRejectionReason.CV_IS_NOT_READABLE.name,
                                 UserProfileRejectionReason.WRONG_NAME_IN_CV.name]
    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}",
                              data=json.dumps(data))
    assert rv.status_code == 400
    assert b'rejected_profile_parts field is missing for REJECTED status' in rv.data

    data["rejected_profile_parts"] = []
    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}",
                              data=json.dumps(data))
    assert rv.status_code == 400
    assert b'Can not parse request data: [] is too short' in rv.data

    data["rejected_profile_parts"] = [UserProfileRequiredPart.CV.name, UserProfileRequiredPart.NAME.name]
    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}",
                              data=json.dumps(data))
    assert rv.status_code == 200

    existing_candidate = user_dao.read_user(existing_candidate.tg_id)
    assert existing_candidate.validation_status == UserProfileValidationStatus.REJECTED
    assert sorted(existing_candidate.missing_profile_parts) \
           == sorted([UserProfileRequiredPart.CV, UserProfileRequiredPart.NAME])
    assert sorted(existing_candidate.validation_rejection_reasons) \
           == sorted([UserProfileRejectionReason.WRONG_NAME_IN_CV, UserProfileRejectionReason.CV_IS_NOT_READABLE])
    assert not existing_candidate.validation_status_update_sent
    assert existing_candidate.validation_status_updated_at > datetime.utcnow() - timedelta(minutes=1)
    assert existing_candidate.changed_after_last_validation_profile_parts == []


def test_accept_candidate(admin_api_client, user_dao, existing_candidate):
    assert existing_candidate.validation_status == UserProfileValidationStatus.NOT_READY

    existing_candidate.set_profile_detail_as_updated(UserProfileRequiredPart.CV)
    assert existing_candidate.validation_status == UserProfileValidationStatus.NOT_READY

    for profile_part in UserProfileRequiredPart:
        existing_candidate.set_profile_detail_as_updated(profile_part)
    assert existing_candidate.validation_status == UserProfileValidationStatus.WAITING_VERIFICATION

    existing_candidate.validation_rejection_reasons = [UserProfileRejectionReason.WRONG_NAME_IN_CV]

    user_dao.save_user(existing_candidate)

    data = {
        "validation_status": UserProfileValidationStatus.VERIFIED.name
    }

    rv = admin_api_client.put(f"/api/v1/candidate/validation_status?tg_id={existing_candidate.tg_id}",
                              data=json.dumps(data))
    assert rv.status_code == 200

    existing_candidate = user_dao.read_user(existing_candidate.tg_id)
    assert existing_candidate.validation_status == UserProfileValidationStatus.VERIFIED
    assert existing_candidate.missing_profile_parts == []
    assert existing_candidate.validation_rejection_reasons == []
    assert not existing_candidate.validation_status_update_sent
    assert existing_candidate.validation_status_updated_at > datetime.utcnow() - timedelta(minutes=1)
    assert existing_candidate.changed_after_last_validation_profile_parts == []


def test_update_admin_password(admin_api_client, user_dao):
    old_admin_password = TEST_ADMIN_RAW_PASSWORD
    new_admin_password = TEST_ADMIN_RAW_PASSWORD + "new"

    assert user_dao.read_credentials(TEST_ADMIN_EMAIL).is_password_correct(old_admin_password)
    assert not user_dao.read_credentials(TEST_ADMIN_EMAIL).is_password_correct(new_admin_password)

    update_password_data = {
        "password": new_admin_password
    }
    rv = admin_api_client.put("/api/v1/my_profile/password", data=json.dumps(update_password_data))
    assert rv.status_code == 400
    update_password_data = {
        "old_password": old_admin_password
    }
    rv = admin_api_client.put("/api/v1/my_profile/password", data=json.dumps(update_password_data))
    assert rv.status_code == 400

    update_password_data = {
        "old_password": old_admin_password,
        "password": new_admin_password
    }
    rv = admin_api_client.put("/api/v1/my_profile/password", data=json.dumps(update_password_data))
    assert rv.status_code == 200

    assert not user_dao.read_credentials(TEST_ADMIN_EMAIL).is_password_correct(old_admin_password)
    assert user_dao.read_credentials(TEST_ADMIN_EMAIL).is_password_correct(new_admin_password)

    update_password_data = {
        "old_password": new_admin_password,
        "password": old_admin_password
    }
    rv = admin_api_client.put("/api/v1/my_profile/password", data=json.dumps(update_password_data))
    assert rv.status_code == 200

    assert user_dao.read_credentials(TEST_ADMIN_EMAIL).is_password_correct(old_admin_password)
    assert not user_dao.read_credentials(TEST_ADMIN_EMAIL).is_password_correct(new_admin_password)


def test_get_admins_page(admin_api_client):
    rv = admin_api_client.get("/page/v1/admins")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert "admins" in response["data"]


def test_get_users_page(admin_api_client):
    rv = admin_api_client.get("/page/v1/candidates")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert "candidates" in response["data"]


def test_get_companies_page(admin_api_client, user_dao):
    company = Company(
        company_id=TEST_COMPANY_ID_PREFIX + "some company",
        company_name="Test Company",
        referral_link_id=str(int((10 ** 10) * random.random())),
        is_consulting=False,
        is_agency=False,
        is_startup=False
    )
    user_dao.upsert_company(company)
    rv = admin_api_client.get("/page/v1/companies")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert "companies" in response["data"]


def test_delete_company(admin_api_client, existing_company):
    rv = admin_api_client.get(f"/api/v1/company?company_id={existing_company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["company_id"] == existing_company.company_id
    rv = admin_api_client.delete(f"/api/v1/company?company_id={existing_company.company_id}")
    assert rv.status_code == 200
    rv = admin_api_client.delete(f"/api/v1/company?company_id={existing_company.company_id}")
    assert rv.status_code == 200
    rv = admin_api_client.get(f"/api/v1/company?company_id={existing_company.company_id}")
    assert rv.status_code == 404


def test_get_create_patch_archive_delete_job(admin_api_client, user_dao, existing_company):
    job = random_job(
        company_id=existing_company.company_id
    )
    job_json = job.as_json_dict()
    del job_json["created_at"]
    del job_json["created_by"]
    del job_json["updated_at"]
    del job_json["job_id"]
    job_json["position"] = job.position.name
    job_json["locations"] = [loc.name for loc in job.locations]
    job_json["relocation_from_countries"] = [country.name for country in job.relocation_from_countries]
    job_json["job_posting_status"] = job.job_posting_status.name
    job_json["job_posting_validation_status"] = job.job_posting_validation_status.name
    job_json["min_experience"] = job.min_experience.name
    rv = admin_api_client.post("/api/v1/job", data=json.dumps(job_json))
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["company_id"] == existing_company.company_id
    assert response["data"]["title"] == job.title
    assert response["data"]["job_posting_validation_status"] == "NOT_READY"
    job_id = response["data"]["job_id"]
    rv = admin_api_client.get(f"/api/v1/job?job_id={job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["company_id"] == existing_company.company_id
    assert response["data"]["title"] == job.title
    new_title = job.title + "new"
    assert response["data"]["title"] != new_title
    job_json["title"] = new_title
    rv = admin_api_client.patch(f"/api/v1/job?job_id={job_id}", data=json.dumps(job_json))
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    rv = admin_api_client.get(f"/api/v1/job?job_id={job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["title"] == new_title
    rv = admin_api_client.patch(f"/api/v1/job/archive?job_id={job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    rv = admin_api_client.get(f"/api/v1/job?job_id={job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["job_posting_status"] == "ARCHIVED"
    rv = admin_api_client.patch(f"/api/v1/job/validate?job_id={job_id}&validation_status=VERIFIED")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    rv = admin_api_client.get(f"/api/v1/job?job_id={job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["job_posting_validation_status"] == "VERIFIED"
    rv = admin_api_client.delete(f"/api/v1/job?job_id={job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    rv = admin_api_client.delete(f"/api/v1/job?job_id={job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    rv = admin_api_client.get(f"/api/v1/job?job_id={job_id}")
    assert rv.status_code == 404


def test_get_jobs_page(admin_api_client, user_dao):
    company = Company(
        company_id=TEST_COMPANY_ID_PREFIX + " some company to test a jobs page",
        company_name="Test Company",
        referral_link_id=str(int((10 ** 10) * random.random())),
        is_consulting=False,
        is_agency=False,
        is_startup=False
    )
    user_dao.upsert_company(company)
    jobs = []
    for _ in range(10):
        job = random_job(company_id=company.company_id)
        user_dao.upsert_job(job)
        jobs.append(job)
    rv = admin_api_client.get("/page/v1/jobs")
    assert rv.status_code == 400
    rv = admin_api_client.get(f"/page/v1/jobs?company_id={company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert "jobs" in response["data"]
    assert len(response["data"]["jobs"]) == 10

    rv = admin_api_client.get(f"/api/v1/company/jobs?company_id={company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert len(response["data"]) == 10


def test_create_delete_recruiter(admin_api_client, unexisting_recruiter, existing_company, user_dao):
    rv = admin_api_client.get("/api/v1/recruiter")
    assert rv.status_code == 400
    rv = admin_api_client.get(f"/api/v1/recruiter?recruiter_email={unexisting_recruiter.email}")
    assert rv.status_code == 400
    assert b'must be set' in rv.data
    rv = admin_api_client.get(f"/api/v1/recruiter?recruiter_email={unexisting_recruiter.email}"
                              f"&company_id={existing_company.company_id}")
    assert rv.status_code == 404
    recruiter_as_json = {
        "email": unexisting_recruiter.email,
        "name": unexisting_recruiter.name,
        "company_id": existing_company.company_id,
        "credentials": {
            "password": TEST_RECRUITER_RAW_PASSWORD
        },
        "recruiter_permissions": []
    }
    invitations = list(user_dao.read_recruiter_invitations(email=unexisting_recruiter.email,
                                                           company_id=existing_company.company_id))
    assert not invitations
    rv = admin_api_client.post(f"/api/v1/recruiter?company_id={existing_company.company_id}",
                               data=json.dumps(recruiter_as_json))
    assert rv.status_code == 200
    rv = admin_api_client.get(f"/api/v1/recruiter?company_id={existing_company.company_id}"
                              f"&recruiter_email={unexisting_recruiter.email}")
    assert rv.status_code == 404
    invitations = list(user_dao.read_recruiter_invitations(email=unexisting_recruiter.email,
                                                           company_id=existing_company.company_id))
    assert len(invitations) == 1
    rv = admin_api_client.post(f"/api/v1/recruiter?company_id={existing_company.company_id}",
                               data=json.dumps(recruiter_as_json))
    assert rv.status_code == 400
    user_dao.delete_recruiter_invitation_by_token(invitations[0].invitation_token)
    rv = admin_api_client.post(f"/api/v1/recruiter?company_id={existing_company.company_id}&skip_invitation=on",
                               data=json.dumps(recruiter_as_json))
    assert rv.status_code == 200
    rv = admin_api_client.get(f"/api/v1/recruiter?company_id={existing_company.company_id}"
                              f"&recruiter_email={unexisting_recruiter.email}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["email"] == unexisting_recruiter.email
    assert response["data"]["name"] == unexisting_recruiter.name
    assert not response["data"]["recruiter_permissions"]

    recruiter_as_json = {
        "recruiter_permissions": [RecruiterPermission.CREATE_JOBS.name]
    }

    rv = admin_api_client.patch(f"/api/v1/recruiter?recruiter_email={unexisting_recruiter.email}"
                                f"&company_id={existing_company.company_id}", data=json.dumps(recruiter_as_json))
    assert rv.status_code == 200
    rv = admin_api_client.get(f"/api/v1/recruiter?recruiter_email={unexisting_recruiter.email}"
                              f"&company_id={existing_company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]["recruiter_permissions"] == {RecruiterPermission.CREATE_JOBS.name:
                                                         RecruiterPermission.CREATE_JOBS.title()}

    rv = admin_api_client.delete(f"/api/v1/recruiter?recruiter_email={unexisting_recruiter.email}"
                                 f"&company_id={existing_company.company_id}")
    assert rv.status_code == 200
    rv = admin_api_client.delete(f"/api/v1/recruiter?recruiter_email={unexisting_recruiter.email}"
                                 f"&company_id={existing_company.company_id}")
    assert rv.status_code == 200
    rv = admin_api_client.get(f"/api/v1/recruiter?recruiter_email={unexisting_recruiter.email}"
                              f"&company_id={existing_company.company_id}")
    assert rv.status_code == 404


def test_get_enums(admin_api_client):
    rv = admin_api_client.get(f"/api/v1/enums")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    for enum_name in ("job_category", "job_position", "skills_category", "user_skill", "user_experience",
                      "user_desired_min_annual_gross_salary", "user_country", "country_locations"):
        assert response["data"][enum_name]


def test_get_enums_maps(admin_api_client):
    rv = admin_api_client.get(f"/api/v1/enums_maps")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    for enum_map_name in ("locations_map", "jobs_map", "skills_map"):
        assert response["data"][enum_map_name]


def test_job_statistics(admin_api_client, existing_company, user_dao, existing_candidate):
    job = random_job(company_id=existing_company.company_id)

    rv = admin_api_client.get(f"/api/v1/job/statistics")
    assert rv.status_code == 400
    assert b'job_id' in rv.data
    assert b'must be set' in rv.data

    rv = admin_api_client.get(f"/api/v1/job/statistics?job_id={job.job_id}")
    assert rv.status_code == 404
    assert b'does not exist' in rv.data

    user_dao.upsert_job(job)
    rv = admin_api_client.get(f"/api/v1/job/statistics?job_id={job.job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert len(response["data"]) == 11

    for field_name in ("counter_total_matches", "counter_total_applications_by_method",
                       "counter_total_applications_by_status", "chart_matches_last_30_days",
                       "chart_daily_notifications_sent_last_30_days", "chart_weekly_notifications_sent_last_30_days",
                       "chart_applications_by_method_last_30_days", "chart_applications_by_status_last_30_days",
                       "counter_total_daily_notification_sent", "counter_total_weekly_notification_sent"):
        assert field_name in response["data"]
        if field_name.startswith("counter_"):
            assert response["data"][field_name]
            for v in response["data"][field_name].values():
                assert v == 0
        elif field_name.startswith("chart_"):
            for v in response["data"][field_name].values():
                for d, c in v:
                    assert not c
        else:
            raise AssertionError(f"Unsupported field name: {field_name}")

    user_dao.create_user_job_match(UserJobMatch(
        user_tg_id=existing_candidate.tg_id,
        job_id=job.job_id,
        matched_on=datetime.utcnow(),
        match_type=UserJobMatchType.FULL_MATCH,
        match_source=UserJobMatchSource.JOB_MATCH,
        daily_notification_sent=False,
        daily_notification_sent_at=None,
        weekly_notification_sent=False,
        weekly_notification_sent_at=None
    ))

    rv = admin_api_client.get(f"/api/v1/job/statistics?job_id={job.job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert response["data"]["counter_total_matches"][UserJobMatchType.FULL_MATCH.name] == 0
    assert response["data"]["counter_total_daily_notification_sent"][UserJobMatchType.FULL_MATCH.name] == 0
    assert response["data"]["counter_total_weekly_notification_sent"][UserJobMatchType.FULL_MATCH.name] == 0

    calculate_jobs_statistics()

    rv = admin_api_client.get(f"/api/v1/job/statistics?job_id={job.job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    assert response["data"]["counter_total_matches"][UserJobMatchType.FULL_MATCH.name] == 1

    assert len(response["data"]["counter_total_matches"]) == 2
    assert response["data"]["counter_total_matches"]["PARTIAL_MATCH"] == 0
    assert response["data"]["counter_total_matches"]["FULL_MATCH"] == 1
    assert response["data"]["counter_total_daily_notification_sent"][UserJobMatchType.FULL_MATCH.name] == 0
    assert response["data"]["counter_total_weekly_notification_sent"][UserJobMatchType.FULL_MATCH.name] == 0

    assert len(response["data"]["counter_total_applications_by_method"]) == 2
    for method in UserJobApplicationMethod:
        assert response["data"]["counter_total_applications_by_method"][method.name] == 0

    assert len(response["data"]["counter_total_applications_by_status"]) == 5
    for status in UserJobApplicationStatus:
        assert response["data"]["counter_total_applications_by_status"][status.name] == 0

    assert len(response["data"]["chart_matches_last_30_days"]) == 2
    assert all(map(lambda x: x[1] == 0, response["data"]["chart_matches_last_30_days"]["PARTIAL_MATCH"]))
    assert sum(map(lambda x: x[1], response["data"]["chart_matches_last_30_days"]["FULL_MATCH"])) == 1
    assert min(map(lambda x: x[1], response["data"]["chart_matches_last_30_days"]["FULL_MATCH"])) == 0
    assert max(map(lambda x: x[1], response["data"]["chart_matches_last_30_days"]["FULL_MATCH"])) == 1

    for match_type in UserJobMatchType:
        if match_type == UserJobMatchType.NO_MATCH:
            continue
        assert all(map(lambda x: x[1] == 0,
                       response["data"]["chart_daily_notifications_sent_last_30_days"][match_type.name]))
        assert all(map(lambda x: x[1] == 0,
                       response["data"]["chart_weekly_notifications_sent_last_30_days"][match_type.name]))

    assert len(response["data"]["chart_applications_by_method_last_30_days"]) == 2
    for method in UserJobApplicationMethod:
        assert all(map(lambda x: x[1] == 0, response["data"]["chart_applications_by_method_last_30_days"][method.name]))

    assert len(response["data"]["chart_applications_by_status_last_30_days"]) == 5
    for status in UserJobApplicationStatus:
        assert all(map(lambda x: x[1] == 0, response["data"]["chart_applications_by_status_last_30_days"][status.name]))

    matches = list(user_dao.read_user_job_matches(job_id=job.job_id))
    assert len(matches) == 1
    match = matches[0]
    user_dao.update_user_job_match_sent_status(
        user_tg_id=match.user_tg_id,
        job_id=match.job_id,
        daily_notification_sent=True,
        daily_notification_sent_at=datetime.utcnow()
    )

    calculate_jobs_statistics()

    rv = admin_api_client.get(f"/api/v1/job/statistics?job_id={job.job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]
    data = response["data"]
    assert data["counter_total_matches"][UserJobMatchType.FULL_MATCH.name] == 1
    assert data["counter_total_daily_notification_sent"][UserJobMatchType.FULL_MATCH.name] == 1
    assert data["counter_total_daily_notification_sent"][UserJobMatchType.PARTIAL_MATCH.name] == 0
    assert data["counter_total_weekly_notification_sent"][UserJobMatchType.FULL_MATCH.name] == 0
    assert data["counter_total_weekly_notification_sent"][UserJobMatchType.PARTIAL_MATCH.name] == 0

    assert sum(map(lambda x: x[1],
                   response["data"]["chart_daily_notifications_sent_last_30_days"][UserJobMatchType.FULL_MATCH.name])) \
           == 1
    assert min(map(lambda x: x[1],
                   response["data"]["chart_daily_notifications_sent_last_30_days"][UserJobMatchType.FULL_MATCH.name])) \
           == 0
    assert all(map(lambda x: x[1] == 0,
                   response["data"]["chart_daily_notifications_sent_last_30_days"]
                   [UserJobMatchType.PARTIAL_MATCH.name]))

    for match_type in UserJobMatchType:
        if match_type == UserJobMatchType.NO_MATCH:
            continue
        assert all(map(lambda x: x[1] == 0,
                       response["data"]["chart_weekly_notifications_sent_last_30_days"][match_type.name]))


def test_page_job(user_dao, existing_company, admin_api_client):
    job = random_job(company_id=existing_company.company_id)

    rv = admin_api_client.get(f"/page/v1/job?job_id={job.job_id}")
    assert rv.status_code == 404

    user_dao.upsert_job(job)

    rv = admin_api_client.get(f"/page/v1/job?job_id={job.job_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert response["data"]

    user_dao.delete_job(job.job_id)


def test_recruiter_invitations(user_dao, existing_company, admin_api_client, unexisting_recruiter):
    rv = admin_api_client.get(f"/api/v1/recruiter/invitations?company_id={existing_company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert not response["data"]

    request_body = {
        "email": unexisting_recruiter.email,
        "name": unexisting_recruiter.name,
        "company_id": existing_company.company_id,
        "recruiter_permissions": [RecruiterPermission.CREATE_JOBS.name]
    }

    rv = admin_api_client.post(f"/api/v1/recruiter?company_id={existing_company.company_id}",
                               data=json.dumps(request_body))
    assert rv.status_code == 200

    rv = admin_api_client.get(f"/api/v1/recruiter/invitations?company_id={existing_company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert len(response["data"]) == 1

    invitations = list(user_dao.read_recruiter_invitations(email=unexisting_recruiter.email,
                                                           company_id=existing_company.company_id))
    assert len(invitations) == 1
    create_credentials_body = {
        "password": "some_password"
    }
    rv = admin_api_client.post(f"/api/v1/recruiter/credentials/from_invitation?token=wrong_token_"
                               f"{invitations[0].invitation_token}",
                               data=json.dumps(create_credentials_body))
    assert rv.status_code == 400
    assert b'Invalid invitation token.' in rv.data
    rv = admin_api_client.post(f"/api/v1/recruiter/credentials/from_invitation?token={invitations[0].invitation_token}",
                               data=json.dumps(create_credentials_body))
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    rv = admin_api_client.post(f"/api/v1/recruiter/credentials/from_invitation?token={invitations[0].invitation_token}",
                               data=json.dumps(create_credentials_body))
    assert rv.status_code == 400
    assert b'You credentials have already been set. Just try to log in.' in rv.data

    rv = admin_api_client.post(f"/api/v1/recruiter?company_id={existing_company.company_id}",
                               data=json.dumps(request_body))
    assert rv.status_code == 400
    assert b'This recruiter has already been invited' in rv.data

    rv = admin_api_client.get(f"/api/v1/recruiter/invitations?company_id={existing_company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert len(response["data"]) == 1

    rv = admin_api_client.delete(f"/api/v1/recruiter/invitations?recruiter_email={unexisting_recruiter.email}"
                                 f"&company_id={existing_company.company_id}")
    assert rv.status_code == 200

    rv = admin_api_client.get(f"/api/v1/recruiter/invitations?company_id={existing_company.company_id}")
    assert rv.status_code == 200
    response = json.loads(rv.data)
    assert response["status"] == "OK"
    assert not response["data"]


def test_user_subscription(user_dao, existing_candidate, existing_company):
    assert not list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id))
    assert not list(user_dao.read_user_subscriptions(company_id=existing_company.company_id))

    user_subscription = UserSubscription(
        user_tg_id=existing_candidate.tg_id,
        company_id=existing_company.company_id,
        job_categories=[JobCategory.SOFTWARE_ENGINEERING],
        job_positions=[JobPosition.SWE_BACKEND],
        all_matching_positions=False,
        muted_until=None
    )

    user_dao.create_user_subscription(user_subscription)

    assert len(list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id))) == 1
    assert len(list(user_dao.read_user_subscriptions(company_id=existing_company.company_id))) == 1

    assert not list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id + 1))
    assert not list(user_dao.read_user_subscriptions(company_id=existing_company.company_id + "+"))

    assert not list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id, muted=True))
    assert len(list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id, muted=False))) == 1

    assert not list(user_dao.read_user_subscriptions(company_id=existing_company.company_id, muted=True))
    assert len(list(user_dao.read_user_subscriptions(company_id=existing_company.company_id, muted=False))) == 1

    user_dao.mute_user_subscription(user_tg_id=existing_candidate.tg_id, company_id=existing_company.company_id,
                                    mute_for=timedelta(days=1))

    assert not list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id, muted=False))
    assert len(list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id, muted=True))) == 1

    assert not list(user_dao.read_user_subscriptions(company_id=existing_company.company_id, muted=False))
    assert len(list(user_dao.read_user_subscriptions(company_id=existing_company.company_id, muted=True))) == 1

    user_dao.unmute_user_subscription(user_tg_id=existing_candidate.tg_id, company_id=existing_company.company_id)

    assert not list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id, muted=True))
    assert len(list(user_dao.read_user_subscriptions(user_tg_id=existing_candidate.tg_id, muted=False))) == 1

    assert not list(user_dao.read_user_subscriptions(company_id=existing_company.company_id, muted=True))
    assert len(list(user_dao.read_user_subscriptions(company_id=existing_company.company_id, muted=False))) == 1

    job = random_job(position=JobPosition.SWE_BACKEND, company_id=existing_company.company_id)
    assert len(list(user_dao.read_matching_user_subscriptions(job))) == 1

    job.position = JobPosition.SWE_IOS
    assert len(list(user_dao.read_matching_user_subscriptions(job))) == 1

    job.position = JobPosition.BUSINESS_ANALYST
    assert not list(user_dao.read_matching_user_subscriptions(job))
