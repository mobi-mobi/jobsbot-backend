import inspect
import jobsbot.core.enums as my_enums

from jobsbot.core.interfaces import HrBotEnum
from jobsbot.core.enums import *  # Do not delete this line


def test_enums():
    for enum_class_name, _ in inspect.getmembers(my_enums, inspect.isclass):
        enum_class = globals()[enum_class_name]
        if issubclass(enum_class, HrBotEnum):
            for enum_position in enum_class:
                enum_position.title()
